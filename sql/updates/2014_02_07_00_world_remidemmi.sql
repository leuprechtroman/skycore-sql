REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90900, 40634, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90900, 40635, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90900, 40636, 0, 1, 3, 1, 1);

REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (29448, 2, 100, 1, 3, -90900, 2);

DELETE FROM `creature_loot_template` WHERE  `entry`=29448 AND `item`=40634;
DELETE FROM `creature_loot_template` WHERE  `entry`=29448 AND `item`=40635;
DELETE FROM `creature_loot_template` WHERE  `entry`=29448 AND `item`=40636;


DELETE FROM `creature_loot_template` WHERE  `entry`=29718 AND `item`=40637;
DELETE FROM `creature_loot_template` WHERE  `entry`=29718 AND `item`=40638;
DELETE FROM `creature_loot_template` WHERE  `entry`=29718 AND `item`=40639;

REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (29718, 2, 100, 1, 3, -90901, 2);

REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90901, 40637, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90901, 40638, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90901, 40639, 0, 1, 3, 1, 1);


DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=40631;
DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=40632;
DELETE FROM `creature_loot_template` WHERE  `entry`=30061 AND `item`=40633;

REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (30061, 2, 100, 1, 3, -90903, 2);

REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90903, 40631, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90903, 40632, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90903, 40633, 0, 1, 3, 1, 1);


DELETE FROM `gameobject_loot_template` WHERE  `entry`=25193 AND `item`=40627;
DELETE FROM `gameobject_loot_template` WHERE  `entry`=25193 AND `item`=40625;
DELETE FROM `gameobject_loot_template` WHERE  `entry`=25193 AND `item`=40626;

REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (25193, 2, 100, 1, 3, -90904, 2);

REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90904, 40626, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90904, 40627, 0, 1, 3, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90904, 40625, 0, 1, 3, 1, 1);
