REPLACE INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES (909001, 3, 1387, 'Gothik\'s Truhe', '', '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 1634, 16060, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 1);
REPLACE INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES (909002, 3, 1387, 'Gothik\'s Truhe', '', '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 1634, 29955, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 1);

REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (16060, 1, 100, 1, 0, -34103, 2);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (16060, 40753, 100, 1, 0, 1, 1);

DELETE FROM `creature_loot_template` WHERE  `entry`=16060 AND `item`=1;
DELETE FROM `creature_loot_template` WHERE  `entry`=16060 AND `item`=40753;
DELETE FROM `creature_loot_template` WHERE  `entry`=16060 AND `item`=45912;

REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (29955, 40753, 100, 1, 0, 1, 1);
REPLACE INTO `gameobject_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (29955, 1, 100, 1, 0, -34145, 4);


DELETE FROM `creature_loot_template` WHERE  `entry`=29955 AND `item`=40753;
DELETE FROM `creature_loot_template` WHERE  `entry`=29955 AND `item`=1;
DELETE FROM `creature_loot_template` WHERE  `entry`=29955 AND `item`=45912;