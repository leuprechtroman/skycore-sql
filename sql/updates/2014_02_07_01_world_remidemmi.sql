DELETE FROM `creature_loot_template` WHERE  `entry`=31311 AND `item`=40629;
DELETE FROM `creature_loot_template` WHERE  `entry`=31311 AND `item`=40628;
DELETE FROM `creature_loot_template` WHERE  `entry`=31311 AND `item`=40630;

REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (31311, 5, 100, 1, 1, -90902, 2);

REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90902, 40628, 0, 1, 1, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90902, 40629, 0, 1, 1, 1, 1);
REPLACE INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (90902, 40630, 0, 1, 1, 1, 1);
