UPDATE `creature_template` SET `dmg_multiplier`=7 WHERE  `entry`=15977;
UPDATE `creature_template` SET `dmg_multiplier`=15 WHERE  `entry`=29229;
UPDATE `creature_template` SET `dmg_multiplier`=18 WHERE  `entry`=29241;
UPDATE `creature_template` SET `dmg_multiplier`=9 WHERE  `entry`=15975;
UPDATE `smart_scripts` SET `event_flags`=9 WHERE  `entryorguid`=15976 AND `source_type`=0 AND `id`=0 AND `link`=0;
UPDATE `smart_scripts` SET `event_flags`=17 WHERE  `entryorguid`=15976 AND `source_type`=0 AND `id`=1 AND `link`=0;
DELETE FROM `smart_scripts` WHERE  `entryorguid`=15652 AND `source_type`=0 AND `id`=1 AND `link`=0;
DELETE FROM smart_scripts` WHERE  `entryorguid`=15652 AND `source_type`=0 AND `id`=0 AND `link`=0;