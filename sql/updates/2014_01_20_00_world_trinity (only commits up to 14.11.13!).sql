--trinity DB updates from 27.09.13- 14.11.13 without changes on rbac and big conversion from EAI to SAI (14 - 30 november) R.I.P EAI :P

UPDATE `creature_template` SET `spell1` = 52497, `spell2` = 52510 WHERE `entry` = 28843;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id` IN (27355,27450);
DELETE FROM `smart_scripts` WHERE `entryorguid`IN(27355,27449,27450) AND `source_type`=0;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry IN(27355,27449,27450);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(27450,0,0,1,8,0,100,0,48790,0,0,0,33,27450,0,0,0,0,0,7,0,0,0,0,0,0,0,'Neltharions Flame Control Bunny - On Spellhit (Neltharions Flame) - Give Kill Credit'),
(27450,0,1,2,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,27449,0,200,0,0,0,0, 'Neltharions Flame Control Bunny - Linked with Previous Event - Set Data 1 1 on Neltharions Flame Fire Bunny'),
(27450,0,2,0,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,27355,0,200,0,0,0,0, 'Neltharions Flame Control Bunny - Linked with Previous Event - Set Data 1 1 on Rothin the Decaying'),
(27449,0,0,1,38,0,100,0,1,1,0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Neltharions Flame Fire Bunny - On Data set 1 1 - Set Data 1 0 on self'),
(27449,0,1,0,61,0,100,0,0,0,0,0,11,48786,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Neltharions Flame Fire Bunny - Linked with Previous Event - Cast Neltharions Flame Fire Bunny: Periodic Fire Aura'),
(27355,0,0,1,25,0,100,0,0,0,0,0,21,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Reset - Disable Combat Movement'),
(27355,0,1,0,61,0,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - Set Phase 0'),
(27355,0,2,3,4,0,100,0,0,0,0,0,11,9613,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Agro - Cast Shadowbolt'),
(27355,0,3,0,61,0,100,0,0,0,0,0,22,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - Set Phase 1'),
(27355,0,4,0,9,1,100,0,0,40,3400,4800,11,9613,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - On Range (Phase 1) - Cast Shadow Bolt'),
(27355,0,5,6,3,1,100,0,0,7,0,0,21,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Less than 7% Mana - Allow Combat Movement'),
(27355,0,6,0,61,1,100,0,0,0,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - Set Phase 2'),
(27355,0,7,0,9,1,100,0,35,80,0,0,21,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Target More than 35 Yards away - Allow Combat Movement'),
(27355,0,8,0,9,1,100,0,5,15,0,0,21,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Target less than 15 Yards away - Disable Combat Movement'),
(27355,0,9,0,9,1,100,0,0,5,0,0,21,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Target less than 5 Yards away - Allow Combat Movement'),
(27355,0,10,0,3,3,100,0,15,100,0,0,22,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On More than 15% Mana - Set Phase 1'),
(27355,0,11,0,0,0,100,0,12000,17000,15000,20000,11,51337,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - IC - On Range (Phase 1) - Cast Shadow Flame'),
(27355,0,12,0,2,0,100,1,0,30,9500,11000,11,51512,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Less than 30% HP - Cast Aegis of Neltharion'),
(27355,0,13,0,7,0,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Evade - Set Phase 0'),
(27355,0,14,0,25,0,100,0,0,0,0,0,18,768,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Reset/Spawn - Disable Combat'),
(27355,0,15,16,38,0,100,0,1,1,0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Data set 1 1 - Set Data 1 0 on self'),
(27355,0,16,17,61,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - Enable Combat'),
(27355,0,17,0,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - Linked with Previous Event - Say'),
(27355,0,18,0,4,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Agro - Say'),
(27355,0,19,0,6,0,100,0,0,0,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rothin the Decaying <Cult of the Damned> - On Death - Say');

DELETE FROM `creature_text` WHERE `entry`=27355;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(27355,0,0,'No... NO! What have you done?! So many ancient wyrms wasted... what magic could do this?',14,0,100,1,0,0,'Rothin the Decaying <Cult of the Damned>'),
(27355,1,0,'Foolish errand $g boy:girl; ... you will die for interrupting my work!',14,0,100,1,0,0,'Rothin the Decaying <Cult of the Damned>'),
(27355,2,0,'This is not the end... death only... strengthens...',14,0,100,1,0,0,'Rothin the Decaying <Cult of the Damned>');

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry`=10541;
DELETE FROM `smart_scripts` WHERE `entryorguid`IN(-23712,-23713,-23714,-23715,-23716,10541) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(-23712,0,0,1,8,0,100,0,16378,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Krakle''s Thermometer - On Spell Hit (Temperature Reading)- Say'),
(-23713,0,0,1,8,0,100,0,16378,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Krakle''s Thermometer - On Spell Hit (Temperature Reading)- Say'),
(-23714,0,0,1,8,0,100,0,16378,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Krakle''s Thermometer - On Spell Hit (Temperature Reading)- Say'),
(-23715,0,0,1,8,0,100,0,16378,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Krakle''s Thermometer - On Spell Hit (Temperature Reading)- Say'),
(-23716,0,0,1,8,0,100,0,16378,0,0,0,33,10541,0,0,0,0,0,7,0,0,0,0,0,0,0,'Krakle''s Thermometer - On Spell Hit (Temperature Reading)- Give Kill Credit'),
(-23716,0,1,0,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Krakle''s Thermometer - Linked with Previous Event - Say');

DELETE FROM `creature_text` WHERE `entry` =10541;

INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES 
(10541, 0, 0, 'It''s 428,000 degrees Kraklenheit... What''s happening, hot stuff?', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer'),
(10541, 0, 1, 'DING! 428,000 degrees Kraklenheit, exactly! Well, approximately. Almost. Somewhere around there...', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer'),
(10541, 0, 2, 'Measuring by Kraklenheit, it is 428,000 dewgrees! That''s Krakley!', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer'),
(10541, 1, 0, 'The temperature is 122 degrees Kraklenheit.', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer'),
(10541, 1, 1, 'The temperature is 9280 degrees Kraklenheit! That''s HOT!', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer'),
(10541, 1, 2, 'Wow, it''s 3 degrees Kraklenheit.  Keep Looking.', 12, 0, 100, 0, 0, 0, 'Krakle''s Thermometer');

UPDATE `smart_scripts` SET `link`=12 WHERE `entryorguid`=8503 AND `source_type`=0 AND `id`=11;
UPDATE `smart_scripts` SET `event_type`=61,`event_param2`=0 WHERE `entryorguid`=8503 AND `source_type`=0 AND `id`=12;
UPDATE `smart_scripts` SET `event_type`=61,`link`=0 WHERE `entryorguid`=8503 AND `source_type`=0 AND `id`=13;

UPDATE `conditions` SET `NegativeCondition`=1 WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=9155 AND `SourceEntry`=0 AND `SourceId`=0 AND `ElseGroup`=0 AND `ConditionTypeOrReference`=2 AND `ConditionTarget`=0 AND `ConditionValue1`=34842 AND `ConditionValue2`=10 AND `ConditionValue3`=0; 
UPDATE `conditions` SET `NegativeCondition`=1 WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=9156 AND `SourceEntry`=0 AND `SourceId`=0 AND `ElseGroup`=0 AND `ConditionTypeOrReference`=2 AND `ConditionTarget`=0 AND `ConditionValue1`=34842 AND `ConditionValue2`=10 AND `ConditionValue3`=0; 

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (25442,25441,25443);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (25442,25441,25443) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(25441,0,0,0,8,0,100,0,45583,0,0,0,33,25441,0,0,0,0,0,7,0,0,0,0,0,0,0,'North Platform - On Spell Hit - Give Quest Credit'),
(25442,0,0,0,8,0,100,0,45583,0,0,0,33,25442,0,0,0,0,0,7,0,0,0,0,0,0,0,'East Platform - On Spell Hit - Give Quest Credit'),
(25443,0,0,0,8,0,100,0,45583,0,0,0,33,25443,0,0,0,0,0,7,0,0,0,0,0,0,0,'West Platform - On Spell Hit - Give Quest Credit');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceGroup`=1 AND `SourceEntry`=57852;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13,1,57852,0,0,31,0,3,30742,0,0,0,0,'','Destroy Altar can hit First Summoning Altar'),
(13,1,57852,0,1,31,0,3,30744,0,0,0,0,'','Destroy Altar can hit Second Summoning Altar'),
(13,1,57852,0,2,31,0,3,30745,0,0,0,0,'','Destroy Altar can hit Third Summoning Altar'),
(13,1,57852,0,3,31,0,3,30950,0,0,0,0,'','Destroy Altar can hit Fourth Summoning Altar');

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (30742,30744,30745,30950);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (30742,30744,30745,30950) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(30742,0,0,0,8,0,100,0,57852,0,0,0,33,30742,0,0,0,0,0,7,0,0,0,0,0,0,0,'First Summoning Altar - On Spell Hit - Give Quest Credit'),
(30744,0,0,0,8,0,100,0,57852,0,0,0,33,30744,0,0,0,0,0,7,0,0,0,0,0,0,0,'Second Summoning Altar - On Spell Hit - Give Quest Credit'),
(30745,0,0,0,8,0,100,0,57852,0,0,0,33,30745,0,0,0,0,0,7,0,0,0,0,0,0,0,'Third Summoning Altar - On Spell Hit - Give Quest Credit'),
(30950,0,0,0,8,0,100,0,57852,0,0,0,33,30950,0,0,0,0,0,7,0,0,0,0,0,0,0,'Fourth Summoning Altar - On Spell Hit - Give Quest Credit');

SET @Reference := 10036;  -- Needs 26 reference loot template entries
DELETE FROM `item_loot_template` WHERE `entry` BETWEEN 51999 AND 52005;
INSERT INTO `item_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
-- Satchel of Helpfull Goods (level 0-25)
(51999,1,100,1,1,-@Reference,1),  -- Cloth
(51999,2,100,1,2,-@Reference-1,1),  -- Leather
(51999,3,100,1,3,-@Reference-2,1),  -- Mail
-- Satchel of Helpfull Goods (level 26-35)
(52000,1,100,1,1,-@Reference-3,1),  -- Cloth
(52000,2,100,1,2,-@Reference-4,1),  -- Leather
(52000,3,100,1,3,-@Reference-5,1),  -- Mail
-- Satchel of Helpfull Goods (level 36-45)
(52001,1,100,1,1,-@Reference-6,1),  -- Cloth
(52001,2,100,1,2,-@Reference-7,1),  -- Leather
(52001,3,100,1,3,-@Reference-8,1),  -- Mail
(52001,4,100,1,4,-@Reference-9,1),  -- Plate
-- Satchel of Helpfull Goods (level 46-55)
(52002,1,100,1,1,-@Reference-10,1), -- Cloth
(52002,2,100,1,2,-@Reference-11,1), -- Leather
(52002,3,100,1,3,-@Reference-12,1), -- Mail
(52002,4,100,1,4,-@Reference-13,1), -- Plate
-- Satchel of Helpfull Goods (level 56-60)
(52003,1,100,1,1,-@Reference-14,1), -- Cloth
(52003,2,100,1,2,-@Reference-15,1), -- leather
(52003,3,100,1,3,-@Reference-16,1), -- Mail
(52003,4,100,1,4,-@Reference-17,1), -- Plate
-- Satchel of Helpfull Goods (level 61-64)
(52004,1,100,1,1,-@Reference-18,1), -- Cloth
(52004,2,100,1,2,-@Reference-19,1), -- leather
(52004,3,100,1,3,-@Reference-20,1), -- mail
(52004,4,100,1,4,-@Reference-21,1), -- plate
-- Satchel of Helpfull Goods (level 65-70)
(52005,1,100,1,1,-@Reference-22,1),  -- Cloth
(52005,2,100,1,2,-@Reference-23,1),  -- leather
(52005,3,100,1,3,-@Reference-24,1),  -- mail
(52005,4,100,1,4,-@Reference-25,1);  -- plate
DELETE FROM `reference_loot_template` WHERE `entry` BETWEEN @Reference AND @Reference+25;
INSERT INTO `reference_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
-- Satchel of Helpfull Goods (level 0-25)
(@Reference,51968,0,1,1,1,1), -- Enumerated Wrap
(@Reference,51994,0,1,1,1,1), -- Tumultuous Cloak
(@Reference+1,51964,0,1,2,1,1), -- Vigorous Belt
(@Reference+1,51994,0,1,2,1,1), -- Tumultuous Cloak
(@Reference+2,51978,0,1,3,1,1), -- Earthbound Girdle
(@Reference+2,51994,0,1,3,1,1), -- Tumultuous Cloak
-- Satchel of Helpfull Goods (level 26-35)
(@Reference+3,51973,0,1,1,1,1), -- Enumerated Handwraps
(@Reference+3,51996,0,1,1,1,1), -- Tumultuous Necklace
(@Reference+4,51965,0,1,2,1,1), -- Vigorous Handguards
(@Reference+4,51996,0,1,2,1,1), -- Tumultuous Necklace
(@Reference+5,51980,0,1,3,1,1), -- Earthbound Handgrips
(@Reference+5,51996,0,1,3,1,1), -- Tumultuous Necklace
-- Satchel of Helpfull Goods (level 36-45)
(@Reference+6,51974,0,1,1,1,1), -- Enumerated Shoulderpads
(@Reference+6,51992,0,1,1,1,1), -- Tumultuous Ring
(@Reference+7,51966,0,1,2,1,1), -- Vigorous Spaulders
(@Reference+7,51992,0,1,2,1,1), -- Tumultuous Ring
(@Reference+8,51976,0,1,3,1,1), -- Earthbound Shoulderguards
(@Reference+8,51992,0,1,3,1,1), -- Tumultuous Ring
(@Reference+9,51984,0,1,4,1,1), -- Stalwart Shoulderpads
(@Reference+9,51992,0,1,4,1,1), -- Tumultuous Ring
-- Satchel of Helpfull Goods (level 46-55)
(@Reference+10,51967,0,1,1,1,1), -- Enumerated Sandals
(@Reference+10,51972,0,1,1,1,1), -- Enumerated Bracers
(@Reference+11,51962,0,1,2,1,1), -- Vigorous Bracers
(@Reference+11,51963,0,1,2,1,1), -- Vigorous Stompers
(@Reference+12,51981,0,1,3,1,1), -- Earthbound Wristguards
(@Reference+12,51982,0,1,3,1,1), -- Earthbound Boots
(@Reference+13,51989,0,1,4,1,1), -- Stalwart Bands
(@Reference+13,51990,0,1,4,1,1), -- Stalwart Treads
-- Satchel of Helpfull Goods (level 56-60)
(@Reference+14,51971,0,1,1,1,1), -- Enumerated Belt
(@Reference+14,51993,0,1,1,1,1), -- Turbulent Cloak
(@Reference+15,51959,0,1,2,1,1), -- Vigorous Belt
(@Reference+15,51993,0,1,2,1,1), -- Turbulent Cloak
(@Reference+16,51977,0,1,3,1,1), -- Earthbound Girdle
(@Reference+16,51993,0,1,3,1,1), -- Turbulent Cloak
(@Reference+17,51985,0,1,4,1,1), -- Stalwart Belt
(@Reference+17,51993,0,1,4,1,1), -- Turbulent Cloak
-- Satchel of Helpfull Goods (level 61-64)
(@Reference+18,51970,0,1,1,1,1), -- Enumerated Gloves
(@Reference+18,51995,0,1,1,1,1), -- Turbulent Necklace
(@Reference+19,51960,0,1,2,1,1), -- Vigorous Gloves
(@Reference+19,51995,0,1,2,1,1), -- Turbulent Necklace
(@Reference+20,51979,0,1,3,1,1), -- Earthbound Grips
(@Reference+20,51995,0,1,3,1,1), -- Turbulent Necklace
(@Reference+21,51987,0,1,4,1,1), -- Stalwart Grips
(@Reference+21,51995,0,1,4,1,1), -- Turbulent Necklace
-- Satchel of Helpfull Goods (level 65-70)
(@Reference+22,51961,0,1,1,1,1), -- Vigorous Shoulderguards
(@Reference+22,51991,0,1,1,1,1), -- Turbulent Signet
(@Reference+23,51969,0,1,2,1,1), -- Enumerated Shoulders
(@Reference+23,51991,0,1,2,1,1), -- Turbulent Signet
(@Reference+24,51975,0,1,3,1,1), -- Earthbound Shoulders
(@Reference+24,51991,0,1,3,1,1), -- Turbulent Signet
(@Reference+25,51983,0,1,4,1,1), -- Stalwart Shoulderguards
(@Reference+25,51991,0,1,4,1,1); -- Turbulent Signet

-- -------------------------------------------------------------------
-- Set some Parameters
-- -------------------------------------------------------------------
SET @Cloth := 400; -- Class Bitmask: 16 (Priest) +128 (Mage) +256 (Warlock)
SET @Leather1 := 1100; -- Class Bitmask: 4 (Hunter) +8 (Rogue) +64 (Shaman) +1024 (Druid)
SET @Leather2 := 1032; -- Class Bitmask: 8 (Rogue) +1024 (Druid)
SET @Mail1 := 3; -- Class Bitmask: 1 (Warrior) +2 (Paladin)
SET @Mail2 := 68; -- Class Bitmask: 4 (Hunter) +8 (Shaman)
SET @Plate := 35; -- Class Bitmask: 1 (Warrior) +2 (Paladin) +32 (DeathKnight)
-- Add conditions to make sure everyone gets beneficial loot for their class
-- -------------------------------------------------------------------
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=5 AND `SourceGroup` BETWEEN 51999 AND 52005;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=10 AND `SourceGroup` BETWEEN @Reference AND @Reference+25;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
-- Cloth Items
(10,@Reference,51968,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Wrap only for clothusers'),
(10,@Reference,51994,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Tumultuous Cloak only for clothusers'),
(10,@Reference+3,51973,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Handwraps only for clothusers'),
(10,@Reference+3,51996,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Tumultuous Necklace only for clothusers'),
(10,@Reference+6,51974,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Shoulderpads only for clothusers'),
(10,@Reference+6,51992,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Tumultuous Ring only for clothusers'),
(10,@Reference+10,51967,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Sandals only for clothusers'),
(10,@Reference+10,51972,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Bracers only for clothusers'),
(10,@Reference+14,51971,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Belt only for clothusers'),
(10,@Reference+14,51993,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Turbulent Cloak only for clothusers'),
(10,@Reference+18,51970,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Gloves only for clothusers'),
(10,@Reference+18,51995,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Turbulent Necklace only for clothusers'),
(10,@Reference+22,51969,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Enumerated Shoulders only for clothusers'),
(10,@Reference+22,51991,0,0,15,0,@Cloth,0,0,0,0,'','SOHG: Turbulent Signet only for clothusers'),
-- Leather Items
(10,@Reference+1,51964,0,0,15,0,@Leather1,0,0,0,0,'','SOHG: Vigorous Belt only for leatherusers'),
(10,@Reference+1,51994,0,0,15,0,@Leather1,0,0,0,0,'','SOHG: Tumultuous Cloak only for leatherusers'),
(10,@Reference+4,51965,0,0,15,0,@Leather1,0,0,0,0,'','SOHG: Vigorous Handguards only for leatherusers'),
(10,@Reference+4,51996,0,0,15,0,@Leather1,0,0,0,0,'','SOHG: Tumultuous Necklace only for leatherusers'),
(10,@Reference+7,51966,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Spaulders only for leatherusers'),
(10,@Reference+7,51992,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Tumultuous ring only for leatherusers'),
(10,@Reference+11,51962,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Bracers only for leatherusers'),
(10,@Reference+11,51963,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Stompers only for leatherusers'),
(10,@Reference+15,51959,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Belt only for leatherusers'),
(10,@Reference+15,51993,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Turbulent Cloak only for leatherusers'),
(10,@Reference+19,51960,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Gloves only for leatherusers'),
(10,@Reference+19,51995,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Turbulent Necklace only for leatherusers'),
(10,@Reference+23,51961,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Vigorous Shoulderguards only for leatherusers'),
(10,@Reference+23,51991,0,0,15,0,@Leather2,0,0,0,0,'','SOHG: Turbulent Signet only for leatherusers'),
-- Mail Items
(10,@Reference+2,51978,0,0,15,0,@Mail1,0,0,0,0,'','SOHG: Earthbound Girdle only for mail users'),
(10,@Reference+2,51994,0,0,15,0,@Mail1,0,0,0,0,'','SOHG: Tumultuous Cloak only for mail users'),
(10,@Reference+5,51980,0,0,15,0,@Mail1,0,0,0,0,'','SOHG: Earthbound Handgrips only for mail users'),
(10,@Reference+5,51996,0,0,15,0,@Mail1,0,0,0,0,'','SOHG: Tumultuous Necklace only for Mail users'),
(10,@Reference+8,51976,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Shoulderguards only for mail users'),
(10,@Reference+8,51992,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Tumultuous Ring only for mail users'),
(10,@Reference+12,51982,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Boots only for mail users'),
(10,@Reference+12,51981,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Wristguards only for mail users'),
(10,@Reference+16,51977,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Girdle only for mail users'),
(10,@Reference+16,51993,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Turbulent Cloak only for mail users'),
(10,@Reference+20,51979,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Grips only for mail users'),
(10,@Reference+20,51995,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Turbulent Necklace only for mail users'),
(10,@Reference+24,51975,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Earthbound Shoulders only for mail users'),
(10,@Reference+24,51991,0,0,15,0,@Mail2,0,0,0,0,'','SOHG: Turbulent Signet only for Mail users'),
-- Plate Items
(10,@Reference+9,51984,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Shoulderpads only for plate users'),
(10,@Reference+9,51992,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Tumultuous Ring only for plate users'),
(10,@Reference+13,51989,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Bands only for plate users'),
(10,@Reference+13,51990,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Treads only for plate users'),
(10,@Reference+17,51985,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Belt only for plate users'),
(10,@Reference+17,51993,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Turbulent Cloak only for plate users'),
(10,@Reference+21,51987,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Grips only for plate users'),
(10,@Reference+21,51995,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Turbulent Necklace only for plate users'),
(10,@Reference+25,51983,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Stalwart Shoulderguards only for plate users'),
(10,@Reference+25,51991,0,0,15,0,@Plate,0,0,0,0,'','SOHG: Turbulent Signet only for plate users');

DELETE FROM `reference_loot_template` WHERE `entry`=11112;
UPDATE `reference_loot_template` SET `entry`=11114 WHERE `item`= 34831 AND `entry`=11115;
DELETE FROM `item_loot_template` WHERE `entry`=35348;
INSERT INTO `item_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
(35348,1,100,1,0,-11113,1), -- Garanteed Drops
(35348,2, 60,1,0,-11116,1), -- Rare Pets
(35348,4, 60,1,0,-11115,1), -- Junk items
(35348,3,  5,1,0,-11114,1); -- Lesser Treasures

-- DB/Quest: Fix: [A/H] The Summoning
-- Note: This warrior quest was deleted in 4.0.1.
SET @ENTRY := 6176; -- Bath'rah the Windwatcher
DELETE FROM `waypoints` WHERE `entry` = @ENTRY AND `pointid` BETWEEN 8 AND 14;
UPDATE `smart_scripts` SET `event_type` = 40 WHERE `entryorguid` = @ENTRY AND `id` = 1;

UPDATE `creature_template_addon` SET `mount`=25678, `bytes2`=0x1 WHERE `entry`=37845;
UPDATE `smart_scripts` SET `action_param1`=45492, `comment`='Quel''Delar Skull Target - on spell hit - Cast Shadow Nova' WHERE `entryorguid`=37852 AND `source_type`=0 AND `id`=1 AND `link`=0;

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(10977,10978,7583);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (10977,10978,7583);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(10977, 0, 0, 0, 8,  0, 100, 0, 17166, 0, 0, 0, 33, 10977 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Quixxil - On Spellhit (Release Umis Yeti) - Kill Credit'),
(10978, 0, 0, 0, 8,  0, 100, 0, 17166, 0, 0, 0, 33, 10978 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Legacki - On Spellhit (Release Umis Yeti)- Kill Credit'),
(7583, 0, 0, 0, 8,  0, 100, 0, 17166, 0, 0, 0, 33, 7583 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Sprinkle - On Spellhit (Release Umis Yeti)- Kill Credit');

UPDATE `creature_template` SET `npcflag`=4227 WHERE `entry`=38316;

DELETE FROM `gossip_menu_option` WHERE `menu_id`=10996 AND `id`=5;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES 
(10996, 5, 1, 'Show me the armor of Scourge lords, Ormus.', 3, 128, 0, 0, 0, 0, '');

DELETE FROM `conditions` WHERE  `SourceTypeOrReferenceId`=15 AND `SourceGroup`=10996 AND `SourceEntry`=5;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(15, 10996, 5, 0, 0, 15, 0, 32, 0, 0, 0, 0, 0, '', 'Ormus the Penitent - Show gossip option if player is a Death Knight');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=48188;

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(13, 1, 48188, 0, 0, 31, 0, 3, 27349, 0, 0, 0, 0, '', 'Flask of Blight Targets Scarlet Onslaught Prisoner');

DELETE FROM `disables` WHERE `sourceType`=0 AND `entry`=48188;

INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES 
(0, 48188, 64, '', '', 'Ignore LOS on Flask of Blight');

UPDATE `gameobject_template` SET `AIName`='SmartGameObjectAI', `ScriptName`='' WHERE  `entry`=184725;
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(21039,21898,20767,21504) ;

DELETE FROM `smart_scripts` WHERE `source_type`=1 AND `entryorguid`=184725;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`IN(21039,21898,20767,21504);
DELETE FROM `smart_scripts` WHERE `source_type`=9 AND `entryorguid`=2103900;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(184725, 1, 0 ,1, 70, 0, 100, 0, 2, 0, 0,0,45,1,1,0,0,0,0,10,73864,21039,0,0,0,0,0, 'Mana Bomb - On State Changed - Set Data Mana Bomb Kill Credit Trigger'),
(184725, 1, 1 ,2, 61, 0, 100, 0, 0, 0, 0,0,33,21039,0,0,0,0,0,16,0,0,0,0,0,0,0, 'Mana Bomb - Linked with Previous Event - Quest Credit'),
(184725, 1, 2 ,0, 61, 0, 100, 0, 0, 0, 0,0,45,1,1,0,0,0,0,9,16769,0,50,0,0,0,0, 'Mana Bomb - Linked with Previous Event - Set Data'),
--
(21039, 0, 0 ,1, 38, 0, 100, 0, 1, 1, 0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Data Set - Set Data'),
(21039, 0, 1 ,2, 61, 0, 100, 0, 0, 0, 0,0,1,0,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Say'),
(21039, 0, 2 ,0, 61, 0, 100, 0, 0, 0, 0,0,80,2103900,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Run Script'),
(21039, 0, 3 ,4, 52, 0, 100, 0, 0, 21039, 0,0,1,1,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Text Over - Say'),
(21039, 0, 4 ,0, 61, 0, 100, 0, 0, 0, 0,0,80,2103900,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Run Script'),
(21039, 0, 5 ,6, 52, 0, 100, 0, 1, 21039, 0,0,1,2,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Text Over Event - Say'),
(21039, 0, 6 ,0, 61, 0, 100, 0, 0, 0, 0,0,80,2103900,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Run Script'),
(21039, 0, 7 ,8, 52, 0, 100, 0, 2, 21039, 0,0,1,3,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Text Over - Say'),
(21039, 0, 8 ,0, 61, 0, 100, 0, 0, 0, 0,0,80,2103900,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Run Script'),
(21039, 0, 9 ,10, 52, 0, 100, 0, 3, 21039, 0,0,1,4,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Text Over - Say'),
(21039, 0, 10 ,0, 61, 0, 100, 0, 0, 0, 0,0,80,2103900,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Kill Credit Trigger - Linked with Previous Event - Run Script'),
--
(21039, 0, 12 ,0, 52, 0, 100, 0, 4, 21039, 0,0,45,1,1,0,0,0,0,9,20767,0,200,0,0,0,0, 'Mana Bomb Kill Credit Trigger - On Text Over - Set Data Mana Bomb Explosion Trigger'),
(20767, 0, 0 ,1, 38, 0, 100, 0, 1, 1, 0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Explosion  Trigger - On Data Set - Set Data'),
(20767, 0, 1 ,2, 61, 0, 100, 0, 0, 0, 0,0,11,35513,0,0,0,0,0,1,0,0,0,0, 0, 0, 0, 'Mana Bomb Explosion  Trigger - Linked with Previous Event - Cast Mana Bomb Explosion'),
--
(21898, 0, 0 ,1, 38, 0, 100, 0, 1, 1, 0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Mana Bomb Lightning  Trigger - On Data Set - Set Data'),
(21898, 0, 1 ,0, 61, 0, 100, 0, 0, 0, 0,0,11,37843,0,0,0,0,0,9,21899,0,200,0, 0, 0, 0, 'Mana Bomb Lightning  Trigger - Linked with Previous Event - Cast Mana Bomb Lightning'),
(2103900, 9, 0 ,0, 0, 0, 100, 0, 0, 0, 0,0,45,1,1,0,0,0,0,9,21898,0,200,0, 0, 0, 0, 'Mana Bomb - Script - Set Data');


DELETE FROM `creature_text` WHERE `entry` IN(21039,18554,16769);
DELETE FROM `creature_text` WHERE `entry` =21504 AND `groupid`>3;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(21039,0,0,'5...',41,0,100,0,0,0,'Mana Bomb'),
(21039,1,0,'4...',41,0,100,0,0,0,'Mana Bomb'),
(21039,2,0,'3...',41,0,100,0,0,0,'Mana Bomb'),
(21039,3,0,'2...',41,0,100,0,0,0,'Mana Bomb'),
(21039,4,0,'1...',41,0,100,0,0,0,'Mana Bomb'),
(18554,0,0,'You come into my house and threaten ME? I think not!',12,0,100,0,0,0,'Sharth Voldoun'),
(18554,1,0,'All goes exceedingly well, my lord. Testing of the smaller prototype at the Cenarion Thicket was a complete success. The second bomb is being ritually fueled in the courtyard below even as we speak. And, I''ve sent a courier to Tuurem to bring the rest of the parts to us here.',12,0,100,0,0,0,'Sharth Voldoun'),
(18554,2,0,'You are satisfied?',12,0,100,0,0,0,'Sharth Voldoun'),
(18554,3,0,'I can assure you that we will not fail, my master. I am personally overseeing every aspect of the construction, and I hold the final codes, myself. Within a day''s time, I will have the bomb detonated on those nearby pests.',12,0,100,0,0,0,'Sharth Voldoun'),
(16769,0,0,'Knowing there isn''t enough time, the Firewing Warlock doesn''t even try to run.', 16,0,100,0,0,0,'Firewing Warlock'),
--
(21504,4,0,'For the time being, yes. However, allow my presence to be a motivator. Prince Kael''thas was displeased with the failure of the crystal experiment on Fallen Sky Ridge. This is one of the reasons for why we chose the Cenarion druids as the testing grounds for the bomb.',12,0,100,0,0,0,'Pathaleon the Calculators Image'),
(21504,5,0,'I need not tell you what will happen should the mana bomb down in the courtyard fail to be used on its target soon? Since moving into the forest, they''ve become increasingly annoying to our operations: here, at Tuurem and to the south at the Bonechewer Ruins.',12,0,100,0,0,0,'Pathaleon the Calculators Image'),
(21504,6,0,'I think that we should teach a lesson to both the Horde and the Alliance. One that they will not soon forget!',12,0,100,0,0,0,'Pathaleon the Calculators Image'),
(21504,7,0,'See to it that you do, Sharth, or I will personally see to your slow torture and death.',12,0,100,0,0,0,'Pathaleon the Calculators Image'),
(21504,8,0,'I believe I may recognize them. Deal with this quickly, Sharth. Then take the mana bomb and destroy their town!',12,0,100,0,0,0,'Pathaleon the Calculators Image');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=35958;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 3, 35958, 0, 0, 31, 0, 3, 16769, 0, 0, 0, '','Mana Bomb Explosion Targets Firewing Warlock'),
(13, 3, 35958, 0, 1, 31, 0, 3, 5355, 0, 0, 0, '','Mana Bomb Explosion Targets Firewing Defender');

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`= 18554;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=18554;
DELETE FROM `smart_scripts` WHERE `entryorguid`=18554;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(21504,0,0,0,38,0,100,0,1,1,0,0,41,5000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Pathaleon the Calculators Image - On Data Set - Despawn'),
--
(18554,0,0,0,0,0,100,0,3000,5000,40000,45000,11,15277,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - IC - Cast    Seal of Reckoning'),
(18554,0,1,0,2,0,100,0,0,40,15000,20000,11,13952,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - On Below 40% HP - Cast Holy Light'),
(18554,0,2,3,4,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,2000,0,0,0,0,0, 'Sharth Voldoun - On Agro - Say'),
(18554,0,3,4,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - Linked with Previous Event - Despawn Pathaleon the Calculators Image'),
(18554,0,4,0,61,0,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - Linked with Previous Event - Set Phase 0'),
--
(18554,0,5,6,25,0,100,0,0,0,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - On Reset - Set Phase 2'),
(18554,0,6,0,61,0,100,0,0,0,0,0,12,21504,8,0,0,0,0,8,0,0,0,-2281.936523,3099.178711,152.817734,3.699372, 'Sharth Voldoun - Linked with Previous Event - Spawn Pathaleon the Calculators Image'),
(18554,0,7,0,10,2,100,0,1,200,60000,60000,1,1,6000,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - Linked with Previous Event - Spawn Pathaleon the Calculators Image'),
(18554,0,8,0,52,2,100,0,1,18554,0,0,1,2,3000,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,9,0,52,2,100,0,2,18554,0,0,1,4,6000,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,10,0,52,2,100,0,4,21504,0,0,1,5,6000,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,11,0,52,2,100,0,5,21504,0,0,1,6,6000,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,12,0,52,2,100,0,6,21504,0,0,1,3,6000,0,0,0,0,1,0,0,0,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,13,0,52,2,100,0,3,18554,0,0,1,7,6000,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say'),
(18554,0,14,0,52,0,100,0,0,18554,0,0,1,8,6000,0,0,0,0,9,21504,0,200,0,0,0,0, 'Sharth Voldoun - On Text Over Event - Say');

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`= 16769;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=16769;
DELETE FROM `smart_scripts` WHERE `entryorguid`=16769;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(16769,0,0,0,25,0,100,0,0,0,0,0,21,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Reset - Prevent Combat Movement'),
(16769,0,1,2,4,0,100,0,0,0,0,0,11,9613,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Firewing Warlock - On Agro - Cast Shadow Bolt'),
(16769,0,2,0,61,0,100,0,0,0,0,0,22,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - Linked with Previous Event - Set Phase 1'),
(16769,0,3,0,9,1,100,0,0,40,2400,3800,11,9613,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Firewing Warlock - On Range - Cast Shadow Bolt'),
(16769,0,4,5,3,1,100,0,0,15,0,0,21,1,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Firewing Warlock - On Less than 15% Mana - Allow Combat Movement'),
(16769,0,5,0,61,1,100,0,0,0,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - Linked with Previous Event - Set Phase 2'),
(16769,0,6,0,9,1,100,0,35,80,0,0,21,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Range - Allow Combat Movement'),
(16769,0,7,0,9,1,100,0,5,15,0,0,21,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Range - Prevent Combat Movement'),
(16769,0,8,0,9,1,100,0,0,5,0,0,21,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Range - Allow Combat Movement'),
(16769,0,9,0,3,2,100,0,30,0,0,0,22,1,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Firewing Warlock - On More than 30% Mana - Set Phase 1'),
(16769,0,10,0,0,0,100,0,5000,9000,25000,35000,11,33483,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - IC - Cast Mana Tap'),
(16769,0,11,0,0,0,100,0,9000,15000,15000,20000,11,33390,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Firewing Warlock - IC - Cast Arcane Torrent'),
(16769,0,12,0,0,0,100,0,3000,5000,18000,24000,11,11962,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Firewing Warlock - IC - Cast Immolate'),
(16769,0,13,0,2,0,100,1,0,30,0,0,11,32932,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Less than 30% HP - Cast Sun Shield'),
(16769,0,14,0,2,0,100,1,0,15,0,0,25,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Less than 15% HP - Flee for Assist'),
(16769,0,15,0,7,0,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Evade - Set Phase 0'),
(16769,0,16,0,38,0,100,0,1,1,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Firewing Warlock - On Data Set - Say');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=22 AND `SourceEntry`=16769;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(22, 17, 16769, 0, 0, 36, 1, 0, 0, 0, 0, 0, '','Only execute SAI if firewing warlock alive');

UPDATE `quest_template` SET `ExclusiveGroup`=13104 WHERE  `Id` IN (13104,13105);
UPDATE `quest_template` SET `PrevQuestId`=0 WHERE  `Id`IN (13110,13122,13118,13125);
UPDATE `quest_template` SET `RequiredClasses`=1503 WHERE  `Id`=13104;
UPDATE `quest_template` SET `RequiredClasses`=32 WHERE  `Id`=13105;
DELETE FROM  `conditions` WHERE `SourceEntry` IN (13122,13110,13118,13125);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(20, 0, 13110, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Restless Dead after Once More Unto The Breach, Hero'),
(19, 0, 13110, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Restless Dead after Once More Unto The Breach, Hero'),
(20, 0, 13110, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Restless Dead after Once More Unto The Breach, Hero'),
(19, 0, 13110, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Restless Dead after Once More Unto The Breach, Hero'),
(20, 0, 13122, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Scourgestone after Once More Unto The Breach, Hero'),
(19, 0, 13122, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Scourgestone after Once More Unto The Breach, Hero'),
(20, 0, 13122, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Scourgestone after Once More Unto The Breach, Hero'),
(19, 0, 13122, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Scourgestone after Once More Unto The Breach, Hero'),
(20, 0, 13118, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Purging Of Scourgeholme  after Once More Unto The Breach, Hero'),
(19, 0, 13118, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Purging Of Scourgeholme  after Once More Unto The Breach, Hero'),
(20, 0, 13118, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Purging Of Scourgeholme  after Once More Unto The Breach, Hero'),
(19, 0, 13118, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Purging Of Scourgeholme  after Once More Unto The Breach, Hero'),
(20, 0, 13125, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Air Stands Still  after Once More Unto The Breach, Hero'),
(19, 0, 13125, 0, 0, 8, 0, 13104, 0, 0, 0, 0, '', 'The Air Stands Still  after Once More Unto The Breach, Hero'),
(20, 0, 13125, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Air Stands Still  after Once More Unto The Breach, Hero'),
(19, 0, 13125, 0, 1, 8, 0, 13105, 0, 0, 0, 0, '', 'The Air Stands Still  after Once More Unto The Breach, Hero');

-- Shattertusk Bull
SET @ENTRY      := 28380;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28380;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,2000,5000,5000,8000,11,51944,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Shattertusk Bull - In Combat - Cast Trample"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,7000,10000,13000,16000,11,55196,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Shattertusk Bull - In Combat - Cast Stomp");

-- Dreadsaber
SET @ENTRY      := 28001;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28001;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,3000,6000,5000,7000,11,24187,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Dreadsaber - In Combat - Cast Claw");

-- Shardhorn Rhino
SET @ENTRY      := 28009;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28009;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,55193,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Shardhorn Rhino - On Aggro - Cast Rhino Charge"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,5000,9000,7000,12000,11,32019,32,0,0,0,0,4,0,0,0,0.0,0.0,0.0,0.0,"Shardhorn Rhino - In Combat - Cast Gore");

-- Shango
SET @ENTRY      := 28297;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28297;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,5000,9000,7000,12000,11,32019,32,0,0,0,0,4,0,0,0,0.0,0.0,0.0,0.0,"Shango - In Combat - Cast Gore");

-- Mangal Crocolisk
SET @ENTRY      := 28002;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28002;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,11,50502,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Mangal Crocolisk - On Aggro - Cast Thick Hide"),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,3000,6000,6000,9000,11,48287,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Mangal Crocolisk - In Combat - Cast Powerfull Bite");

-- Emperor Cobra
SET @ENTRY      := 28011;
SET @SOURCETYPE := 0;

DELETE FROM `creature_ai_scripts` WHERE  `creature_id`=28011;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,@SOURCETYPE,0,1,1,0,100,0,0,0,0,0,21,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - On Spawn - Prevent Combat"),
(@ENTRY,@SOURCETYPE,1,0,61,0,100,0,0,0,0,0,22,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Link - Set Phase to 0 on Spawn"),
(@ENTRY,@SOURCETYPE,2,3,4,0,100,0,0,0,0,0,11,32093,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - On Aggro - Cast Poison"),
(@ENTRY,@SOURCETYPE,3,0,61,0,100,0,0,0,0,0,23,1,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - On Link - Set Phase 1"),
(@ENTRY,@SOURCETYPE,4,0,9,1,100,0,0,40,3400,4800,11,32093,32,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Cast Poison Spit (Phase 1)"),
(@ENTRY,@SOURCETYPE,5,0,9,1,100,0,35,80,0,0,21,1,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Start Combat Movement at 35 Yards (Phase 1)"),
(@ENTRY,@SOURCETYPE,6,0,9,1,100,0,5,15,0,0,21,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Prevent Combat Movement at 15 Yards (Phase 1)"),
(@ENTRY,@SOURCETYPE,7,0,9,1,100,0,0,5,0,0,21,1,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Start Combat Movement Below 5 Yards (Phase 1)"),
(@ENTRY,@SOURCETYPE,8,0,7,0,100,0,0,0,0,0,22,0,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"Emperor Cobra - Set Phase to 0 on Evade");

UPDATE `smart_scripts` SET `action_type`=85, `action_param2`=2 WHERE  `entryorguid`=32588 AND `source_type`=0 AND `id`=5;

UPDATE `smart_scripts` SET `event_flags` = 1 WHERE `entryorguid` IN (16325,16326) AND `source_type` = 0 AND `id` = 0 AND `link` = 1;

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (18305,18306,18307);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (18305,18306,18307) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(18305,0,0,0,8,0,100,0,32205,0,0,0,33,18305,0,0,0,0,0,7,0,0,0,0,0,0,0,'Burning Blade Pyre (01) - On Spell Hit (Place Maghar Battle Standard)- Give Quest Credit'),
(18306,0,0,0,8,0,100,0,32205,0,0,0,33,18306,0,0,0,0,0,7,0,0,0,0,0,0,0,'Burning Blade Pyre (02) - On Spell Hit (Place Maghar Battle Standard)- Give Quest Credit'),
(18307,0,0,0,8,0,100,0,32205,0,0,0,33,18307,0,0,0,0,0,7,0,0,0,0,0,0,0,'Burning Blade Pyre (03) - On Spell Hit (Place Maghar Battle Standard)- Give Quest Credit');

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry`=23439;
DELETE FROM `smart_scripts` WHERE `entryorguid`= 23439 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(23439,0,0,1,38,0,100,0,1,1,0,0,45,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Hungry Nether Ray - On Data set - Set Data'),
(23439,0,1,0,61,0,100,0,0,0,0,0,11,41427,0,0,0,0,0,23,0,0,0,0,0,0,0,'Hungry Nether Ray - Linked with Previous Event - Cast Lucille Feed Credit Trigger');

DELETE FROM  `smart_scripts`  WHERE  `entryorguid`=23219 AND  `id`=6;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(23219, 0, 6, 0, 6, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 9, 23439, 0, 50, 0, 0, 0, 0, 'Blackwind Warp Chaser - On Death - Set Data Hungry Nether Ray');

-- Fix I've Got a Flying Machine
-- Steel Gate Chief Archaeologist SAI & Text & Condition
SET @ENTRY			:= 24399;
SET @ENTRY1			:= 24418;
SET @ENTRY2			:= 24439;
SET @ENTRY3			:= 24438;
SET @STALKER		:= 105997;
SET @MENUID			:= 8954;
SET @OPTION			:= 0;

DELETE FROM `creature_template_addon` WHERE `entry`=24418;
INSERT INTO `creature_template_addon` (`entry`,`path_id`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
(24418, 0, 0, 0, 0x1, 0x1, '43775 43889'); -- Steel Gate Flying Machine - Flight Drop Off Buff

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY; 
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,1,62,0,100,0,@MENUID,@OPTION,0,0,11,45973,1,0,0,0,0,7,0,0,0,0,0,0,0, 'Steel Gate Chief Archaeologist - On gossip option select - Cast spell'),
(@ENTRY,0,1,2,61,0,100,0,0,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Steel Gate Chief Archaeologist - On Link - Close gossip'),
(@ENTRY,0,2,0,61,0,100,0,0,0,0,0,11,43767,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Steel Gate Chief Archaeologist - On Link - Cast Invoker'),
(@ENTRY,0,3,4,19,0,100,0,11390,0,0,0,11,45973,1,0,0,0,0,7,0,0,0,0,0,0,0, 'Steel Gate Chief Archaeologist - On Quest Accept - Cast spell'),
(@ENTRY,0,4,0,61,0,100,0,0,0,0,0,11,43767,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Steel Gate Chief Archaeologist - On Link - Cast Invoker'),
(@ENTRY,0,5,6,19,0,100,0,11390,0,0,0,12,@ENTRY3,3,120000,0,0,0,8,0,0,0,1972.773,-3265.381,134.719,0, 'Steel Gate Chief Archaeologist - On Quest Accept - Summon Graple Target');

DELETE FROM `spell_area` WHERE `spell`=43889;
INSERT INTO `spell_area` (`spell`, `area`, `quest_start`, `quest_end`, `aura_spell`, `racemask`, `gender`, `autocast`, `quest_start_status`, `quest_end_status`) VALUES 
(43889, 3999, 11390, 0, 0, 0, 2, 1, 8, 0);

UPDATE `creature_template` SET `modelid1`=11686, `modelid2`=0, `AIName`= 'SmartAI', `type_flags`=1048576 WHERE `entry`=@ENTRY3;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY3; 
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY3,0,0,0,54,0,100,0,0,0,0,0,11,43890,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Grapple Target - Just Summoned - Cast Invisibility on self');

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry`= @ENTRY1;
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES 
(@ENTRY1, 43768, 1, 0); 

-- Gossip conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=@MENUID AND `SourceEntry`=@OPTION;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(15,@MENUID,@OPTION,0,9,11390,0,0,0,'','Show gossip option 0 if player has quest I''ve got a Flying Machine');

DELETE FROM `conditions` WHERE `SourceEntry`=@ENTRY1;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(16, 0, @ENTRY1, 0, 23, 3999, 0, 0, 0, '', 'Dismount player when not in intended zone');
-- Condition for Grappling Hook spell(43770)
DELETE FROM `conditions` WHERE `SourceEntry`=43770;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13, 1, 43770, 0, 31, 3, 24439, 0, 0, '', 'Spell 43770(Grappling Hook) targets npc 24439(Sack of Relics)');
-- Spell Conditions
DELETE FROM `conditions` WHERE `SourceEntry`IN (43891,43892,43789);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13, 1, 43892, 0, 31, 3, 24439, 0, 0, '', 'Spell 43892 targets npc 24439'),
(13, 1, 43891, 0, 29, @ENTRY2, 1, 0, 0, '', 'Spell 43892 targets npc 24439'),
(13, 1, 43789, 0, 31, 3, 24439, 0, 0, '', 'Spell 43892 targets npc 24439');

-- Sack of Relics SAI
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY2 AND `source_type`=0;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=@ENTRY2;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY2,0,0,0,8,0,100,0,43770,0,0,0,11,46598,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,'Sack of Relics - On Link - Mount Sack to vehicle'),
(@ENTRY2,0,1,2,8,0,100,1,43892,0,0,0,11,46598,0,0,0,0,0,10,105997,15214,0,0.0,0.0,0.0,0.0,' Sack of Relics - Remove Vehicle - In range'),
(@ENTRY2,0,2,3,61,0,100,1,0,0,0,0,11,36553,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,'Sack of Relics - On Link - Cast pet stay(36553)'),
(@ENTRY2,0,3,4,61,0,100,1,0,0,0,0,33,24439,0,0,0,0,0,21,20,0,0,0.0,0.0,0.0,0.0,'Sack of Relics - On Link - Quest Credit'),
(@ENTRY2,0,4,0,61,0,100,1,0,0,0,0,41,10000,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,'Sack of Relics - On Link - Despawn');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=43770;
INSERT INTO `spell_linked_spell` (`spell_trigger`,`spell_effect`,`type`,`comment`) VALUES
(43770,43789,1,'Trigger grip beam');

DELETE FROM `smart_scripts` WHERE `entryorguid`=-@STALKER AND `source_type`=0;
UPDATE `creature_template` SET AIName='SmartAI' WHERE entry=15214;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(-@STALKER,0,0,0,1,0,100,0,0,0,0,0,11,43892,0,0,0,0,0,11,@ENTRY2,10,0,0.0,0.0,0.0,0.0,'Invisible Stalker - OOC - Cast Spell');

DELETE FROM `disables` WHERE `entry` =56940;
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES
(0,56940,64,'','','Disable LOS check for Thorim Story Kill Credit');

UPDATE `creature_template` SET `AIname`='SmartAI' WHERE `entry`=28389;
DELETE FROM `smart_scripts` WHERE `entryorguid`=28389 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(28389,0,0,0,8,0,100,0,51592,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Primordial Hatchling - On Spel Hit(Pickup Primordial Hatchling) - Despawn');
-- Creature_AI to Smart_AI Conversion for Primordial Drake Egg (ID 28408) and loot fix for Primordial Hatchling (ID 28389)
UPDATE `creature_template` SET `AIName`= 'SmartAI',`flags_extra`=`flags_extra`|2 WHERE `entry`  =28408;
DELETE FROM `creature_ai_scripts` WHERE `creature_id` =28408;
-- Smart AI conversion for Primordial Egg creature ai scripts, also prevents these eggs from moving/auto attacking
DELETE FROM `smart_scripts` WHERE `entryorguid`  =28408;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(28408,0,0,1,25,0,100,0,0,0, 0, 0, 21 ,0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Primordial Drake Egg - On Spawn - Prevent Combat Movement"),
(28408,0,1,0,61,0,100,0,0,0, 0, 0, 20 ,0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Primordial Drake Egg - Linked with Previous Event - Disable Combat"),
(28408,0,2,0,6 ,0,100,0,0,0, 0, 0, 11 ,51595, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, "Primordial Drake Egg - On Death - Cast Summon Primordial Hatchling");
-- Remove Borean Leather, Icy Dragonscale and Book of Glyph Mastery from Primordial Hatchling as these are not meant to drop from these
DELETE FROM `creature_loot_template` WHERE `entry`=28389 AND `item` IN (33568,38557,45912);

DELETE FROM `game_graveyard_zone` WHERE `id`=631 AND `ghost_zone`=2159;
UPDATE `game_graveyard_zone` SET `id`=1265, `faction`=0 WHERE `ghost_zone`=2159; -- Dustwallow Marsh, Mudsprocket GY

DELETE FROM `gossip_menu` WHERE (`entry`=9578 AND `text_id`=12926) OR (`entry`=4825 AND `text_id`=5881) OR (`entry`=8891 AND `text_id`=11645);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
(9578, 12926), -- 27705
(4825, 5881), -- 5957
(8891, 11645); -- 186267

DELETE FROM `gossip_menu_option` WHERE (`menu_id`=9578 AND `id`=0) OR (`menu_id`=4825 AND `id`=0) OR (`menu_id`=8891 AND `id`=0);
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `box_coded`, `box_money`, `box_text`) VALUES
(9578, 0, 3, 'Please teach me.', 0, 0, ''), -- 27705
(4825, 0, 3, 'Please teach me.', 0, 0, ''), -- 5957
(8891, 0, 0, 'Call the Headless Horseman.', 0, 0, ''); -- 186267

UPDATE `creature_template` SET `gossip_menu_id`=9578 WHERE `entry`=27705;
UPDATE `creature_template` SET `gossip_menu_id`=4825 WHERE `entry`=5957;

DELETE FROM `gameobject_template` WHERE `entry` IN (180609, 180610, 180611, 184633);
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `size`, `WDBVerified`) VALUES
(180609, 7, 39, 'Doodad_GeneralChairLoEnd02', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 17359), -- -Unknown-
(180610, 7, 39, 'Doodad_GeneralChairLoEnd04', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 17359), -- -Unknown-
(180611, 7, 39, 'Doodad_GeneralChairLoEnd05', '', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 17359), -- -Unknown-
(184633, 8, 233, 'Forge', '', '', '', 3, 10, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 17359); -- -Unknown-

-- Fixes Ormorok the Tree Shaper Spell Reflect
DELETE FROM `spell_proc_event` WHERE (`entry` = '47981');
INSERT INTO `spell_proc_event` (`entry`,`procEx`) VALUES ('47981','2048');

-- 12180   The Captive Prospectors
UPDATE `smart_scripts` SET `link`=1 WHERE  `entryorguid`IN(27113,27114,27115) AND `source_type`=0 AND `id`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`IN(27113,27114,27115) AND  `id`=1;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(27113,0,1,0,61,0,100,0,0,0,0,0,33,27113,0,0,0,0,0,7,0,0,0,0,0,0,0,'Prospector Gann - On Spell Hit - Give Kill Credit'),
(27114,0,1,0,61,0,100,0,0,0,0,0,33,27114,0,0,0,0,0,7,0,0,0,0,0,0,0,'Prospector Torgan - On Spell Hit - Give Kill Credit'),
(27115,0,1,0,61,0,100,0,0,0,0,0,33,27115,0,0,0,0,0,7,0,0,0,0,0,0,0,'Prospector Veranna - On Spell Hit - Give Kill Credit');

-- [11150] Raze Direhorn Post!
-- [11205] Raze Direhorn Post!
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(23751,23752,23753);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (23751,23752,23753);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(23751, 0, 0, 0, 8,  0, 100, 0, 42356, 0, 0, 0, 33, 23751   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'North Tent - On Spellhit - Kill Credit'),
(23752, 0, 0, 0, 8,  0, 100, 0, 42356, 0, 0, 0, 33, 23752   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Northeast Tent - On Spellhit - Kill Credit'),
(23753, 0, 0, 0, 8,  0, 100, 0, 42356, 0, 0, 0, 33, 23753   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'East Tent - On Spellhit - Kill Credit');
-- Know Your Ley Lines [11547]
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN (25156,25154,25157);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (25156,25154,25157) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(25156,0,0,0,8,0,100,0,45191,0,0,0,33,25156,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Portal - On Spell Hit(Sample Ley Line Field) - Give Quest Credit'),
(25154,0,0,0,8,0,100,0,45191,0,0,0,33,25154,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Shrine - On Spell Hit(Sample Ley Line Field) - Give Quest Credit'),
(25157,0,0,0,8,0,100,0,45191,0,0,0,33,25157,0,0,0,0,0,7,0,0,0,0,0,0,0,'Sunwell - Quest Bunny - Sunwell - On Spell Hit(Sample Ley Line Field) - Give Quest Credit');

UPDATE `smart_scripts` SET `link`=2,`action_type`=33,`action_param1`=29692,`target_type`=7,`comment`='Hut Fire - On Spell Hit - Give Quest Credit' WHERE `entryorguid`=29692 AND `source_type`=0 AND `id`=1 AND `link`=0; 
DELETE FROM `smart_scripts` WHERE `entryorguid`=29692 AND `id`=2 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `id`, `event_type`, `action_type`, `target_type`, `comment`) VALUES
(29692,2,61,41,1, 'Hut Fire - Link With Event 1 - Despawn'); 

DELETE FROM `smart_scripts` WHERE `entryorguid`=29884 AND `id`=7;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(29884, 0, 7, 0, 0, 0, 100, 0, 6000, 10000, 6000, 10000, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Gymer - IC - Say 1');

UPDATE `creature_template` SET `faction_A`=35, `faction_H`=35 WHERE  `entry`=29884;

-- Remove unused skinning_loot_template.
DELETE FROM `skinning_loot_template` WHERE `entry`=100015;
-- Spell effects without target types AREA/NEARBY/CONE so this conditions are useless. 
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `sourcegroup`=1 AND `sourceentry` IN (43789,43891);
-- Must have the same loot as item 35348. In TDB it uses reference 11112 which is empty.
DELETE FROM `item_loot_template` WHERE `entry`=34863;
INSERT INTO `item_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
(34863,1,100,1,0,-11113,1),
(34863,2,60,1,0,-11116,1),
(34863,3,5,1,0,-11114,1),
(34863,4,60,1,0,-11115,1);

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry`=15941;
DELETE FROM `creature_text` WHERE `entry` IN (15945,15941);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(15945,0,0,'You can''t do this to me! We had a deal!',12,0,0,0,0,0,''),
(15941,0,0,'What? Oh, not this again!',12,0,0,0,0,0,'');
DELETE FROM `smart_scripts` WHERE `entryorguid`=15945 AND `source_type`=0 AND `id` IN (14,15);
DELETE FROM `smart_scripts` WHERE `entryorguid`=15941 AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(15945,0,14,15,8,0,100,0,27907,0,0,0,33,15945,0,0,0,0,0,7,0,0,0,0,0,0,0,'Apprentice Meledor - On Spell Hit(Disciplinary Rod) - Quest Credit'),
(15945,0,15,0,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Apprentice Meledor - Link - Say line'),
(15941,0,0,1,8,0,100,0,27907,0,0,0,33,15941,0,0,0,0,0,7,0,0,0,0,0,0,0,'Apprentice Ralen - On Spell Hit(Disciplinary Rod) - Quest Credit'),
(15941,0,1,0,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Apprentice Ralen - Link - Say line');

DELETE FROM `smart_scripts` WHERE `entryorguid`=15938 AND `source_type`=0 AND (`id` BETWEEN 15 AND 22);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(15938,0,15,0,8,0,100,0,1243,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 1) - Quest Credit'),
(15938,0,16,0,8,0,100,0,1244,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 2) - Quest Credit'),
(15938,0,17,0,8,0,100,0,1245,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 3) - Quest Credit'),
(15938,0,18,0,8,0,100,0,2791,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 4) - Quest Credit'),
(15938,0,19,0,8,0,100,0,10937,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 5) - Quest Credit'),
(15938,0,20,0,8,0,100,0,10938,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 6) - Quest Credit'),
(15938,0,21,0,8,0,100,0,25389,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 7) - Quest Credit'),
(15938,0,22,0,8,0,100,0,48161,0,0,0,33,15938,0,0,0,0,0,7,0,0,0,0,0,0,0,'Eversong Ranger - On Spell Hit(Power Word: Fortitude Rank 8) - Quest Credit');

UPDATE `smart_scripts` SET `event_flags`=1 WHERE `entryorguid`=1118 AND `id`=0 AND `source_type`=0;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=17 AND `SourceEntry` IN(66661,66642,30015,3678);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(17, 0, 66661, 0, 0, 29, 0, 35012, 40, 0, 1, 0, 0, '', 'Cant use Captured Kvaldir Banner within 40 yards of Ornolf The Scarred'),
(17, 0, 66642, 0, 0, 29, 0, 34980, 40, 0, 1, 0, 0, '', 'Cant use Kvaldir War Horn within 40 yards of Drottinn Hrothgar'),
(17, 0, 30015, 0, 0, 29, 0, 17207, 100, 0, 1, 0, 0, '', 'Cant use Gift of Naias within 100 yards of Naias'),
(17, 0, 3678, 0, 0, 29, 0, 2624, 100, 0, 1, 0, 0, '', 'Cant use Catelyns Blade within 100 yards of Gazban');

DELETE FROM `smart_scripts` WHERE `entryorguid` = 23030 AND `source_type` = 0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(23030, 0, 0, 0, 6, 0, 100, 2, 0, 0, 0, 0, 43, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Dragonmaw Sky Stalker - On Creature Death - Dismount"),
(23030, 0, 1, 0, 0, 0, 100, 2, 0, 0, 0, 0, 19, 0x02000000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Dragonmaw Sky Stalker - In Combat - Remove unit flag"),
(23030, 0, 2, 0, 9, 0, 100, 2, 5, 41, 15800, 15800, 11, 40872, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Dragonmaw Sky Stalker - At 5 - 41 Range - Cast Immolation Arrow"),
(23030, 0, 3, 0, 9, 0, 100, 2, 5, 41, 9800, 9800, 11, 40873, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Dragonmaw Sky Stalker - At 5 - 41 Range - Cast Shoot");

-- Make invisible some visible Triggers
UPDATE `creature_template` SET `flags_extra`=`flags_extra`|128 WHERE `entry` IN (21052,21403,21334,21436,21437,21438,21439,21440);

--
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=21215;

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(15, 21215, 0, 0, 0, 12, 0, 12, 0, 0, 0, 0, 0, '', 'Only display gossip during Hallows End'),
(15, 21215, 0, 0, 0, 23, 0, 1638, 0, 0, 0, 0, 0, '', 'Only display gossip in Thunderbluff'),
(15, 21215, 0, 0, 0, 1, 0, 24755, 0, 0, 1, 0, 0, '', 'only display gossip if player does not have tricked or treated aura');

UPDATE `smart_scripts` SET `target_type`=7 WHERE `entryorguid`=6740 AND `source_type`=0 AND `id`=1 AND `link`=0; 

-- Quest item "Drain Schematics" should be only obtainable if:
-- 1) player has completed quest 9720 "Balance Must Be Preserved"
-- 2) player has no quest 9731 "Drain Schematics"
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = '1' AND `SourceEntry` = '24330';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`) VALUES
('1','18088','24330','0','0','8','0','9720','0','0','0'),
('1','18088','24330','0','0','9','0','9731','0','0','1'),
('1','18089','24330','0','0','8','0','9720','0','0','0'),
('1','18089','24330','0','0','9','0','9731','0','0','1'),
('1','18340','24330','0','0','8','0','9720','0','0','0'),
('1','18340','24330','0','0','9','0','9731','0','0','1'),
('1','20088','24330','0','0','8','0','9720','0','0','0'),
('1','20088','24330','0','0','9','0','9731','0','0','1'),
('1','20089','24330','0','0','8','0','9720','0','0','0'),
('1','20089','24330','0','0','9','0','9731','0','0','1');

-- Set drop chance to normal(non-quest)
UPDATE `creature_loot_template` SET `ChanceOrQuestChance` = abs(`ChanceOrQuestChance`) WHERE (`item` = '24330');

UPDATE `smart_scripts` SET `action_param1`=60 WHERE `entryorguid`=208001 AND `source_type`=9 AND `id` IN (7,8,9);

--
SET @Mechagnome=29384; -- Captive Mechagnome

-- Update creature template to use SAI
UPDATE `creature_template` SET `ainame`='SmartAI',`scriptname`='' WHERE `entry`=@Mechagnome;
-- Add gossip option
DELETE FROM `gossip_menu_option` WHERE `menu_id`=9871;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES
(9871,0,0,'I\'m not a laborer. I\'m here to free you from servitude in the mines.',1,1,0,0,0,0,NULL);
-- Add conditions for gossip option visibility
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=9871;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(15,9871,0,0,0,9,0,12957,0,0,0,0,0,'','Show gossip only if quest is active');
-- Add Captive Mechagnome text
DELETE FROM `creature_text` WHERE `entry`=@Mechagnome;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@Mechagnome,0,0,'Does not compute. Unit malfunctioning. Directive: shut down.',12,0,0,0,0,0,''),
(@Mechagnome,0,1,'New directive: leave mine and return to Inventor\'s Library.',12,0,0,0,0,0,''),
(@Mechagnome,0,2,'New directive: assist in the defeat of the iron dwarves.',12,0,0,0,0,0,''),
(@Mechagnome,0,3,'Free again? Keeper Mimir\'s work awaits.',12,0,0,0,0,0,''),
(@Mechagnome,0,4,'Thank you, $r. I will join your struggle against the stormforged.',12,0,0,0,0,0,'');
-- Add Captive Mechagnome SAI
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Mechagnome AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@Mechagnome,0,0,1,62,0,100,0,9871,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0,0,0,0,'Captive Mechagnome - On Gossip Option Select - Close Gossip'),
(@Mechagnome,0,1,2,61,0,100,0,0,0,0,0,83,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Captive Mechagnome - Link With Previous Event - Remove npcflag 1'),
(@Mechagnome,0,2,3,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Captive Mechagnome - Link With Previous Event - Talk'),
(@Mechagnome,0,3,4,61,0,100,0,0,0,0,0,33,29962,0,0,0,0,0,7,0,0,0,0,0,0,0,'Captive Mechagnome - Link With Previous Event - Give Quest Credit'),
(@Mechagnome,0,4,5,61,0,100,0,0,0,0,0,69,0,0,0,0,0,0,1,0,0,0,7818.4,-86.48,880.63,0,'Captive Mechagnome - Link With Previous Event - Move To Position'),
(@Mechagnome,0,5,0,61,0,100,0,0,0,0,0,41,4000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Captive Mechagnome - Link With Previous Event - Despawn');

--
UPDATE `creature_template` SET `InhabitType`=4 WHERE `entry`=21657;

--
DELETE FROM `creature_loot_template` WHERE `entry` =8996;
UPDATE `creature_template` SET `lootid`=0, `mingold`=0, `maxgold`=0 WHERE `entry`=8996;

--
DELETE FROM `game_event_creature` WHERE `guid` IN (136921,136922) AND `eventEntry`=24;
UPDATE `game_event_seasonal_questrelation` SET `eventEntry`=2 WHERE `questId` IN (6983,6984);

--
SET @MARKER1 := 20473; -- Surveying Marker One
SET @MARKER2 := 20475; -- Surveying Marker Two
SET @MARKER3 := 20476; -- Surveying Marker Three

DELETE FROM `smart_scripts` WHERE `entryorguid` IN(@MARKER1,@MARKER2,@MARKER3) AND `source_type`=0;
UPDATE creature_template SET AIName="SmartAI" WHERE entry IN(@MARKER1,@MARKER2,@MARKER3);
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@MARKER1,0,0,0,8,0,100,0,35246,0,0,0,33,@MARKER1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Surveying Marker One - On Spellhit - Quest Credit"),
(@MARKER2,0,0,0,8,0,100,0,35246,0,0,0,33,@MARKER2,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Surveying Marker Two - On Spellhit - Quest Credit"),
(@MARKER3,0,0,0,8,0,100,0,35246,0,0,0,33,@MARKER3,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Surveying Marker Three - On Spellhit - Quest Credit");

--
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(18849,19008);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (18849,19008);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(18849, 0, 0, 0, 8,  0, 100, 0, 33067, 0, 0, 0, 33, 18849   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Invis Alliance Siege Engine - East - On Spellhit - Kill Credit'),
(19008, 0, 0, 0, 8,  0, 100, 0, 33067, 0, 0, 0, 33, 19008   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Invis Alliance Siege Engine - West - On Spellhit - Kill Credit');

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(15941,15945,28352,28064,28304,28305);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN(15941,15945,28352,28064,28304,28305);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(15941, 0, 0, 0, 8,  0, 100, 0, 27907, 0, 15000, 15000, 33, 15941   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Apprentice Ralen - On Spellhit - Kill Credit'),
(15945, 0, 0, 0, 8,  0, 100, 0, 27907, 0, 15000, 15000, 33, 15945   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Apprentice Meledor - On Spellhit - Kill Credit'),
(28352, 0, 0, 0, 8,  0, 100, 0, 51381, 0, 180000, 180000, 33, 28352   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Nethurbian Crater KC Bunny - On Spellhit - Kill Credit'),
(28064, 0, 0, 0, 8,  0, 100, 0, 51247, 0, 0, 0, 33, 28064   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Drakkari Pedestal 01 - On Spellhit - Kill Credit'),
(28304, 0, 0, 0, 8,  0, 100, 0, 51247, 0, 0, 0, 33, 28304   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Drakkari Pedestal 02 - On Spellhit - Kill Credit'),
(28305, 0, 0, 0, 8,  0, 100, 0, 51247, 0, 0, 0, 33, 28305   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Drakkari Pedestal 03 - On Spellhit - Kill Credit');

--
SET @GGUID=5289;

DELETE FROM `gameobject` WHERE `id`=181632;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`) VALUES
(@GGUID,181632,1,1,1,-4511.94,-781.874,-41.5711,6.04346,0,0,0.119574,-0.992825,300,0,1);

-- IoC template updates
UPDATE creature_template SET difficulty_entry_1 = 35415 WHERE entry = 34775;
UPDATE creature_template SET difficulty_entry_1 = 35433 WHERE entry = 35069;
UPDATE creature_template SET difficulty_entry_1 = 35413 WHERE entry = 34793;
UPDATE creature_template SET difficulty_entry_1 = 35421 WHERE entry = 35273;
UPDATE creature_template SET difficulty_entry_1 = 35429 WHERE entry = 34944;
UPDATE creature_template SET difficulty_entry_1 = 35401 WHERE entry = 34919;
UPDATE creature_template SET difficulty_entry_1 = 35403 WHERE entry = 34924;
UPDATE creature_template SET difficulty_entry_1 = 35407 WHERE entry = 34918;
UPDATE creature_template SET difficulty_entry_1 = 35405 WHERE entry = 34922;
UPDATE creature_template SET difficulty_entry_1 = 35431 WHERE entry = 34776;
UPDATE creature_template SET difficulty_entry_1 = 35419 WHERE entry = 34802;

-- Skill requirements for Flying Carpets and Flying Machines
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=17 AND `SourceEntry` IN (75596,61309,61451,44153,44151);

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(17, 0, 75596, 0, 0, 7, 0, 197, 425, 0, 0, 0, 0, '', 'Player has must have Tailoring with skill level 425 to Ride Frosty Flying Carpet'),
(17, 0, 61309, 0, 0, 7, 0, 197, 425, 0, 0, 0, 0, '', 'Player has must have Tailoring with skill level 425 to Ride Magnificent Flying Carpet'),
(17, 0, 61451, 0, 0, 7, 0, 197, 300, 0, 0, 0, 0, '', 'Player has must have Tailoring with skill level 300 to Ride Flying Carpet'),
(17, 0, 44153, 0, 0, 7, 0, 202, 300, 0, 0, 0, 0, '', 'Player has must have Engineering with skill level 300 to Ride Flying Machine'),
(17, 0, 44151, 0, 0, 7, 0, 202, 375, 0, 0, 0, 0, '', 'Player has must have Engineering with skill level 375 to Ride Turbo-Charged Flying Machine');

UPDATE `smart_scripts` SET `event_param1`=489 WHERE `entryorguid`=2150 AND `source_type`=0 AND `id`=0;

UPDATE `creature_template` SET `InhabitType`=4 WHERE `entry`=32535;

--
UPDATE `smart_scripts` SET `event_param3`=103900, `event_param4`=103900 WHERE  `entryorguid`=28566 AND `source_type`=0 AND `id`=3 AND `link`=0;
UPDATE `smart_scripts` SET `event_flags`=0 WHERE  `entryorguid`=28566 AND `source_type`=0 AND `id` IN(3,4,5,6) AND `link`=0;

--
UPDATE `quest_template` SET `RequiredClasses`=431 WHERE `Id` IN (24553,24564,24594);
UPDATE `quest_template` SET `RequiredClasses`=1104 WHERE `Id` IN (24595,24596,24598,24799);

--
UPDATE `creature_text` SET `entry`=23622 WHERE  `entry`=23616 AND `groupid`=2 AND `id`=0;

-- DB/Quest: Fix: A Dark Influence (12220) | by wintergreen77
SET @SPELL     := 48218; -- Sampling Energy
SET @NPC_HEART := 27263; -- Vordrassil's Heart Credit
SET @NPC_LIMB  := 27264; -- Vordrassil's Limb Credit
SET @NPC_TEARS := 27265; -- Vordrassil's Tears Credit

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN (@NPC_HEART, @NPC_LIMB, @NPC_TEARS);
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@NPC_HEART, @NPC_LIMB, @NPC_TEARS) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(@NPC_HEART, 0, 0, 0, 8, 0, 100, 0, @SPELL, 0, 0, 0, 33, @NPC_HEART, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'On Spellhit - Give Vordrasill''s Heart Kill credit - Action Invoker'),
(@NPC_LIMB, 0, 0, 0, 8, 0, 100, 0, @SPELL, 0, 0, 0, 33, @NPC_LIMB, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'On Spellhit - Give Vordrasill''s Limb Kill credit - Action Invoker'),
(@NPC_TEARS, 0, 0, 0, 8, 0, 100, 0, @SPELL, 0, 0, 0, 33, @NPC_TEARS, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'On Spellhit - Give Vordrasill''s Tears Kill credit - Action Invoker');

--
DELETE FROM `creature_ai_scripts` WHERE `creature_id` IN (25416,25418);
DELETE FROM `creature_ai_texts` WHERE `entry` IN (-487,-488,-489,-498,-499,-500,-501);

UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (25416,25418);

DELETE FROM `creature_text` WHERE `entry` IN (25416,25418);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(25416,0,0,'Who dares?',12,0,0,0,0,0,''),
(25416,1,0,'STOP!',12,0,0,0,0,0,''),
(25416,2,0,'Tell Imperean that I will consider a cessation of hostilities. But first, Churn must stop his watery intrusions upon my rise!',12,0,0,0,0,0,''),
(25416,3,0,'I grow bored with you. Begone!',12,0,0,0,0,0,''),
(25418,0,0,'I... submit. As long as Simmer agrees to stop boiling my pool, I agree to an armistice.',12,0,0,0,0,0,''),
(25418,1,0,'Now, remove yourself from my presence. You would be wise not to come within sight of me again.',12,0,0,0,0,0,''),
(25418,2,0,'WAIT... NO MORE!',12,0,0,0,0,0,'');

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (25416,25418) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(25416,0,0,1,4,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - On Aggro - Say line 0'),
(25416,0,1,0,61,0,100,0,0,0,0,0,42,0,5,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - Link With Event 0 - Set Invincible HP'),
(25416,0,2,0,0,0,100,1,5000,5000,5000,5000,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - IC - Say Line 2 After 5 secs'),
(25416,0,3,0,0,0,100,1,10000,10000,10000,10000,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - IC - Say Line 3 After 10 secs'),
(25416,0,4,5,2,0,100,1,0,5,0,0,85,45599,0,0,0,0,0,7,0,0,0,0,0,0,0,'Simmer - At 5 % HP - Give Quest Credit'),
(25416,0,5,6,61,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - Link With Event 4 - Say Line 1'),
(25416,0,6,7,61,0,100,0,0,0,0,0,2,35,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - Link With Event 5 - Change Faction'),
(25416,0,7,0,61,0,100,0,0,0,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - Link With Event 6 - Evade'),
(25416,0,8,0,1,0,100,0,120000,120000,120000,120000,2,1983,0,0,0,0,0,1,0,0,0,0,0,0,0,'Simmer - OOC - Change Faction After 2 mins'),
(25418,0,0,0,0,0,100,0,5300,5300,9900,9900,11,50206,0,0,0,0,0,2,0,0,0,0,0,0,0,'Churn - IC - Cast Scalding Steam'),
(25418,0,1,0,4,0,100,0,0,0,0,0,42,0,5,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - On Aggro - Set Invincible HP'),
(25418,0,2,0,0,0,100,1,5000,5000,5000,5000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - IC - Say Line 0 After 5 secs'),
(25418,0,3,0,0,0,100,1,10000,10000,10000,10000,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - IC - Say Line 1 After 10 secs'),
(25418,0,4,5,2,0,100,1,0,5,0,0,85,45598,0,0,0,0,0,7,0,0,0,0,0,0,0,'Churn - At 5 % HP - Give Quest Credit'),
(25418,0,5,6,61,0,100,0,0,0,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - Link With Event 4 - Say Line 2'),
(25418,0,6,7,61,0,100,0,0,0,0,0,2,35,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - Link With Event 5 - Change Faction'),
(25418,0,7,0,61,0,100,0,0,0,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - Link With Event 6 - Evade'),
(25418,0,8,0,1,0,100,0,120000,120000,120000,120000,2,1984,0,0,0,0,0,1,0,0,0,0,0,0,0,'Churn - OOC - Change Faction After 2 mins');

--
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` =25654;

DELETE FROM `smart_scripts` WHERE `entryorguid` =25654 AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(25654,0,0,0,8,0,100,0,45835,0,0,0,11,45837,0,0,0,0,0,7,0,0,0,0,0,0,0,'Stop the Plague Kill Credit Bunny - On Spell Hit (Bixies Inhibiting Powder) - Give Quest Credit'),
(25654,0,1,0,8,0,100,0,45834,0,0,0,11,45837,0,0,0,0,0,7,0,0,0,0,0,0,0,'Stop the Plague Kill Credit Bunny - On Spell Hit (Highmesas Cleansing Seeds) - Give Quest Credit');

-- A Valiant`s Field Training (All) (Horde) progress
UPDATE `quest_template` SET `RequestItemsText` = 'A good day of training will never hurt anyone. It''s good to see you keeping up.' WHERE `id` IN (13765, 13771, 13776, 13781, 13786);

-- Taking Battle To The Enemy (Horde and Alliance) progress
UPDATE `quest_template` SET `RequestItemsText` = 'Have you tested yourself in combat against the Scourge?' WHERE `id` IN (13813, 13791);

-- Stop The Aggressors (Horde) progress
UPDATE `quest_template` SET `RequestItemsText` = 'Have you shown the Kvaldir that we will not let them threaten us?' WHERE `id` = 14140;

-- You`ve Really Done It This Time, Kul (Horde) progress
UPDATE `quest_template` SET `RequestItemsText` = 'Did you free Kul and those hopeless aspirants?' WHERE `id` = 14142;

-- Threat From Above (Horde) progress
UPDATE `quest_template` SET `RequestItemsText` = 'Have you defeated the Cult of the Damned raiding party?' WHERE `id` = 13812;

-- Rescue at Sea (Horde) progress
UPDATE `quest_template` SET `RequestItemsText` = 'Have you helped fend off the Kvaldir attack?' WHERE `id` = 14136;

--
UPDATE `gameobject_template` SET `AIName`='SmartGameObjectAI', `ScriptName`='' WHERE  `entry`=177673;
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` =12369;

DELETE FROM `smart_scripts` WHERE `entryorguid` =177673 AND `source_type`=1 OR `entryorguid`=17767300 AND `source_type`=9 OR `entryorguid`=12369 AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(177673, 1, 0 ,1, 70, 0, 100, 0, 2, 0, 0,0,80,17767300,2,0,0,0,0,1,0,0,0,0,0,0,0, 'Serpent Statue - On State Changed - Run Script'),
(17767300, 9, 0 ,0, 0, 0, 100, 0, 5000, 5000, 0,0,12,12369,1,180000,0,0,0,8,0,0,0,254.166855,2966.883545,1.367331,0.957399, 'Serpent Statue - Script - Spawn Lord Kragaru'),
(17767300, 9, 1 ,0, 0, 0, 100, 0, 185000, 185000, 0,0,32,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Serpent Statue - Script - Reset Go'),
(12369, 0, 0 ,0, 9, 0, 100, 0, 0, 5, 3000,4000,11,15496,0,0,0,0,0,7,0,0,0,0, 0, 0, 0, 'Lord Kragaru - On Range - Cast Cleave'),
(12369, 0, 1 ,0, 13, 0, 100, 0, 10000, 15000, 0,0,11,12555,0,0,0,0,0,7,0,0,0,0, 0, 0, 0, 'Lord Kragaru - On Target Casting - Cast Pummel');

UPDATE gameobject SET `phaseMask`=3 WHERE `id`=201367;

--
DELETE FROM `disables` WHERE `sourceType` = 0 AND `entry` = 54114;
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`) VALUES
(0, 54114, 64, 0, 0, 'Heart of the Phoenix uses Cooldown, but won''t work through LoS, so ignore LoS.');

--
DELETE FROM spell_scripts WHERE id=26373;
DELETE FROM spell_linked_spell WHERE spell_trigger=26373;
INSERT INTO spell_linked_spell VALUES (26373,26448,0,'Lunar Festival Invitation - Teleport Moonglade');

--
DELETE FROM `disables` WHERE `sourceType`=0 AND `entry`IN(69922,69956);
INSERT INTO `disables`(`sourceType`,`entry`,`flags`,`comment`) VALUES
(0,69922,64,'Ignore LOS on Temper Quel Delar'),
(0,69956,64,'Ignore LOS on Return Tempered Quel Delar');

--
DELETE FROM `spell_area` WHERE `spell`=71313;
INSERT INTO `spell_area` (`spell`, `area`, `quest_start`, `quest_end`, `aura_spell`, `racemask`, `gender`, `autocast`, `quest_start_status`, `quest_end_status`) VALUES 
(71313, 4862, 24461, 24522, 0, 0, 2, 1, 74, 11);

UPDATE `creature_questender` SET `id`=27872 WHERE `quest`=12474;

-- Bros. Before Ho Ho Ho's fix by nelegalno
UPDATE `creature_template` SET `type_flags`=4096 WHERE `entry` IN (5661,26044,739,927,1182,1351,1444,5484,5489,8140,12336);

--
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(17111,17112,17113);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (17111,17112,17113);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(17111, 0, 0, 0, 8,  0, 100, 0, 29916, 0, 0, 0, 33, 17111   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Captive Jaguar - On Spellhit - Kill Credit'),
(17112, 0, 0, 0, 8,  0, 100, 0, 29916, 0, 0, 0, 33, 17112   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Captive Tarantula - On Spellhit - Kill Credit'),
(17113, 0, 0, 0, 8,  0, 100, 0, 29916, 0, 0, 0, 33, 17113   , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Captive Crocolisk - On Spellhit - Kill Credit');

DELETE FROM `disables` WHERE `sourceType`=0 AND `entry`IN (29916,29917);
INSERT INTO `disables`(`sourceType`,`entry`,`flags`,`comment`) VALUES
(0,29917,64,'Ignore LOS on Feed Captured Animal'),
(0,29916,64,'Ignore LOS on Feed Captured Animal');

--
UPDATE `smart_scripts` SET `event_type`=0 WHERE `entryorguid`=6066 AND `source_type`=0 AND `id`=0 AND `link`=0; 

-- Rewrite [Qs] Gruesome, But Necessary /11257-H,11246-A /using some of old event data, fix visual bugs, death states, adds conditions
-- Rewrite scripts from EAI to SAI to add spells to the Winterskorn creatures, add some that are missing at all, add texts, fix dynamicflags, remove pointless phasing that created some bugs
SET @Tribesman:= 23661;
SET @Woodsman := 23662;
SET @Shield_Maiden := 23663;
SET @Warrior := 23664;
SET @Raider := 23665;
SET @Berserker := 23666;
SET @Rune_Seer := 23667;
SET @Rune_Caster := 23668;
SET @Oracle := 23669;
SET @Elder := 23670;
SET @Dismember := 43036;
SET @Transform := 43059;
SET @Credit := 43037;

-- Delete the old EAIs while using part of their data
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Tribesman;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Woodsman;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Shield_Maiden;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Warrior;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Raider;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Berserker;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Rune_Seer;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Rune_Caster;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@Oracle;

-- Winterskorn Tribesman
UPDATE `creature_template` SET `AIName`='SmartAI ' WHERE `entry`=@Tribesman;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Tribesman;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Tribesman,0,0,1,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Tribesman - On hit by spell Dismember - Cast spell trasnform on self'),
(@Tribesman,0,1,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Tribesman - Linked with previous event - Cast kill credit to player'),
(@Tribesman,0,2,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Tribesman - IC - Say random text from id 0');

-- Winterskorn Woodsman
UPDATE `creature_template` SET `AIName`='SmartAI ' WHERE `entry`=@Woodsman;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Woodsman;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Woodsman,0,0,0,0,0,100,0,100,1000,5000,14500,11,38557,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Woodsman - IC - Cast spell Throw on victim'),
(@Woodsman,0,1,0,0,0,100,0,1000,4500,4900,8100,11,43410,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Woodsman - IC - Cast spell Chop on victim'),
(@Woodsman,0,2,3,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Woodsman - On hit by spell Dismember - Cast spell trasnform on self'),
(@Woodsman,0,3,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Woodsman - Linked with previous event - Cast kill credit to player'),
(@Woodsman,0,4,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Woodsman - IC - Say random text from id 0');

-- Winterskorn Shield-Maiden
UPDATE `creature_template` SET `AIName`='SmartAI ' WHERE `entry`=@Shield_Maiden;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Shield_Maiden;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Shield_Maiden,0,0,0,0,0,100,0,1000,2000,6000,14500,11,43416,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Shield_Maiden - IC - Throw Shield'),
(@Shield_Maiden,0,1,2,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Shield_Maiden - On hit by spell Dismember - Cast spell trasnform on self'),
(@Shield_Maiden,0,2,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Shield_Maiden - Linked with previous event - Cast kill credit to player'),
(@Shield_Maiden,0,3,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Shield_Maiden - IC - Say random text from id 0'),
(@Shield_Maiden,0,4,0,1,0,100,0,0,60000,15000,70000,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Shield_Maiden - OOC - Say random text from id 1');

-- Winterskorn Warrior
UPDATE `creature_template` SET `AIName`='SmartAI ' WHERE `entry`=@Warrior;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Warrior;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Warrior,0,0,1,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Warrior - On hit by spell Dismember - Cast spell trasnform on self'),
(@Warrior,0,1,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Warrior - Linked with previous event - Cast kill credit to player'),
(@Warrior,0,2,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Warrior - IC - Say random text from id 0'),
(@Warrior,0,3,0,1,0,100,0,0,60000,15000,70000,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Warrior - OOC - Say random text from id 1');

-- Winterskorn Raider
UPDATE `creature_template` SET `InhabitType`=`InhabitType`|1,`AIName`='SmartAI ' WHERE `entry`=@Raider;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Raider;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Raider,0,0,0,13,0,100,0,4500,14000,0,0,11,11978,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Raider - On target casting spell - Cast kick to interrupt and silence'),
(@Raider,0,1,2,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Raider - On hit by spell Dismember - Cast spell trasnform on self'),
(@Raider,0,2,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Raider - Linked with previous event - Cast kill credit to player'),
(@Raider,0,3,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Raider - IC - Say random text from id 0'),
(@Raider,0,4,0,1,0,100,0,0,60000,15000,70000,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Raider - OOC - Say random text from id 1');

-- Winterskorn Berserker
UPDATE `creature_template` SET dynamicflags=8,`AIName`='SmartAI ' WHERE `entry`=@Berserker;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Berserker;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Berserker,0,0,1,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Berserker - On hit by spell Dismember - Cast spell trasnform on self'),
(@Berserker,0,1,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Berserker - Linked with previous event - Cast kill credit to player'),
(@Berserker,0,2,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Berserker - IC - Say random text from id 0'),
(@Berserker,0,3,0,1,0,100,0,0,60000,15000,70000,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Berserker- OOC - Say random text from id 1');

-- Winterskorn Rune-Seer
UPDATE `creature_template` SET `AIName`='SmartAI ' WHERE `entry`=@Rune_Seer;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Rune_Seer;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Rune_Seer,0,0,0,0,0,100,0,1000,3500,8000,12000,11,43453,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Seer - IC - Cast spell Rune Ward on self'),
(@Rune_Seer,0,1,0,0,0,100,0,1500,4500,2900,8000,11,43083,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Rune_Seer - IC - Cast spell Frostbolt on victim'),
(@Rune_Seer,0,2,0,0,0,100,0,100,4500,10000,14500,11,34787,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Rune_Seer - IC - Cast spell Freezing circle victim /aoe/'),
(@Rune_Seer,0,3,0,2,0,100,0,10,90,7500,20100,11,11986,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Seer - On HP% - Cast spell Healing wave on self'),
(@Rune_Seer,0,4,0,14,0,100,0,1000,20,8000,18000,11,11986,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Rune_Seer - On friednly HP deficit  - Cast spell Healing wave on ally'),
(@Rune_Seer,0,5,6,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Seer - On hit by spell Dismember - Cast spell trasnform on self'),
(@Rune_Seer,0,6,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Rune_Seer - Linked with previous event - Cast kill credit to player'),
(@Rune_Seer,0,7,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Seer - IC - Say random text from id 0');

-- Winterskorn Rune-Caster
UPDATE `creature_template` SET dynamicflags=8,`AIName`='SmartAI ' WHERE `entry`=@Rune_Caster;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Rune_Caster;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Rune_Caster,0,0,0,0,0,100,0,1000,3500,3000,9000,11,43083,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Rune_Caster - IC - Cast spell Frostbolt'),
(@Rune_Caster,0,1,0,0,0,100,0,2500,6500,8000,11000,11,12548,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Rune_Caster - IC - Cast spell Frost Shock'),
(@Rune_Caster,0,2,0,0,0,100,0,0,5500,8000,12500,11,37798,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Rune_Caster - IC - Cast spell Exploding Rune'),
(@Rune_Caster,0,3,4,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Caster - On hit by spell Dismember - Cast spell trasnform on self'),
(@Rune_Caster,0,4,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Rune_Caster - Linked with previous event - Cast kill credit to player'),
(@Rune_Caster,0,5,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Rune_Caster - IC - Say random text from id 0');

-- Winterskorn Oracle
UPDATE `creature_template` SET dynamicflags=8,`AIName`='SmartAI ' WHERE `entry`=@Oracle;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Oracle;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Oracle,0,0,0,0,0,100,0,1000,3500,3000,9000,11,43083,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Oracle - IC - Cast spell Frostbolt'),
(@Oracle,0,1,2,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Oracle - On hit by spell Dismember - Cast spell trasnform on self'),
(@Oracle,0,2,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Oracle - Linked with previous event - Cast kill credit to player'),
(@Oracle,0,3,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Oracle - IC - Say random text from id 0');

-- Winterskorn Elder
UPDATE `creature_template` SET dynamicflags=8,`AIName`='SmartAI ' WHERE `entry`=@Elder;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@Elder;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Elder,0,0,0,0,0,100,0,0,500,120000,120000,11,32734,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Elder - IC - Cast spell Eearth Shield'),
(@Elder,0,1,0,0,0,100,0,2000,3800,3000,8500,11,9532,0,0,0,0,0,2,0,0,0,0,0,0,0, 'Elder - IC - Cast spell Lightening bolt'),
(@Elder,0,2,0,2,0,100,0,10,90,7500,20100,11,11986,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Elder - On HP% - Cast spell Healing wave on self'),
(@Elder,0,3,0,14,0,100,0,1000,20,8000,18000,11,11986,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Elder - On friednly HP deficit  - Cast spell Healing wave on ally'),
(@Elder,0,4,0,6,0,100,0,0,0,0,0,28,32734,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Elder - On death - Remove auras from Eearth Shield'),
(@Elder,0,5,6,8,0,100,0,@Dismember,0,0,0,11,@Transform,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Elder - On hit by spell Dismember - Cast spell trasnform on self'),
(@Elder,0,6,0,61,0,100,0,0,0,0,0,11,@Credit,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Elder - Linked with previous event - Cast kill credit to player'),
(@Elder,0,7,0,0,0,100,0,1000,15000,7200,35000,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Elder - IC - Say random text from id 0');

-- Add conditions for Dismember - cannot be used on Transformed units
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=17 AND `SourceEntry`=@Dismember;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition` ,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(17,0,@Dismember,0,0,1,1,@Transform,0,0,1,0,'','Amulet can target only Iskalder');

-- Add texts:
DELETE FROM `creature_text` WHERE `entry`=@Tribesman;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Tribesman,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Tribesman'),
(@Tribesman,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Tribesman');

DELETE FROM `creature_text` WHERE `entry`=@Woodsman;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Woodsman,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Woodsman'),
(@Woodsman,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Woodsman');

DELETE FROM `creature_text` WHERE `entry`=@Berserker;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Berserker,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,0,'Come on, stop trying to hit me and hit me already!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,1,'I train better against a target dummy.',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,2,'Practice or not, you''re going to spit teeth!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,3,'Sure you don''t need a break, old woman?',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,4,'When the little ones come, I won''t hold back like I am with you.',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,5,'You practice as if you''re still asleep.',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,6,'You spar like a girl!',12,0,100,1,0,0,'Winterskon Berserker'),
(@Berserker,1,7,'Your sparring is feeble!',12,0,100,1,0,0,'Winterskon Berserker');

DELETE FROM `creature_text` WHERE `entry`=@Warrior;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Warrior,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,0,'Come on, stop trying to hit me and hit me already!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,1,'I train better against a target dummy.',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,2,'Practice or not, you''re going to spit teeth!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,3,'Sure you don''t need a break, old woman?',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,4,'When the little ones come, I won''t hold back like I am with you.',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,5,'You practice as if you''re still asleep.',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,6,'You spar like a girl!',12,0,100,1,0,0,'Winterskon Warrior'),
(@Warrior,1,7,'Your sparring is feeble!',12,0,100,1,0,0,'Winterskon Warrior');

DELETE FROM `creature_text` WHERE `entry`=@Raider;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Raider,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,0,'Come on, stop trying to hit me and hit me already!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,1,'I train better against a target dummy.',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,2,'Practice or not, you''re going to spit teeth!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,3,'Sure you don''t need a break, old woman?',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,4,'When the little ones come, I won''t hold back like I am with you.',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,5,'You practice as if you''re still asleep.',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,6,'You spar like a girl!',12,0,100,1,0,0,'Winterskon Raider'),
(@Raider,1,7,'Your sparring is feeble!',12,0,100,1,0,0,'Winterskon Raider');

DELETE FROM `creature_text` WHERE `entry`=@Shield_Maiden;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Shield_Maiden,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,0,'Come on, stop trying to hit me and hit me already!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,1,'I train better against a target dummy.',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,2,'Practice or not, you''re going to spit teeth!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,3,'When the little ones come, I won''t hold back like I am with you.',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,4,'You practice as if you''re still asleep.',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,5,'You spar like a girl!',12,0,100,1,0,0,'Winterskon Shield_Maiden'),
(@Shield_Maiden,1,6,'Your sparring is feeble!',12,0,100,1,0,0,'Winterskon Shield_Maiden');

DELETE FROM `creature_text` WHERE `entry`=@Elder;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Elder,0,0,'For Ymiron!',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,1,'I will break you!',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,2,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,3,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,4,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,5,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Elder'),
(@Elder,0,6,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Elder');

DELETE FROM `creature_text` WHERE `entry`=@Rune_Caster;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Rune_Caster,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Rune_Caster'),
(@Rune_Caster,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Rune_Caster');

DELETE FROM `creature_text` WHERE `entry`=@Rune_Seer;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@Rune_Seer,0,0,'Die, maggot!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,1,'For Ymiron!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,2,'Haraak foln!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,3,'I spit on you!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,4,'I will break you!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,5,'I will feed you to the dogs!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,6,'I will take pleasure in gutting you!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,7,'I''ll eat your heart!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,8,'Look what''s come to play.',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,9,'My life for Ymiron!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,10,'Sniveling pig!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,11,'There will be no everlasting life for you!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,12,'Ugglin oo bjorr!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,13,'YAAARRRGH!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,14,'You come to die!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,15,'You tiny creatures disgust me!',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,16,'Your entrails will make a fine necklace.',12,0,100,1,0,0,'Winterskon Rune_Seer'),
(@Rune_Seer,0,17,'Your race is a disease upon the world!',12,0,100,1,0,0,'Winterskon Rune_Seer');

--
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `speed_walk`='1.2', `speed_run`='0.98571' WHERE `entry`=35415;
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `speed_walk`='1.2', `speed_run`='1', `unit_class`=4 WHERE `entry`=35431;
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `speed_walk`='2.8', `speed_run`='2.42857', `unit_class`=4 WHERE `entry`=35413;
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `speed_walk`='3.2' WHERE `entry`=35419;
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80 WHERE `entry`=35429;
UPDATE `creature_template` SET `minlevel`=80, `maxlevel`=80, `speed_walk`='1.2', `unit_class`=4 WHERE `entry`=35433;

-- Template updates
UPDATE `creature_template` SET `speed_walk`=1 WHERE `entry`=20809; -- Mana Bomb Channel Trigger
UPDATE `creature_template` SET `speed_walk`=1,`speed_run`=1.14286 WHERE `entry` IN (26631,31350); -- Novos the Summoner
UPDATE `creature_template` SET `speed_walk`=1,`speed_run`=1 WHERE `entry` IN (27894,32795); -- Antipersonnel Cannon
UPDATE `creature_template` SET `speed_walk`=1,`speed_run`=0.99206 WHERE `entry`=30337; -- Jotunheim Rapid-Fire Harpoon

--
UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry`=25425;
DELETE FROM `smart_scripts` WHERE `entryorguid`=25425;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(25425,0,0,0,8,0,100,0,45607,0,0,0,33,25425,0,0,0,0,0,7,0,0,0,0,0,0,0,'On Spell Hit(Kaganishu''s Fetish Trigger) - Quest Credit');
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceGroup`=1 AND `SourceEntry`=45607;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13,1,45607,0,0,31,0,3,25425,0,0,0,0,'','Kaganishu''s Fetish Trigger hits Farseer Grimwalker''s Spirit');

--
SET @RUNESEEKINGPICK :=  43666; 
SET @RUNEDSTONEGIANT :=  24329;
SET @NPC_REWARD      :=  24329; 

DELETE FROM `conditions` WHERE `SourceEntry`=@RUNESEEKINGPICK AND `SourceTypeOrReferenceId`=17;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(17,0,@RUNESEEKINGPICK,0,0,36,1,0,0,0,1,0,'', 'Runeseeking Pick can target only a corpse'),
(17,0,@RUNESEEKINGPICK,0,0,31,1,3,@RUNEDSTONEGIANT,0,0,0,'', 'Runeseeking Pick can target only RUNEDSTONEGIANT');

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` = @RUNEDSTONEGIANT; 

DELETE FROM `smart_scripts` WHERE `entryorguid` = @RUNEDSTONEGIANT AND `source_type`=0; 
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, 
`event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, 
`action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(@RUNEDSTONEGIANT,0,0,1,8,0,100,1,@RUNESEEKINGPICK,0,0,0,80,@NPC_REWARD*100+1,2,0,0,0,0,16,0,0,0,0,0,0,0, 'Runed Stone Giant- Script - Give Quest Credit'),
(@RUNEDSTONEGIANT,0,1,0,61,0,100,1,0,0,0,0,41,2000,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Runed Stone Giant - Despawn after 2 seconds');

--
SET @ARTRUIS := 28659;
SET @JALOOT   := 28667;
SET @ZEPIK    := 28668;
SET @ARTRUISGUID := 202971;
SET @JALOOTGUID   := 202969;
SET @ZEPIKGUID    := 202970;

DELETE FROM `creature_questender` WHERE `id` IN (@ZEPIK,@JALOOT) AND `quest` IN (12582, 12689);
INSERT INTO `creature_questender` (`id`, `quest`) VALUES
(@ZEPIK, 12582),  -- Zepik the Gorloc Hunter q12582 Frenzyheart Champion
(@JALOOT, 12689);  -- Jaloot q12689 Hand of the Oracles

UPDATE `creature_queststarter` SET `id`=@ZEPIK WHERE `quest`=12582;

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`= @ARTRUIS;

DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@ARTRUIS;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ARTRUIS;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@ARTRUIS, 0, 0, 1, 11, 0, 100, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Respawn- Respawn Jaloot'),
(@ARTRUIS, 0, 1, 0, 61, 0, 100, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Respawn Zephik'),
(@ARTRUIS, 0, 2, 0, 0, 0, 100, 0, 5000, 9000, 9000, 15000, 11, 15530, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - IC - Cast Frostbolt (Phase 1)'),
(@ARTRUIS, 0, 3, 0, 0, 0, 100, 0, 7000, 11000, 11000, 15000, 11, 54261, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - IC - Cast Ice Lance (Phase 1)'),
(@ARTRUIS, 0, 4, 0, 9, 0, 100, 0, 0, 10, 14000, 18000, 11, 11831, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,  'Artruis the Heartless - On Range - Cast Frost Nova (Phase 1)'),
(@ARTRUIS, 0, 5, 0, 0, 0, 100, 0, 9000, 13000, 25000, 35000, 11, 54792, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,  'Artruis the Heartless - Cast Icy Veins (Phase 1)'),
(@ARTRUIS, 0, 6, 7, 6, 0, 100, 0, 0, 0, 0, 0, 11, 52518, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - On death - Spawn Artruis Phylactery'),
(@ARTRUIS, 0, 7, 8, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 4, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event- Send Data 1 4 to Jaloot'),
(@ARTRUIS, 0, 8, 9, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 4, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Send Data 1 4 to Zephik'),
(@ARTRUIS, 0, 9, 0, 61, 0, 100, 0, 0, 0, 0, 0, 1, 5, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Say Line 5'),
(@ARTRUIS, 0, 10, 11, 2, 0, 100, 1, 0, 30, 0, 0, 11, 52185, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - At 20% Hp Cast Bindings of Submission'),
(@ARTRUIS, 0, 11, 12, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Send Data 1 1 to Jaloot'),
(@ARTRUIS, 0, 12, 13, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Send Data 1 1 to Zephik'),
(@ARTRUIS, 0, 13, 14, 61, 0, 100, 0, 0, 0, 0, 0, 1, 3, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Say Line 3'),
(@ARTRUIS, 0, 14, 0, 61, 0, 100, 0, 0, 0, 0, 0, 1, 4, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Say Line 4'),
(@ARTRUIS, 0, 15, 16, 25, 0, 100, 0, 0, 0, 0, 0, 45, 1, 2, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Reset - Send Data 1 2 to Jaloot'),
(@ARTRUIS, 0, 16, 0, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 2, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Send Data 1 2 to Zephik'),
(@ARTRUIS, 0, 17, 18, 38, 0, 100, 0, 1, 2, 0, 0, 45, 1, 3, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Receive Data 1 2- Send Data 1 3 to Jaloot'),
(@ARTRUIS, 0, 18, 19, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52185, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Remove Bindings of Submission'),
(@ARTRUIS, 0, 19,  0, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Set Data 1 0'),
(@ARTRUIS, 0, 20, 21, 4, 0, 100, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 10, @JALOOTGUID, @JALOOT, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Agro- Respawn Jaloot'),
(@ARTRUIS, 0, 21, 22, 61, 0, 100, 0, 0, 0, 0, 0, 70, 0, 0, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Respawn Zephik'),
(@ARTRUIS, 0, 22, 0, 61, 0, 100, 0, 0, 0, 0, 0, 1, 0, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Say Line 0'),
(@ARTRUIS, 0, 23, 0, 2, 0, 100, 1, 0, 75, 0, 0, 1, 1, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - On 75% HP - Say Line 1'),
(@ARTRUIS, 0, 24, 0, 2, 0, 100, 1, 0, 50, 0, 0, 1, 2, 10000, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - On 50% HP - Say Line 2'),
(@ARTRUIS, 0, 25, 26, 38, 0, 100, 0, 1, 1, 0, 0, 45, 1, 5, 0, 0, 0, 0, 10, @ZEPIKGUID, @ZEPIK, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Receive Data 1 1- Send Data 1 5 to Zepik'),
(@ARTRUIS, 0, 26, 27, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52185, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Remove Bindings of Submission'),
(@ARTRUIS, 0, 28,  0, 61, 0, 100, 0, 0, 0, 0, 0, 45, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - Linked with Previous Event - Set Data 1 0'),
(@ARTRUIS, 0, 29,  0, 0, 0, 100, 0, 1000, 1000, 1000, 1000, 11, 53163, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - IC - Cast Dessawn Retainer'),
(@ARTRUIS, 0, 30,  0, 4, 0, 100, 0, 0, 0, 0, 0, 11, 53163, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Artruis the Heartless - On Agro - Cast Dessawn Retainer');

DELETE FROM `creature_text` WHERE `entry` =@ARTRUIS;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@ARTRUIS,0,0,'Ah, the hero. Your little friends said you would come. This certainly saves me the trouble of hunting you down myself.',14,0,100,2,0,0,'Artruis the Heartless'),
(@ARTRUIS,1,0,'I have weathered a hundred years of war and suffering. Do you truly think it wise to pit your mortals boddies against a being that cannot die? I''d venture you have more to lose',14,0,100,2,0,0,'Artruis the Heartless'),
(@ARTRUIS,2,0,'Even shattered into countless pieces, the crystals all around weaken me... perhaps I should not have underestimated the titans so...',14,0,100,2,0,0,'Artruis the Heartless'),
(@ARTRUIS,3,0,'These two brave, ignorant fools despise each other, yet somehow they both spoke of you as a hero. Perhaps you just lack the will to choose a side... let me extend you that opportunity.',14,0,100,2,0,0,'Artruis the Heartless'),
(@ARTRUIS,4,0,'Artruis is shielded. You must choose your side quickly to break his spell.',41,0,100,2,0,0,'Artruis the Heartless'),
(@ARTRUIS,5,0,'Arthas once mustered strength...of the very same sort... perhaps his is the path that you will follow.',14,0,100,2,0,0,'Artruis the Heartless');

UPDATE `creature_template` SET `AIName`= 'SmartAI',`npcflag`=0,`unit_flags`=32832 WHERE `entry`= @JALOOT;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@JALOOT;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@JALOOT, 0, 0, 1, 11, 0, 100, 0, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Spawn - Remove quest giver flag'),
(@JALOOT, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Set Unit Flags'),
(@JALOOT, 0, 2, 0, 61, 0, 100, 0, 0, 0, 0, 0, 11, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Cast Tomb of the Heartless'),
(@JALOOT, 0, 3, 4, 38, 0, 100, 0, 1, 1, 0, 0, 2, 14, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Data Set 1 1 - Set Hostile'),
(@JALOOT, 0, 4, 5, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Remove Tomb of the Heartless'),
(@JALOOT, 0, 5, 0, 61, 0, 100, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 18, 40, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Attack'),
(@JALOOT, 0, 6, 7, 38, 0, 100, 0, 1, 2, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Data Set 1 2 - Set Friendly'),
(@JALOOT, 0, 7, 8, 61, 0, 100, 0, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Set Unit Flags'),
(@JALOOT, 0, 8 ,0, 61, 0, 100, 0, 0, 0, 0, 0, 11, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Cast Tomb of the Heartless'),
(@JALOOT, 0, 9, 0, 6, 0, 100, 0, 0, 0, 0, 0, 45, 1, 1, 0, 0, 0, 0, 10, @ARTRUISGUID, @ARTRUIS, 0, 0, 0, 0, 0, 'Jaloot - On Death - Send Data 1 1 to Artruis'),
(@JALOOT, 0, 10,11, 38, 0, 100, 0, 1, 3, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Data Set 1 3 - Set Friendly'),
(@JALOOT, 0, 11,12, 61, 0, 100, 0, 0, 0, 0, 0, 19, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Remove Unit Flags'),
(@JALOOT, 0, 12,13, 61, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Say Line 0'),
(@JALOOT, 0, 13,14, 61, 0, 100, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 10, @ARTRUISGUID, @ARTRUIS, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Attack Artruis'),
(@JALOOT, 0, 14, 0, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52185, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Remove Bindings of Submission'),
(@JALOOT, 0, 15,16, 38, 0, 100, 0, 1, 4, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Data Set 1 4 - Set Friendly'),
(@JALOOT, 0, 16,17, 61, 0, 100, 0, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Set Unit Flags'),
(@JALOOT, 0, 17,18, 61, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Add quest giver flag'),
(@JALOOT, 0, 18,19 , 61, 0, 100, 0, 0, 0, 0, 0, 41, 120000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Despawn After 2 mins'),
(@JALOOT, 0, 19,0, 61, 0, 100, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - Linked with Previous Event - Evade'),
(@JALOOT, 0, 20, 0, 9, 0, 100, 0, 0, 5, 15000, 20000, 11, 52943, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Range - Cast Lightning Whirl'),
(@JALOOT, 0, 21, 0, 9, 0, 100, 0, 0, 5, 15000, 18000, 11, 52944, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Range - Cast Lightning Strike'),
(@JALOOT, 0, 22, 0, 9, 0, 100, 0, 0, 5, 20000, 25000, 11, 52964, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Range - Cast Spark Frenzy'),
(@JALOOT, 0, 23, 0, 2, 0, 100, 0, 0, 30, 9000, 12000, 11, 52969, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Jaloot - On Less than 30% HP - Cast Energy Siphon');

DELETE FROM `creature_text` WHERE `entry` =@JALOOT;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@JALOOT, 0, 0, 'Now you not catch us with back turned.  Now we hurt you bad undead. BAD!', 12, 0, 100, 0, 0, 0, 'Jaloot');

UPDATE `creature_template` SET `AIName`= 'SmartAI',`npcflag`=0,`unit_flags`=32832 WHERE `entry`= @ZEPIK;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ZEPIK;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(@ZEPIK, 0, 0, 1, 11, 0, 100, 1, 0, 0, 0, 0, 81, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Spawn - Remove quest giver flag'),
(@ZEPIK, 0, 1, 2, 61, 0, 100, 1, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Set Unit Flags'),
(@ZEPIK, 0, 2, 0, 61, 0, 100, 0, 0, 0, 0, 0, 11, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Cast Tomb of the Heartless'),
(@ZEPIK, 0, 3, 4, 38, 0, 100, 0, 1, 1, 0, 0, 2, 14, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Data Set 1 1 - Set Hostile'),
(@ZEPIK, 0, 4, 5, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Remove Tomb of the Heartless'),
(@ZEPIK, 0, 5, 0, 61, 0, 100, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 18, 40, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Attack'),
(@ZEPIK, 0, 6, 7, 38, 0, 100, 0, 1, 2, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Data Set 1 2 - Set Friendly'),
(@ZEPIK, 0, 7, 8, 61, 0, 100, 0, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Set Unit Flags'),
(@ZEPIK, 0, 8 ,0, 61, 0, 100, 0, 0, 0, 0, 0, 11, 52182, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Cast Tomb of the Heartless'),
(@ZEPIK, 0, 9, 0, 6, 0, 100, 0, 0, 0, 0, 0, 45, 1, 2, 0, 0, 0, 0, 10, @ARTRUISGUID, @ARTRUIS, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Death - Send Data 1 2 to Artruis'),
(@ZEPIK, 0, 10, 11, 38, 0, 100, 0, 1, 5, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Data Set 1 5 - Set Friendly'),
(@ZEPIK, 0, 11, 12, 61, 0, 100, 0, 0, 0, 0, 0, 19, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Remove Unit Flags'),
(@ZEPIK, 0, 12, 13, 61, 0, 100, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Say Line 0'),
(@ZEPIK, 0, 13,14, 61, 0, 100, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0, 0, 0, 10, @ARTRUISGUID, @ARTRUIS, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Attack Artruis'),
(@ZEPIK, 0, 14,0, 61, 0, 100, 0, 0, 0, 0, 0, 28, 52185, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Remove Bindings of Submission'),
(@ZEPIK, 0, 15,16, 38, 0, 100, 0, 1, 4, 0, 0, 2, 250, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Data Set 1 4 - Set Friendly'),
(@ZEPIK, 0, 16,17, 61, 0, 100, 0, 0, 0, 0, 0, 18, 512, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Set Unit Flags'),
(@ZEPIK, 0, 17,18, 61, 0, 100, 0, 0, 0, 0, 0, 81, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Add quest giver flag'),
(@ZEPIK, 0, 18,19 ,61, 0, 100, 0, 0, 0, 0, 0, 41, 120000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Despawn After 2 mins'),
(@ZEPIK, 0, 19,0, 61, 0, 100, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - Linked with Previous Event - Evade'),
(@ZEPIK, 0, 20, 0, 9, 0, 100, 0, 0, 20, 15000, 18000, 11, 52761, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Range - Cast Barbed Net'),
(@ZEPIK, 0, 21, 0, 9, 0, 100, 0, 5, 30, 12000, 15000, 11, 52889, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Range - Cast Envenomed Shot'),
(@ZEPIK, 0, 22, 0, 9, 0, 100, 0, 0, 5, 15000, 18000, 11, 52873, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Range - Cast Open Wound'),
(@ZEPIK, 0, 23, 0, 9, 0, 100, 0, 5, 30, 3000, 7000, 11, 52758, 2, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Range - Cast Piercing Arrow'),
(@ZEPIK, 0, 24, 0, 9, 0, 100, 0, 0, 5, 30000, 40000, 11, 52886, 2, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Range - Cast Spike Trap'),
(@ZEPIK, 0, 25, 0, 2, 0, 100, 0, 0, 30, 30000, 30000, 11, 52895, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Zepik the Gorloc Hunter - On Less than 30% HP Cast Bandage - Cast Bandage');

DELETE FROM `creature_text` WHERE `entry` =@ZEPIK;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@ZEPIK, 0, 0, 'You going die big bad undead thing! You not catch Zepik sleeping this time!', 12, 0, 100, 0, 0, 0, 'Zepik the Gorloc Hunter');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceGroup`=1 AND `SourceEntry`=52185;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(13,1,52185,0,0,31,0,3,@ZEPIK,0,0,0,'','bindings of submission can hit Zepik the Gorloc Hunter'),
(13,1,52185,0,1,31,0,3,@JALOOT,0,0,0,'','bindings of submission can hit Jaloot'),
(13,1,52185,0,2,31,0,3,@ARTRUIS,0,0,0,'','bindings of submission can hit Artruis the Heartless');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=17 AND `SourceEntry` IN(51186,51188,51189,51190,51191,51192);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(17, 0, 51186, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Goregek the Bristlepine Hunter cannot be used near artruis'),
(17, 0, 51188, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Dajik the Wasp Hunter cannot be used near artruis'),
(17, 0, 51189, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Zepik the Gorloc Hunter cannot be used near artruis'),
(17, 0, 51190, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Lafoo cannot be used near artruis'),
(17, 0, 51191, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Jaloot cannot be used near artruis'),
(17, 0, 51192, 0, 0, 29, 0, 28659, 50, 0, 1, 0, 0, '', 'Summon Moodle cannot be used near artruis');

UPDATE `creature_template` SET `npcflag`=`npcflag`|2 WHERE `entry` IN (28667,28668);

-- Words for Delivery (25500)
UPDATE `quest_template` SET `Details` = 'Here we go. This is the speech, $N!$b$bIt has everything to be a great battle starter: It''s inspirational, has eloquent language and speaks directly to gnomes. Never has a finer speech been written!$b$bTurn this in to Captain Tread Sparknozzle and see if there''s anything else he needs of you.', `OfferRewardText` = 'This is the speech from Toby? Fantastic!$bRecruit, you''ve done all we could possibly ask for and the Gnomeregan Army thanks you!$bWe''ve got a strike team heading to Gnomeregan now and that wouldn''t have been possible without your assistance. For all of your help, we''d like you to join the Gnomeregan Reserves!$bIt doesn''t pay very well, but you do get this snazzy helmet. Hang on to that, you might need it someday!' WHERE `Id`=25500;

-- Words for Delivery (25286)
UPDATE `quest_template` SET `Details` = 'Here we go. This is the speech, $N!$b$bIt has everything to be a great battle starter: It''s inspirational, has eloquent language and speaks directly to gnomes. Never has a finer speech been written!$b$bTurn this in to Captain Tread Sparknozzle and get some transportation to High Tinker Mekkatorque!', `OfferRewardText` = 'Toby is all done with the speech? Let me see...$b<Reads the speech> Pumping pistons! What a fantastic speech! I knew Toby was a good writer, but this is perhaps the best he''s ever written.$bI think it''s time to get you out to the battle.' WHERE `Id`=25286;

--
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`=25791; 

DELETE FROM `smart_scripts` WHERE `entryorguid`=25791;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(25791, 0, 0, 0, 11, 0, 100, 0, 0, 0, 0, 0, 11, 45948, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - On Respawn - Cast Oil Coat'),
(25791, 0, 1, 0, 8 ,0, 100, 1, 53326, 0, 0, 0, 23, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - On Spellhit - Increment Phase'),
(25791, 0, 2, 3, 1 ,1, 100, 1, 1000, 1000, 0, 0, 2, 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - OOC - Set Faction'),
(25791, 0, 3, 4,61 ,1, 100, 1, 0, 0, 0, 0, 11, 46075, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Cast Summon Wolf Droppings'),
(25791, 0, 4, 5,61 ,1, 100, 1, 0, 0, 0, 0, 89, 10, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Set Random Movement'),
(25791, 0, 5, 0,61 ,1, 100, 1, 0, 0, 0, 0, 41, 15000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Despawn'),
(25791, 0, 6, 7, 0 ,1, 100, 1, 1000, 1000, 0, 0, 2, 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - IC - Set Faction'),
(25791, 0, 7, 8,61 ,1, 100, 1, 0, 0, 0, 0, 11, 46075, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Cast Summon Wolf Droppings'),
(25791, 0, 8, 9,61 ,1, 100, 1, 0, 0, 0, 0, 89, 10, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Set Random Movement'),
(25791, 0, 9,10,61 ,1, 100, 1, 0, 0, 0, 0, 41, 15000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Despawn'),
(25791, 0,10, 0,61 ,1, 100, 1, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Oil-stained Wolf - Linked with Previous Event - Evade');

-- [11258] [11247] Burn Skorn ,Burn

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(24098,24100,24102);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (24098,24100,24102);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(24102, 0, 0, 0, 8,  0, 100, 0, 43057, 0, 0, 0, 11, 43065 , 2, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Barraks Bunny - On Spellhit  - Cast Burn Skorn, Burn!: Barracks Kill Credit'),
(24098, 0, 0, 0, 8,  0, 100, 0, 43057, 0, 0, 0, 11, 43058 , 2, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Longhouse NW Bunny - On Spellhit - Cast Burn Skorn, Burn!: NW Kill Credit'),
(24100, 0, 0, 0, 8,  0, 100, 0, 43057, 0, 0, 0, 11, 43061 , 2, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Longhouse NE Bunny - On Spellhit - Cast Burn Skorn, Burn!: NE Kill Credit');

-- [11259] [11245] Towers of Certain Doom

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry` IN(24087,24092,24093,24094);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (24087,24092,24093,24094) AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid` =2408700 AND `source_type`=9;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(24087, 0, 0, 2, 8,  0, 100, 0, 49625, 0, 15000, 15000, 11, 43067 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower NW Bunny - On Spellhit - Cast Towers of Certain Doom: NW Kill Credit'),
(24092, 0, 0, 2, 8,  0, 100, 0, 49625, 0, 15000, 15000, 11, 43077 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower E Bunny - On Spellhit - Cast Towers of Certain Doom: E Kill Credit'),
(24093, 0, 0, 2, 8,  0, 100, 0, 49625, 0, 15000, 15000, 11, 43086 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SW Bunny - On Spellhit - Cast Towers of Certain Doom: SW Kill Credit'),
(24094, 0, 0, 2, 8,  0, 100, 0, 49625, 0, 15000, 15000, 11, 43087 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SE Bunny - On Spellhit - Cast Towers of Certain Doom: SE Kill Credit'),
(24087, 0, 1, 2, 8,  0, 100, 0, 49634, 0, 15000, 15000, 11, 43067 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower NW Bunny - On Spellhit - Cast Towers of Certain Doom: NW Kill Credit'),
(24092, 0, 1, 2, 8,  0, 100, 0, 49634, 0, 15000, 15000, 11, 43077 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower E Bunny - On Spellhit - Cast Towers of Certain Doom: E Kill Credit'),
(24093, 0, 1, 2, 8,  0, 100, 0, 49634, 0, 15000, 15000, 11, 43086 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SW Bunny - On Spellhit - Cast Towers of Certain Doom: SW Kill Credit'),
(24094, 0, 1, 2, 8,  0, 100, 0, 49634, 0, 15000, 15000, 11, 43087 , 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SE Bunny - On Spellhit - Cast Towers of Certain Doom: SE Kill Credit'),
(24087, 0, 2, 0, 61,  0, 100, 0, 0, 0, 0, 0, 80, 2408700 , 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower NW Bunny - Linked with Previous Event - Run Timed Script'),
(24092, 0, 2, 0, 61,  0, 100, 0, 0, 0, 0, 0, 80, 2408700 , 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower E Bunny - Linked with Previous Event - Run Timed Script'),
(24093, 0, 2, 0, 61,  0, 100, 0, 0, 0, 0, 0, 80, 2408700 , 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SW Bunny - Linked with Previous Event - Run Timed Script'),
(24094, 0, 2, 0, 61,  0, 100, 0, 0, 0, 0, 0, 80, 2408700 , 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower SE Bunny - Linked with Previous Event - Run Timed Script'),
(2408700, 9, 0, 0, 0,  0, 100, 0, 2000, 2000, 0, 0, 11, 56511 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower Bunny - Script - Cast Towers of Certain Doom: Tower Bunny Smoke Flare Effect'),
(2408700, 9, 1, 0, 0,  0, 100, 0, 15000, 15000, 0, 0, 11, 43069 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower Bunny - Script - Cast Towers of Certain Doom: Skorn Cannonfire'),
(2408700, 9, 2, 0, 0,  0, 100, 0, 100, 100, 0, 0, 85, 43072 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Skorn Tower Bunny - Script - Cast Towers of Certain Doom: Tower Caster Instakill');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13  AND `SourceEntry`IN(49625,49634,43072);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13,1,49625,0,0,31,0,3,24087,0,0,0,0,'','Braves Flare targets Skorn Tower NW Bunny'),
(13,1,49625,0,1,31,0,3,24092,0,0,0,0,'','Braves Flare targets Skorn Tower E Bunny'),
(13,1,49625,0,2,31,0,3,24093,0,0,0,0,'','Braves Flare targets Skorn Tower SW Bunny'),
(13,1,49625,0,3,31,0,3,24094,0,0,0,0,'','Braves Flare targets Skorn Tower SE Bunny'),
(13,1,49634,0,0,31,0,3,24087,0,0,0,0,'','Seargents Flare targets Skorn Tower NW Bunny'),
(13,1,49634,0,1,31,0,3,24092,0,0,0,0,'','Seargents Flare targets Skorn Tower E Bunny'),
(13,1,49634,0,2,31,0,3,24093,0,0,0,0,'','Seargents Flare targets Skorn Tower SW Bunny'),
(13,1,49634,0,3,31,0,3,24094,0,0,0,0,'','Seargents Flare targets Skorn Tower SE Bunny'),
(13,1,43072,0,0,31,0,3,23668,0,0,0,0,'','Towers of Certain Doom: Tower Caster Instakill targets Winterskorn Rune-Caster'),
(13,1,43072,0,1,31,0,3,23667,0,0,0,0,'','Towers of Certain Doom: Tower Caster Instakill targets Winterskorn Rune-Seer'),
(13,1,43072,0,2,31,0,3,23669,0,0,0,0,'','Towers of Certain Doom: Tower Caster Instakill targets Winterskorn Oracle');

--
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`= 9376;

DELETE FROM `creature_ai_scripts` WHERE `creature_id`=9376;

DELETE FROM `smart_scripts` WHERE `entryorguid`=9376 AND `source_type`=0;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(9376, 0, 0, 0,11, 0, 100, 0, 0, 0, 0, 0, 11, 13913, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Blazerunner - On Spawn - Cast Blazerunner Aura'),
(9376, 0, 1, 0, 9, 0, 100, 0, 0, 10, 10000, 20000, 11, 17277, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Blazerunner - On Range - Cast Blast Wave'),
(9376, 0, 2, 0, 8, 0, 100, 0, 14247, 0, 0, 0, 28, 13913, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Blazerunner - On Spellhit (Blazerunner Dispel) - Remove Blazerunner Aura');

--
UPDATE `creature_template` SET `gossip_menu_id`=10036 WHERE  `entry`=31273;

DELETE FROM `gossip_menu` WHERE `entry`IN(10036,10037);
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
(10036, 13930),
(10037, 13931);

DELETE FROM `gossip_menu_option` WHERE `menu_id`=10036;
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES
(10036, 0, 0, 'Hold on friend. Tell me what happened here.', 1, 1, 10037, 0, 0, 0, NULL);

UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE  `entry`=31273;

DELETE FROM `smart_scripts` WHERE `entryorguid` =31273;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(31273, 0, 0, 0, 62, 0, 100, 0, 10036, 0, 0, 0, 33, 31272, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Dying Berserker - On Gossip Select - Give Kill Credit');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=10036;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`SourceId`,`ElseGroup`,`ConditionTypeOrReference`,`ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`ConditionValue3`,`NegativeCondition`,`ErrorTextId`,`ScriptName`,`Comment`) VALUES
(15,10036,0,0,0,9,0,13228,0,0,0,0,'','Dying Berserker show gossip only if player on quest The Broken Front');

--
DELETE FROM `smart_scripts` WHERE `entryorguid`=6497;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(6497, 0, 0, 1, 62, 0, 100, 0, 125, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - On Gossip Select - Close Menu'),
(6497, 0, 1, 2, 61, 0, 100, 0, 0, 0, 0, 0, 2, 14, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - Linked with Previous Event - Set Hostile'),
(6497, 0, 2, 0, 61, 0, 100, 0, 0, 0, 0, 0, 55, 120000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - Linked with Previous Event - Stop WP'),
(6497, 0, 3, 4, 11, 0, 100, 0, 0, 0, 0, 0, 2, 68, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - On Spawn - Set Friendly'),
(6497, 0, 4, 0, 61, 0, 100, 0, 0, 0, 0, 0, 53, 0, 6497, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - Linked with previous Event - Start WP'),
(6497, 0, 5, 0, 64, 0, 100, 0, 0, 0, 0, 0, 54, 15000, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'Astor Hadren - On Gossip Hello - Pause WP');

UPDATE `creature` SET `MovementType`=0 WHERE  `guid`=44738;
UPDATE `creature_addon` SET `path_id`=0 WHERE  `guid`=44738;

DELETE FROM  `waypoint_data` WHERE `id`=447380;
DELETE FROM `waypoints` WHERE `entry`=6497;
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(6497, 1, 1597.4, 566.768, 37.8602, 'Astor Hadren'),
(6497, 2, 1613.91, 562.333, 35.146, 'Astor Hadren'),
(6497, 3, 1631.83, 557.812, 33.7566, 'Astor Hadren'),
(6497, 4, 1661.1, 550.278, 33.3758, 'Astor Hadren'),
(6497, 5, 1689.98, 545.014, 33.4033, 'Astor Hadren'),
(6497, 6, 1714.5, 540.561, 33.6137, 'Astor Hadren'),
(6497, 7, 1737.88, 533.75, 33.3961, 'Astor Hadren'),
(6497, 8, 1772.23, 525.741, 33.3961, 'Astor Hadren'),
(6497, 9, 1802.45, 516.861, 33.4219, 'Astor Hadren'),
(6497, 10, 1836.05, 504.973, 34.1916, 'Astor Hadren'),
(6497, 11, 1850.21, 496.778, 34.6882, 'Astor Hadren'),
(6497, 12, 1867.39, 481.534, 34.5889, 'Astor Hadren'),
(6497, 13, 1877.67, 463.224, 34.1472, 'Astor Hadren'),
(6497, 14, 1890.49, 434.482, 33.8649, 'Astor Hadren'),
(6497, 15, 1898.6, 404.11, 34.3546, 'Astor Hadren'),
(6497, 16, 1909.06, 371.483, 34.0201, 'Astor Hadren'),
(6497, 17, 1927.47, 333.806, 35.1605, 'Astor Hadren'),
(6497, 18, 1948.33, 293.56, 38.7782, 'Astor Hadren'),
(6497, 19, 1960.17, 272.962, 38.3855, 'Astor Hadren'),
(6497, 20, 1981.05, 237.328, 36.6631, 'Astor Hadren'),
(6497, 21, 1955.89, 236.574, 41.3907, 'Astor Hadren'),
(6497, 22, 1922.53, 240.366, 49.3415, 'Astor Hadren'),
(6497, 23, 1938.41, 259.961, 44.9292, 'Astor Hadren'),
(6497, 24, 1949.26, 286.982, 38.8665, 'Astor Hadren'),
(6497, 25, 1941.21, 306.375, 37.5792, 'Astor Hadren'),
(6497, 26, 1927.42, 334.68, 35.107, 'Astor Hadren'),
(6497, 27, 1912.91, 362.632, 33.9933, 'Astor Hadren'),
(6497, 28, 1899.77, 399.685, 34.2583, 'Astor Hadren'),
(6497, 29, 1894.23, 419.938, 34.2863, 'Astor Hadren'),
(6497, 30, 1883.81, 449.623, 33.9371, 'Astor Hadren'),
(6497, 31, 1865.35, 482.581, 34.6089, 'Astor Hadren'),
(6497, 32, 1839.53, 502.555, 34.3171, 'Astor Hadren'),
(6497, 33, 1810.76, 513.551, 33.4393, 'Astor Hadren'),
(6497, 34, 1780.72, 522.962, 33.3959, 'Astor Hadren'),
(6497, 35, 1744.62, 532.56, 33.3959, 'Astor Hadren'),
(6497, 36, 1713.98, 539.842, 33.5092, 'Astor Hadren'),
(6497, 37, 1683.12, 546.073, 33.4104, 'Astor Hadren'),
(6497, 38, 1642.1, 554.955, 33.448, 'Astor Hadren'),
(6497, 39, 1612.29, 563.53, 35.3895, 'Astor Hadren');

-- Template updates for creature 27263 (Vordrassil's Heart Credit)
UPDATE `creature_template` SET `unit_flags`=`unit_flags`|33555200 WHERE `entry`=27263;

--
DELETE FROM `npc_vendor` WHERE `entry` IN(33238,33239);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`) VALUES
(33238, 24, 33443, 0, 0, 0),
(33238, 26, 35952, 0, 0, 0),
(33238, 23, 33454, 0, 0, 0),
(33238, 21, 33449, 0, 0, 0),
(33238, 22, 33451, 0, 0, 0),
(33238, 25, 35949, 0, 0, 0),
(33238, 30, 35950, 0, 0, 0),
(33238, 27, 35953, 0, 0, 0),
(33238, 28, 35951, 0, 0, 0),
(33238, 29, 35948, 0, 0, 0),
(33238, 2, 5237, 0, 0, 0),
(33238, 3, 5565, 0, 0, 0),
(33238, 4, 16583, 0, 0, 0),
(33238, 1, 3775, 0, 0, 0),
(33238, 6, 17030, 0, 0, 0),
(33238, 9, 17033, 0, 0, 0),
(33238, 10, 21177, 0, 0, 0),
(33238, 8, 17032, 0, 0, 0),
(33238, 5, 17020, 0, 0, 0),
(33238, 7, 17031, 0, 0, 0),
(33238, 16, 43235, 0, 0, 0),
(33238, 13, 41586, 0, 0, 0),
(33238, 11, 37201, 0, 0, 0),
(33238, 14, 43231, 0, 0, 0),
(33238, 15, 43233, 0, 0, 0),
(33238, 18, 44605, 0, 0, 0),
(33238, 19, 44614, 0, 0, 0),
(33238, 17, 43237, 0, 0, 0),
(33238, 12, 41584, 0, 0, 0),
(33238, 20, 44615, 0, 0, 0),
(33239, 24, 33443, 0, 0, 0),
(33239, 26, 35952, 0, 0, 0),
(33239, 23, 33454, 0, 0, 0),
(33239, 21, 33449, 0, 0, 0),
(33239, 22, 33451, 0, 0, 0),
(33239, 25, 35949, 0, 0, 0),
(33239, 30, 35950, 0, 0, 0),
(33239, 27, 35953, 0, 0, 0),
(33239, 28, 35951, 0, 0, 0),
(33239, 29, 35948, 0, 0, 0),
(33239, 2, 5237, 0, 0, 0),
(33239, 3, 5565, 0, 0, 0),
(33239, 4, 16583, 0, 0, 0),
(33239, 1, 3775, 0, 0, 0),
(33239, 6, 17030, 0, 0, 0),
(33239, 9, 17033, 0, 0, 0),
(33239, 10, 21177, 0, 0, 0),
(33239, 8, 17032, 0, 0, 0),
(33239, 5, 17020, 0, 0, 0),
(33239, 7, 17031, 0, 0, 0),
(33239, 16, 43235, 0, 0, 0),
(33239, 13, 41586, 0, 0, 0),
(33239, 11, 37201, 0, 0, 0),
(33239, 14, 43231, 0, 0, 0),
(33239, 15, 43233, 0, 0, 0),
(33239, 18, 44605, 0, 0, 0),
(33239, 19, 44614, 0, 0, 0),
(33239, 17, 43237, 0, 0, 0),
(33239, 12, 41584, 0, 0, 0),
(33239, 20, 44615, 0, 0, 0);

-- DB/Quest: Fix: Wooly Justice (12707)
SET @MEDALLION := 52596; -- Spell: Medallion of Mam'toth
SET @MAMMOTH   := 28851; -- NPC: Enraged Mammoth
SET @TRAMPLE   := 52603; -- Spell: Trample
SET @T_AURA    := 52607; -- Spell: Enraged Mammoth: Trample Aura for On Death Kill Credit
SET @DISCIPLE  := 28861; -- NPC: Mam'toth Disciple
SET @CREDIT    := 28876; -- NPC: Mam'toth Disciple Kill Credit Bunny

-- Add SAI for Enraged Mammoth
UPDATE `creature_template` SET `AIName`='SmartAI',`spell1`=52601,`spell2`=@TRAMPLE WHERE `entry`=@MAMMOTH;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@MAMMOTH AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@MAMMOTH,0,0,0,8,0,100,0,@MEDALLION,0,0,0,2,35,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Enraged Mammoth - On hit by spell from medallion - Change faction to friendly'),
(@MAMMOTH,0,1,0,1,0,100,0,10000,10000,10000,10000,2,1924,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Enraged Mammoth - On OOC for 10 sec - Change faction to back to normal'),
(@MAMMOTH,0,2,0,1,0,100,0,10000,10000,10000,10000,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Enraged Mammoth - On OOC for 10 sec - DESPAWN');

-- Add SAI for Mam'toth disciple
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@DISCIPLE;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@DISCIPLE AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@DISCIPLE,0,0,0,6,0,100,0,0,0,0,0,33,@CREDIT,0,0,0,0,0,7,0,0,0,0,0,0,0, 'Mam''toth disciple - On death - Give credit to invoker, if Tampered'),
(@DISCIPLE,0,1,0,25,0,100,0,0,0,0,0,28,@T_AURA,0,0,0,0,0,1,0,0,0,0,0,0,0, 'Mam''toth disciple - On reset - Remove aura from trample');

-- Add conditions for spell Medallion of Mam'toth
DELETE FROM `conditions` WHERE `SourceEntry`=@MEDALLION AND `SourceTypeOrReferenceId`=17;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(17,0,@MEDALLION,0,0,31,1,3,@MAMMOTH,0,0,0,'', 'Medallion of Mam''toth can hit only Enraged Mammoths');

-- Add conditions for spell Trample
DELETE FROM `conditions` WHERE `SourceEntry`=@TRAMPLE AND `SourceTypeOrReferenceId`=13;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(13,1,@TRAMPLE,0,0,31,0,3,@DISCIPLE,0,0,0,'', 'Trample effect 1 can hit only hit disciple of Mam''toth'),
(13,2,@TRAMPLE,0,0,31,0,3,@DISCIPLE,0,0,0,'', 'Trample effect 2 can hit only hit disciple of Mam''toth');

-- Add conditions for smart_event 0 of Mam'toth disciple
DELETE FROM `conditions` WHERE `SourceEntry`=@DISCIPLE AND `SourceTypeOrReferenceId`=22;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(22,1,@DISCIPLE,0,0,1,1,@T_AURA,0,0,0,0,'', 'Mam''toth disciple 1st event is valid only, if has Tampered aura credit');

-- Add conditions for spell Trample Aura
DELETE FROM `conditions` WHERE `SourceEntry`=@T_AURA AND `SourceTypeOrReferenceId`=13;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(13,1,@T_AURA,0,0,31,0,3,@DISCIPLE,0,0,0,'', 'TAura effect can hit only Disciple of Mam''toth');

SET @MAMMOTH   := 28851; -- NPC: Enraged Mammoth

DELETE FROM `smart_scripts` WHERE `entryorguid`=@Mammoth AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@Mammoth,0,0,1,8,0,100,0,52596,0,0,0,2,35,0,0,0,0,0,1,0,0,0,0,0,0,0,'Enraged Mammoth - On Spell Hit(Medallion of Mam''toth) - Change Faction To friendly'),
(@Mammoth,0,1,0,61,0,100,0,0,0,0,0,81,16777216,0,0,0,0,0,1,0,0,0,0,0,0,0,'Enraged Mammoth - Link With Previous Event - Set Flags 16777216'),
(@Mammoth,0,2,3,1,0,100,0,10000,10000,10000,10000,2,1924,0,0,0,0,0,1,0,0,0,0,0,0,0,'Enraged Mammoth - On OOC for 10 sec - Change Faction Back To Normal'),
(@Mammoth,0,3,0,61,0,100,0,0,0,0,0,81,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Enraged Mammoth - Link With Previous Event - Restore flags'),
(@Mammoth,0,4,0,28,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Enraged Mammoth - On Passenger Removed - Despawn');

--
UPDATE `creature_template` SET `AIName`= 'SmartAI',`ScriptName`='' WHERE `entry` =29811;
DELETE FROM `smart_scripts` WHERE `entryorguid` =29811;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(29811,0,0,1,62,0,100,0,9843,0,0,0,15,12864,0,0,0,0,0,7,0,0,0,0,0,0,0,'Frostborn Scout - On Gossip Select - Give Quest Credit'),
(29811,0,1,0,61,0,100,0,0,0,0,0,41,30000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Frostborn Scout - Linked with Previous Event - Despawn'),
(29811,0,2,0,25,0,100,0,0,0,0,0,90,7,0,0,0,0,0,1,0,0,0,0,0,0,0,'Frostborn Scout - On Spawn - Set Bytes 1');

UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=16236; -- Eye Stalk
UPDATE `creature_template` SET `speed_run`=1.14286 WHERE `entry`=28948; -- Malmortis

--
UPDATE `creature_template` SET `ainame`='SmartAI' WHERE `entry` IN (20813,20815,20816,20814);

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (20813,20815,20816,20814) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(20813,0,0,0,8,0,100,0,35724,0,0,0,33,20813,0,0,0,0,0,7,0,0,0,0,0,0,0,'Zeth''Gor Quest Credit Marker, Barracks - On Spell Hit (Throw Torch) - Quest Credit'),
(20814,0,0,0,8,0,100,0,35724,0,0,0,33,20814,0,0,0,0,0,7,0,0,0,0,0,0,0,'Zeth''Gor Quest Credit Marker, Stable - On Spell Hit (Throw Torch) - Quest Credit'),
(20815,0,0,0,8,0,100,0,35724,0,0,0,33,20815,0,0,0,0,0,7,0,0,0,0,0,0,0,'Zeth''Gor Quest Credit Marker, East Hovel - On Spell Hit (Throw Torch) - Quest Credit'),
(20816,0,0,0,8,0,100,0,35724,0,0,0,33,20816,0,0,0,0,0,7,0,0,0,0,0,0,0,'Zeth''Gor Quest Credit Marker, West Hovel - On Spell Hit (Throw Torch) - Quest Credit');

--
UPDATE `creature_template` SET `npcflag`=129 WHERE `entry` = 33238;
UPDATE `creature_template` SET `npcflag`=129 WHERE `entry` = 33239;
UPDATE `creature_template` SET `speed_walk`=1, `speed_run`=1 WHERE `entry`=29613; -- Eye Stalk (1)

UPDATE `creature_template` SET `npcflag`=16777216 WHERE `entry` IN (35415, 35413, 35419, 35429); -- Demolisher, Catapult, Glaive Thrower & Keep Cannon

DELETE FROM `npc_spellclick_spells` WHERE `npc_entry` IN (35415, 35413, 35419, 35429);
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
(35415, 66245, 1, 0), -- Demolisher (Ride Vehicle)
(35413, 66245, 1, 0), -- Catapult (Ride Vehicle)
(35419, 68503, 1, 0), -- Glaive Thrower (Ride Vehicle)
(35429, 68458, 1, 0); -- Keep Cannon (Keep Cannon)

-- 10913 An Improper Burial

DELETE FROM `creature_ai_scripts` WHERE  `creature_id` IN(21859,21846,21869);

UPDATE creature_template SET AIName='SmartAI' WHERE entry IN(21859,21846,21869);

DELETE FROM `smart_scripts` WHERE `entryorguid`IN(21859,21846,21869) AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(21859,0,0,1,8,0,100,1,39189,0,0,0,11,59216,0,0,0,0,0,1,0,0,0,0,0,0,0,'Slain Sha tar Vindicator - On Spellhit (Sha tari Torch) - Cast Burning Corpse'),
(21859,0,1,2,61,0,100,0,0,0,0,0,33,21859,0,0,0,0,0,7,0,0,0,0,0,0,0,'Slain Sha tar Vindicator - Linked with Previous Event - Give Kill Credit'),
(21859,0,2,3,61,0,100,0,0,0,0,0,11,37759,0,0,0,0,0,0,0,0,0,0,0,0,0, 'Slain Sha tar Vindicator - Linked with Previous Event - Cast Bone Wastes - Summon Draenei Guardian Spirit'),
(21859,0,3,0,61,0,100,0,0,0,0,0,41,15000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Slain Sha tar Vindicator - Linked with Previous Event - Despawn after 15 seconds'),
(21846,0,0,1,8,0,100,1,39189,0,0,0,11,59216,0,0,0,0,0,1,0,0,0,0,0,0,0,'Slain Auchenai Warrior - On Spellhit (Sha tari Torch) - Cast Burning Corpse'),
(21846,0,1,2,61,0,100,0,0,0,0,0,33,21846,0,0,0,0,0,7,0,0,0,0,0,0,0,'Slain Auchenai Warrior - Linked with Previous Event - Give Kill Credit'),
(21846,0,2,0,61,0,100,0,0,0,0,0,41,15000,0,0,0,0,0,1,0,0,0,0,0,0,0,'Slain Auchenai Warrior - Linked with Previous Event - Despawn after 15 seconds'),
(21869,0,0,1,11,0,100,0,0,0,0,0,89,5,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,'Unliving Guardian - Link - Set Rendom Move'),
(21869,0,1,0,61,0,100,0,0,0,0,0,41,300000,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,'Unliving Guardian - On Spawn - Force Despawn 5 min');

DELETE FROM `creature_template_addon` WHERE  `entry`=21869;
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES 
(21869, 0, 0, 0, 4097, 0, '17327');

--
DELETE FROM `smart_scripts` WHERE `entryorguid`=28518 AND `source_type`=0 AND `id` BETWEEN 5 AND 8;

INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(28518, 0, 5, 0, 19, 0, 100, 0, 12676, 0, 0, 0, 85, 55413, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Stefan Vadu - On quest accept (Sabotage) - Cast Clearquest'),
(28518, 0, 6, 0, 19, 0, 100, 0, 12669, 0, 0, 0, 85, 55411, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Stefan Vadu - On quest accept (So Far, So Bad) - Cast Clearquest'),
(28518, 0, 7, 0, 19, 0, 100, 0, 12677, 0, 0, 0, 85, 55412, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Stefan Vadu - On quest accept (Hazardous Materials) - Cast Clearquest'),
(28518, 0, 8, 0, 19, 0, 100, 0, 12661, 0, 0, 0, 85, 55410, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 'Stefan Vadu - On quest accept (Infiltrating Voltarus) - Cast Clearquests');

--
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=30098;

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(13,7,30098,0,0,31,0,3,17253,0,0,0, '', 'Defile Uthers Tomb Targets Defile Uthers Tomb Trigger'),
(13,7,30098,0,1,31,0,5,181653,0,0,0, '', 'Defile Uthers Tomb Targets Temp Uthers Statue');

DELETE FROM `smart_scripts` WHERE `entryorguid` IN(17253,1854) AND `source_type`=0;

UPDATE creature_template SET `InhabitType`=4 WHERE entry IN(17233);
UPDATE creature_template SET AIName='SmartAI' WHERE entry IN(17253,1854);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=17 AND `SourceEntry` =30098;

INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
(17, 0, 30098, 0, 0, 29, 0, 17233, 40, 0, 1, 0, 0, '', 'Cant cast Defile Uthers Tomb within 40 yards of Ghost of Uther Lightbringer');


INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(17253,0,0,0,8,0,100,0,30098,0,60000,60000,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Defile Uthers Tomb Trigger - On Spellhit - Set Phase 2'),
(17253,0,1,2,1,2,100,0,5000,5000,5000,5000,1,4,10000,0,0,0,0,9,17233,0,200,0,0,0,0,'Defile Uthers Tomb Trigger - OOC (Phase 2) - Say Line 4 on Ghost of Uther the Lightbringer'),
(17253,0,2,0,61,2,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Defile Uthers Tomb Trigger - Linked with Previous Event (Phase 2) - Set Phase 1'),
(17253,0,3,0,52,0,100,0,4,17233,0,0,1,5,10000,0,0,0,0,9,17233,0,200,0,0,0,0,'Defile Uthers Tomb Trigger - On Text Over on Ghost of Uther (line 4) - Say Line 5 on Ghost of Uther the Lightbringer'),
(17253,0,4,0,52,0,100,0,5,17233,0,0,1,6,10000,0,0,0,0,9,17233,0,200,0,0,0,0,'Defile Uthers Tomb Trigger - On Text Over on Ghost of Uther (line 5) - Say Line 6 on Ghost of Uther the Lightbringer'),
(17253,0,5,0,52,0,100,0,6,17233,0,0,1,7,10000,0,0,0,0,9,17233,0,200,0,0,0,0,'Defile Uthers Tomb Trigger - On Text Over on Ghost of Uther (line 6) - Say Line 7 on Ghost of Uther the Lightbringer'),
(1854,0,0,0,4,0,100,0,0,0,0,0,1,1,10000,0,0,0,0,1,0,0,0,0,0,0,0,'High Priest Thel danis - On Agro - Say Line 1'),
(1854,0,1,0,16,0,100,0,10951,50,6000,6000,11,10951,0,0,0,0,0,1,0,0,0,0,0,0,0,'High Priest Thel danis - On Missing Inner Fire (Rank 4) - Cast Inner Fire (Rank 4)'),
(1854,0,2,0,2,0,100,0,0,50,15000,15000,11,30155,0,0,0,0,0,1,0,0,0,0,0,0,0,'High Priest Thel danis - On Low HP - Cast Heal'),
(1854,0,3,0,0,0,100,0,0,0,2500,3000,11,15498,0,0,0,0,0,2,0,0,0,0,0,0,0,'High Priest Thel danis - IC - Cast Holy Smite'),
(1854,0,4,0,25,0,100,0,0,0,0,0,11,10591,0,0,0,0,0,1,0,0,0,0,0,0,0,'High Priest Thel danis - On Spawn - Cast Inner Fire (Rank 4)');

-- Event script to spawn Ghost of Uther the Lightbringer in correct place, seems the trigger npc is in exact spot where spawns in video so using the co-ordinates of that

DELETE FROM `event_scripts` WHERE `id`=10561;
INSERT INTO `event_scripts` (`id`, `delay`, `command`, `datalong`, `datalong2`, `dataint`, `x`, `y`, `z`, `o`) VALUES 
(10561, 0, 10, 17233, 90000, 0, 972.969971, -1824.719971, 82.540703, 0.2935);

-- Texts groupid 0,1,2,3 are for alliance quest so starting from 4 here

DELETE FROM `creature_text` WHERE `entry`=17233 AND `groupid` BETWEEN 4 AND 7;
DELETE FROM `creature_text` WHERE `entry`=1854 AND `groupid`=1;

INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES 
(17233, 4, 0, 'Why do you do this? Did I somehow wrong you in life?', 12, 0, 100, 0, 0, 0, 'Ghost of Uther Lightbringer '),
(17233, 5, 0, 'Ah, I see it now in your mind. This is the work of one of my former students... Mehlar Dawnblade. It is sad to know that his heart has turned so dark.', 12, 0, 100, 0, 0, 0, 'Ghost of Uther Lightbringer'),
(17233, 6, 0, 'Return to him. Return to Mehlor and tell him that I forgive him and that I understand why he believes what he does.', 12, 0, 100, 0, 0, 0, 'Ghost of Uther Lightbringer '),
(17233, 7, 0, 'I can only hope that he will see the Light and instead turn his energies to restoring once-beautiful Quel''Thalas.', 12, 0, 100, 0, 0, 0, 'Ghost of Uther Lightbringer '),
(1854, 1, 0, 'By the Light you will leave this tomb!', 12, 0, 100, 0, 0, 0, 'High Priest Thel danis');

UPDATE `smart_scripts` SET `action_param1`=10951 WHERE  `entryorguid`=1854 AND `source_type`=0 AND `id`=4 AND `link`=0;

UPDATE `creature_formations` SET `point_1`=18, `point_2`=36 WHERE `memberGUID`=127058;
UPDATE `creature_formations` SET `point_1`=9, `point_2`=17 WHERE `memberGUID` IN (202695,202696);
UPDATE `creature_formations` SET `point_1`=3, `point_2`=6 WHERE `memberGUID`=201735;
UPDATE `creature_formations` SET `point_1`=3, `point_2`=7 WHERE `memberGUID` IN (202680,202682);
UPDATE `creature_formations` SET `point_1`=3, `point_2`=8 WHERE `memberGUID` IN (201764);
UPDATE `creature_formations` SET `point_1`=2, `point_2`=5 WHERE `memberGUID` IN (202805,202806);
UPDATE `creature_formations` SET `point_1`=1, `point_2`=6 WHERE `memberGUID` IN (202803,202804,202681,202684);

UPDATE `smart_scripts` SET `link`=0, `action_type`=11, `action_param1`=43787, `action_param2`=0 WHERE `entryorguid`=24399 AND `id`=5;

UPDATE `creature_template` SET `speed_walk`=1.0 WHERE `entry` IN (2674,25855,28887);
UPDATE `creature_template` SET `speed_run`=1.14286,`speed_walk`=1.0 WHERE `entry`=28833;

-- Creature text for Wrath-Scryer Soccothrates
SET @ENTRY = 20886;
DELETE FROM `creature_text` WHERE `entry`=@ENTRY;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@ENTRY, 0, 0, 'Have you come to kill Dalliah? Can I watch?',     14, 0, 100, 0, 0, 11237, 'Wrath-Scryer Soccothrates - OCC'),
(@ENTRY, 1, 0, 'At last, a target for my frustrations!',          14, 0, 100, 0, 0, 11238, 'Wrath-Scryer Soccothrates - Aggro'),
(@ENTRY, 2, 0, 'Yes, that was quite satisfying',                  14, 0, 100, 0, 0, 11239, 'Wrath-Scryer Soccothrates - Kill'),
(@ENTRY, 3, 0, 'On guard!',                                       14, 0, 100, 0, 0, 11241, 'Wrath-Scryer Soccothrates - Charge'),
(@ENTRY, 3, 1, 'Defend yourself, for all the good it will do...', 14, 0, 100, 0, 0, 11242, 'Wrath-Scryer Soccothrates - Charge'),
(@ENTRY, 4, 0, 'Knew this was... the only way out',               14, 0, 100, 0, 0, 11243, 'Wrath-Scryer Soccothrates - Death');

-- Creature text for Dalliah the Doomsayer
SET @ENTRY = 20885;
DELETE FROM `creature_text` WHERE `entry`=@ENTRY;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@ENTRY, 0, 0, 'Don''t worry about me; kill that worthless dullard instead!', 14, 0, 100, 0, 0, 11085, 'Dalliah the Doomsayer - Aggro Soccothrates First'),
(@ENTRY, 1, 0, 'It is unwise to anger me.',                                   14, 0, 100, 0, 0, 11086, 'Dalliah the Doomsayer - Aggro'),
(@ENTRY, 2, 0, 'Completely ineffective. Just like someone else I know.',      14, 0, 100, 0, 0, 11087, 'Dalliah the Doomsayer - Kill'),
(@ENTRY, 2, 1, 'You chose the wrong opponent.',                               14, 0, 100, 0, 0, 11088, 'Dalliah the Doomsayer - Kill'),
(@ENTRY, 3, 0, 'Reap the Whirlwind!',                                         14, 0, 100, 0, 0, 11089, 'Dalliah the Doomsayer - Cast Whirlwind'),
(@ENTRY, 3, 1, 'I''ll cut you to pieces!',                                    14, 0, 100, 0, 0, 11090, 'Dalliah the Doomsayer - Cast Whirlwind'),
(@ENTRY, 4, 0, 'Ahh... That is much better.',                                 14, 0, 100, 0, 0, 11091, 'Dalliah the Doomsayer - Cast Heal'),
(@ENTRY, 4, 1, 'Ahh... Just what I needed.',                                  14, 0, 100, 0, 0, 11092, 'Dalliah the Doomsayer - Cast Heal'),
(@ENTRY, 5, 0, 'Now I''m really... angry...',                                 14, 0, 100, 0, 0, 11093, 'Dalliah the Doomsayer - Death'),
(@ENTRY, 6, 0, 'More than you can handle, scryer?',                           14, 0, 100, 0, 0, 11094, 'Dalliah the Doomsayer - Soccothrates at 25%'),
(@ENTRY, 6, 1, 'I suppose I''ll end up fighting them all myself.',            14, 0, 100, 0, 0, 11095, 'Dalliah the Doomsayer - Soccothrates at 25%'),
(@ENTRY, 6, 2, 'I''ve grown used to cleaning up your messes.',                14, 0, 100, 0, 0, 11096, 'Dalliah the Doomsayer - Soccothrates at 25%'),
(@ENTRY, 7, 0, 'Congratulations. I''ve wanted to do that for years.',         14, 0, 100, 0, 0, 11097, 'Dalliah the Doomsayer - Soccothratess dies');

-- Creature text for Wrath-Scryer Soccothrates
SET @ENTRY = 20886;
DELETE FROM `creature_text` WHERE `entry`=@ENTRY;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@ENTRY, 0, 0, 'Have you come to kill Dalliah? Can I watch?',                                             14, 0, 100, 0, 0, 11237, 'Wrath-Scryer Soccothrates - Aggro Dalliah First'),
(@ENTRY, 1, 0, 'At last, a target for my frustrations!',                                                  14, 0, 100, 0, 0, 11238, 'Wrath-Scryer Soccothrates - Aggro'),
(@ENTRY, 2, 0, 'Yes, that was quite satisfying',                                                          14, 0, 100, 0, 0, 11239, 'Wrath-Scryer Soccothrates - Kill'),
(@ENTRY, 3, 0, 'On guard!',                                                                               14, 0, 100, 0, 0, 11241, 'Wrath-Scryer Soccothrates - Charge'),
(@ENTRY, 3, 1, 'Defend yourself, for all the good it will do...',                                         14, 0, 100, 0, 0, 11242, 'Wrath-Scryer Soccothrates - Charge'),
(@ENTRY, 4, 0, 'Knew this was... the only way out',                                                       14, 0, 100, 0, 0, 11243, 'Wrath-Scryer Soccothrates - Death'),
(@ENTRY, 5, 0, 'Having problems, Dalliah? How nice.',                                                     14, 0, 100, 0, 0, 11244, 'Wrath-Scryer Soccothrates - Dalliah at 25%'),
(@ENTRY, 5, 1, 'This may be the end of you Dalliah, what a shame that would be.',                         14, 0, 100, 0, 0, 11245, 'Wrath-Scryer Soccothrates - Dalliah at 25%'),
(@ENTRY, 5, 2, 'I suggest a new strategy, you draw the attackers while I gather reinforcements. Hahaha!', 14, 0, 100, 0, 0, 11246, 'Wrath-Scryer Soccothrates - Dalliah at 25%'),
(@ENTRY, 6, 0, 'Finally! Well done!',                                                                     14, 0, 100, 0, 0, 11247, 'Wrath-Scryer Soccothrates - Dalliah dies');

-- Creature text for Mennu the Betrayer
SET @ENTRY = 17941;
DELETE FROM `creature_text` WHERE `entry`=@ENTRY;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@ENTRY, 0, 0, 'The work must continue.',         14, 0, 100, 0, 0, 10376, 'Mennu the Betrayer - Aggro'),
(@ENTRY, 0, 1, 'You brought this on yourselves.', 14, 0, 100, 0, 0, 10378, 'Mennu the Betrayer - Aggro'),
(@ENTRY, 0, 2, 'Don''t make me kill you!',        14, 0, 100, 0, 0, 10379, 'Mennu the Betrayer - Aggro'),
(@ENTRY, 1, 0, 'It had to be done.',              14, 0, 100, 0, 0, 10380, 'Mennu the Betrayer - Kill'),
(@ENTRY, 1, 1, 'You should not have come.',       14, 0, 100, 0, 0, 10381, 'Mennu the Betrayer - Kill'),
(@ENTRY, 2, 0, 'I... Deserve this.',              14, 0, 100, 0, 0, 10382, 'Mennu the Betrayer - Death');

-- Remove some unused creature_ai_texts
DELETE FROM creature_ai_texts WHERE entry IN (-1,-133,-134,-135,-136,-137,-138,-139,-217,-218,-219,-220,-221,-222,-223,-224,-225,-226,-227,-810,-811,-812,-813,-814,-815);
DELETE FROM creature_ai_texts WHERE entry BETWEEN -876 AND -841;
DELETE FROM creature_ai_texts WHERE entry BETWEEN -643 AND -634;
DELETE FROM creature_ai_texts WHERE entry BETWEEN -763 AND -740;
-- Clean up some EAI errors
UPDATE creature_ai_scripts SET action1_param2 = 0 WHERE action1_type = 1 AND creature_id IN (22992,22993,22994,23163);
-- Remove some EAI already converted to cpp
DELETE FROM creature_ai_scripts WHERE creature_id IN (26529,26530,26532,26533);
UPDATE creature_template SET AIName = '' WHERE entry IN (26529,26530,26532,26533);

-- SAI for Lord Thorval speech to deciples
SET @ENTRY = 29196;
UPDATE `creature_template` SET AIName = 'SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `creature_ai_scripts` WHERE `creature_id`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY*100 AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,60000,60000,600000,600000,80,@ENTRY*100,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - OOC - Run Script'),
(@ENTRY*100,9,0,0,0,0,100,0,1000,1000,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 0'),
(@ENTRY*100,9,1,0,0,0,100,0,8000,8000,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 1'),
(@ENTRY*100,9,2,0,0,0,100,0,8000,8000,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 2'),
(@ENTRY*100,9,3,0,0,0,100,0,8000,8000,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 3'),
(@ENTRY*100,9,4,0,0,0,100,0,8000,8000,0,0,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 4'),
(@ENTRY*100,9,5,0,0,0,100,0,8000,8000,0,0,1,5,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 5'),
(@ENTRY*100,9,6,0,0,0,100,0,8000,8000,0,0,1,6,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 6'),
(@ENTRY*100,9,7,0,0,0,100,0,8000,8000,0,0,1,7,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 7'),
(@ENTRY*100,9,8,0,0,0,100,0,7000,7000,0,0,1,8,0,0,0,0,0,1,0,0,0,0,0,0,0,'Lord Thorval - Script - Say 8');
-- Creature text for Lord Thorval
DELETE FROM creature_ai_texts WHERE entry BETWEEN -735 AND -727;
DELETE FROM `creature_text` WHERE `entry`=@ENTRY;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(@ENTRY, 0, 0, 'As disciples of blood, you strive to master the very lifeforce of your enemies.',                                       12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 1 of Speech To Deciples)'),
(@ENTRY, 1, 0, 'Be it by blade or incantation, blood feeds our attacks and weakens our foes.',                                          12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 2 of Speech To Deciples)'),
(@ENTRY, 2, 0, 'True masters learn to make blood serve more than just their strength in battle.',                                       12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 3 of Speech To Deciples)'),
(@ENTRY, 3, 0, 'Stripping energy from our foes, both fighting and fallen, allows us to persevere where lesser beigns falls exhausted.', 12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 4 of Speech To Deciples)'),
(@ENTRY, 4, 0, 'And every foe that falls, energy sapped and stolen, only further fuels our assault.',                                   12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 5 of Speech To Deciples)'),
(@ENTRY, 5, 0, 'As masters of blood, we know battle without end...',                                                                    12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 6 of Speech To Deciples)'),
(@ENTRY, 6, 0, 'We know hunger never to be quenched...',                                                                                12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 7 of Speech To Deciples)'),
(@ENTRY, 7, 0, 'We know power never to be overcome...',                                                                                 12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 8 of Speech To Deciples)'),
(@ENTRY, 8, 0, 'As masters of blood, we are masters of life and death itself. Agains us, even hope falls drained and lifeless.',        12, 0, 100, 1, 0, 0, 'Lord Thorval - (Part 9 of Speech To Deciples)');

-- Remove some EAI already converted to cpp
DELETE FROM creature_ai_scripts WHERE creature_id IN (25040,26499,30663,30918,32273);
UPDATE creature_template SET AIName = '' WHERE entry IN (25040,26499,30663,30918,32273);

-- Pathing for Mennu the Betrayer Entry: 17941
SET @NPC := 79362;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=89.26869,`position_y`=-380.2368,`position_z`=15.0899 WHERE `guid`=@NPC;
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_flag`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,89.26869,-380.2368,15.0899,0,0,0,0,100,0),
(@PATH,2,121.6363,-380.3765,29.95734,0,1000,0,0,100,0),
(@PATH,3,89.42728,-380.2369,15.12144,0,0,0,0,100,0),
(@PATH,4,49.4763,-380.2191,3.035575,0,0,0,0,100,0);

-- Pathing for Rokmar the Crackler Entry: 17991
SET @NPC := 79339;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=-34.37422,`position_y`=-458.817,`position_z`=-1.952406 WHERE `guid`=@NPC;
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_flag`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,-34.37422,-458.817,-1.952406,0,1000,0,0,100,0),
(@PATH,2,-62.09498,-454.9725,-1.592298,0,0,0,0,100,0),
(@PATH,3,-13.55803,-454.8065,2.49773,0,0,0,0,100,0),
(@PATH,4,18.32117,-448.4476,3.055895,0,0,0,0,100,0),
(@PATH,5,-13.55803,-454.8065,2.49773,0,0,0,0,100,0),
(@PATH,6,-62.09498,-454.9725,-1.592298,0,0,0,0,100,0);

DELETE FROM `creature_template_addon` WHERE `entry`=21304;
INSERT INTO `creature_template_addon` (`entry`, `bytes2`, `auras`) VALUES (21304,1, '31261');

SET @CGUID := 213212;
DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID+0 AND @CGUID+12;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
(@CGUID+0, 21304, 552, 3, 1, 197.9555, -86.81332, -10.01739, 5.8294, 7200, 0, 0),
(@CGUID+1, 21304, 552, 3, 1, 206.3423, -98.27836, -10.02623, 2.6529, 7200, 0, 0),
(@CGUID+2, 21304, 552, 3, 1, 270.7674, -66.62302, 22.45336, 5.742133, 7200, 0, 0),
(@CGUID+3, 21304, 552, 3, 1, 226.1842, -162.0961, -10.03523, 0.3490658, 7200, 0, 0),
(@CGUID+4, 21304, 552, 3, 1, 264.271, -191.2798, -10.02188, 5.61996, 7200, 0, 0),
(@CGUID+5, 21304, 552, 3, 1, 293.8853, 70.93681, 22.52617, 1.553343, 7200, 0, 0),
(@CGUID+6, 21304, 552, 3, 1, 291.632, 70.58091, 22.52693, 2.007129, 7200, 0, 0),
(@CGUID+7, 21304, 552, 3, 1, 285.4156, 127.1274, 22.29513, 4.694936, 7200, 0, 0),
(@CGUID+8, 21304, 552, 3, 1, 257.3438, 155.5679, 22.33209, 4.712389, 7200, 0, 0),
(@CGUID+9, 21304, 552, 3, 1, 298.8479, 151.7484, 22.31051, 5.707227, 7200, 0, 0),
(@CGUID+10, 20879, 552, 3, 1, 285.5186, 146.1547, 22.31179, 5.794493, 7200, 0, 0),
(@CGUID+11, 20880, 552, 3, 1, 301.7973, 127.4436, 22.31079, 1.308997, 7200, 0, 0),
(@CGUID+12, 20880, 552, 3, 1, 305.7355, 148.0587, 24.8633, 3.979351, 7200, 0, 0);

DELETE FROM `creature` WHERE `guid` IN (79506,79533,79535,79561,79570,79448);
DELETE FROM `creature_addon` WHERE `guid` IN (79506,79533,79535,79561,79570,79448);

DELETE FROM `creature` WHERE `guid` BETWEEN 86054 AND 86064;
DELETE FROM `creature_addon` WHERE `guid` BETWEEN 86054 AND 86064;
DELETE FROM `waypoint_data` WHERE `id`=860600;

DELETE FROM `creature_template_addon` WHERE `entry`=20869;
INSERT INTO `creature_template_addon` (`entry`, `bytes2`, `auras`) VALUES (20869,1, '31261 36716');

SET @CGUID := 86054; -- 86059 to 86064 free
DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID+0 AND @CGUID+4;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
(@CGUID+0, 20869, 552, 3, 1, 264.2865, -61.32112, 22.45335, 5.288348, 7200, 0, 0),
(@CGUID+1, 20869, 552, 3, 1, 336.5143, 27.42666, 48.42604, 3.839724, 7200, 0, 0),
(@CGUID+2, 20869, 552, 3, 1, 253.942, 131.8811, 22.39496, 0.7679449, 7200, 0, 0),
(@CGUID+3, 20869, 552, 3, 1, 255.4978, 158.9143, 22.36194, 5.410521, 7200, 0, 0),
(@CGUID+4, 20869, 552, 3, 1, 395.413, 18.19484, 48.29602, 2.495821, 7200, 0, 0);

-- Pathing for Gargantuan Abyssal Entry: 20898
SET @NPC := 79433;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=444.3863,`position_y`=-151.7787,`position_z`=43.03745 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`bytes2`,`mount`,`auras`) VALUES (@NPC,@PATH,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_flag`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,444.3863,-151.7787,43.03745,0,0,0,0,100,0),
(@PATH,2,456.1953,-162.0229,43.09797,0,0,0,0,100,0),
(@PATH,3,444.3863,-151.7787,43.03745,0,0,0,0,100,0),
(@PATH,4,437.7717,-136.3138,43.10011,0,0,0,0,100,0),
(@PATH,5,441.6954,-123.0422,43.10011,0,0,0,0,100,0),
(@PATH,6,445.0636,-105.6565,43.10011,0,0,0,0,100,0),
(@PATH,7,446.4647,-89.31697,43.10009,0,0,0,0,100,0),
(@PATH,8,446.4129,-65.38671,48.39542,0,0,0,0,100,0),
(@PATH,9,446.4648,-89.31628,43.10009,0,0,0,0,100,0),
(@PATH,10,445.0636,-105.6565,43.10011,0,0,0,0,100,0),
(@PATH,11,441.6954,-123.0422,43.10011,0,0,0,0,100,0),
(@PATH,12,437.7717,-136.3138,43.10011,0,0,0,0,100,0);

-- Pathing for Soul Devourer Entry: 20866
SET @NPC := 79477;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `spawndist`=0,`MovementType`=2,`position_x`=207.7079,`position_y`=-129.0737,`position_z`=-10.10952 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`bytes2`,`mount`,`auras`) VALUES (@NPC,@PATH,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_flag`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,207.7079,-129.0737,-10.10952,0,0,0,0,100,0),
(@PATH,2,245.4314,-128.669,-10.11772,0,0,0,0,100,0),
(@PATH,3,245.1088,-143.0256,-10.11027,0,0,0,0,100,0),
(@PATH,4,256.1706,-143.8266,-10.11091,0,0,0,0,100,0),
(@PATH,5,276.7943,-145.2905,-10.11652,0,0,0,0,100,0),
(@PATH,6,256.1706,-143.8266,-10.11091,0,0,0,0,100,0),
(@PATH,7,245.1088,-143.0256,-10.11027,0,0,0,0,100,0),
(@PATH,8,245.4314,-128.669,-10.11772,0,0,0,0,100,0),
(@PATH,9,207.7079,-129.0737,-10.10952,0,0,0,0,100,0),
(@PATH,10,221.3861,-128.9554,-10.11454,0,0,0,0,100,0);
-- 0xF130518200003BFB .go 207.7079 -129.0737 -10.10952

-- Pathing for Soul Devourer Entry: 20866
SET @NPC := 86053;
SET @PATH := @NPC * 10;
UPDATE `creature` SET `id`=20866,`curhealth`=1,`spawndist`=0,`MovementType`=2,`position_x`=221.8707,`position_y`=-152.5772,`position_z`=-10.11229 WHERE `guid`=@NPC;
DELETE FROM `creature_addon` WHERE `guid`=@NPC;
INSERT INTO `creature_addon` (`guid`,`path_id`,`bytes2`,`mount`,`auras`) VALUES (@NPC,@PATH,1,0, '');
DELETE FROM `waypoint_data` WHERE `id`=@PATH;
INSERT INTO `waypoint_data` (`id`,`point`,`position_x`,`position_y`,`position_z`,`orientation`,`delay`,`move_flag`,`action`,`action_chance`,`wpguid`) VALUES
(@PATH,1,221.8707,-152.5772,-10.11229,0,0,0,0,100,0),
(@PATH,2,237.1845,-152.618,-10.10515,0,0,0,0,100,0),
(@PATH,3,253.953,-152.8117,-10.1066,0,0,0,0,100,0),
(@PATH,4,253.2783,-175.503,-10.10356,0,0,0,0,100,0),
(@PATH,5,253.953,-152.8117,-10.1066,0,0,0,0,100,0),
(@PATH,6,237.1845,-152.618,-10.10515,0,0,0,0,100,0),
(@PATH,7,221.8707,-152.5772,-10.11229,0,0,0,0,100,0),
(@PATH,8,208.5352,-152.2789,-10.11248,0,0,0,0,100,0);
-- 0xF130518200003BFD .go 221.8707 -152.5772 -10.11229

UPDATE `creature_template` SET `speed_walk`= 1.6, `speed_run`= 1.428571 WHERE `entry`= 20885;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 20859;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 20857;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21186;
UPDATE `creature_template` SET `speed_walk`= 1.6, `speed_run`= 1.428571 WHERE `entry`= 20886;
UPDATE `creature_template` SET `speed_walk`= 1.2, `speed_run`= 1.428571 WHERE `entry`= 20864;
UPDATE `creature_template` SET `speed_walk`= 1.2, `speed_run`= 1.428571 WHERE `entry`= 20865;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 21303;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 21304;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20869;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20875;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20873;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20866;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20868;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20900;
UPDATE `creature_template` SET `speed_walk`= 0.888888, `speed_run`= 1.428571 WHERE `entry`= 20880;
UPDATE `creature_template` SET `speed_walk`= 0.888888, `speed_run`= 1.428571 WHERE `entry`= 20879;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 20977;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 0.8571429 WHERE `entry`= 20904;
UPDATE `creature_template` SET `speed_walk`= 1.6, `speed_run`= 1.714286 WHERE `entry`= 20898;
UPDATE `creature_template` SET `speed_walk`= 1.6, `speed_run`= 1.714286 WHERE `entry`= 20870;
UPDATE `creature_template` SET `speed_walk`= 1.6, `speed_run`= 1.714286 WHERE `entry`= 20912;
UPDATE `creature_template` SET `speed_walk`= 8, `speed_run`= 2.857143 WHERE `entry`= 20978;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1 WHERE `entry`= 21962;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20906;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20909;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20910;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 22494;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20881;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20882;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20883;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20896;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 21702;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20897;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20901;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.428571 WHERE `entry`= 20902;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 15384;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21436;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21437;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21438;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21439;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 21440;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 20515;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 22491;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 22479;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 18884;
UPDATE `creature_template` SET `speed_walk`= 1, `speed_run`= 1.142857 WHERE `entry`= 20673;

--
-- Updating data of table creature_ai_scripts
--
UPDATE creature_ai_scripts SET event_chance = 15, action1_param1 = -200, action1_param2 = -201, action1_param3 = -202 WHERE creature_id = 2017;
UPDATE creature_ai_scripts SET event_param1 = 7800, event_param2 = 15700, event_param3 = 48300, event_param4 = 66300 WHERE creature_id = 2350;
UPDATE creature_ai_scripts SET event_type = 9, event_param2 = 20 WHERE creature_id = 3100;
UPDATE creature_ai_scripts SET event_chance = 100, event_param1 = 9000, event_param2 = 18000, event_param4 = 24000 WHERE creature_id = 3461;
UPDATE creature_ai_scripts SET event_param1 = 7000, event_param2 = 15000, event_param4 = 18000 WHERE creature_id = 3473;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 3616, action1_param2 = 0, action1_param3 = 1, comment = 'Highperch Wyvern - Cast Poison Proc on Spawn' WHERE creature_id = 4107;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 3616, action1_param2 = 0, action1_param3 = 1, comment = 'Highperch Consort - Cast Poison Proc on Spawn' WHERE creature_id = 4109;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 3616, action1_param2 = 0, action1_param3 = 1, comment = 'Highperch Patriarch - Cast Poison Proc on Spawn' WHERE creature_id = 4110;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 10022, action1_param2 = 0, action1_param3 = 1, comment = 'Venomous Cloud Serpent - Cast Deadly Poison on Spawn' WHERE creature_id = 4118;
UPDATE creature_ai_scripts SET action2_param1 = -106 WHERE creature_id = 4851;
UPDATE creature_ai_scripts SET event_type = 9, event_chance = 100, event_param1 = 0, event_param2 = 5, event_param3 = 6000, event_param4 = 14000, action1_param2 = 4, action1_param3 = 32 WHERE creature_id = 5828;
UPDATE creature_ai_scripts SET event_chance = 100, event_param1 = 3000, event_param2 = 7000, event_param3 = 7000 WHERE creature_id = 5831;
UPDATE creature_ai_scripts SET event_param1 = 3000, event_param2 = 9000, event_param3 = 9000, event_param4 = 16000 WHERE creature_id = 5832;
UPDATE creature_ai_scripts SET event_type = 4, event_param3 = 0, event_param4 = 0, action1_param2 = 0, action1_param3 = 1, comment = 'Dishu - Cast Savannah Cubs on Aggro' WHERE creature_id = 5865;
UPDATE creature_ai_scripts SET event_param3 = 14000, event_param4 = 18000 WHERE creature_id = 6047;
UPDATE creature_ai_scripts SET event_flags = 1, event_param1 = 5000, event_param2 = 11000, event_param3 = 11000, event_param4 = 17000 WHERE creature_id = 6268;
UPDATE creature_ai_scripts SET action1_param2 = 1 WHERE creature_id = 6517;
UPDATE creature_ai_scripts SET event_param1 = 7000, event_param2 = 12000, event_param3 = 9000, event_param4 = 15000 WHERE creature_id = 7873;
UPDATE creature_ai_scripts SET action2_param1 = -106 WHERE creature_id = 8877;
UPDATE creature_ai_scripts SET event_flags = 1, event_param3 = 120000, event_param4 = 120000, action2_param1 = -106 WHERE creature_id = 8956;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 5, event_param3 = 6000, event_param4 = 9000, comment = 'Scarshield Grunt - Cast Strike' WHERE creature_id = 9043;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 5, event_param3 = 5000, event_param4 = 8000, action1_param3 = 0 WHERE creature_id = 9445;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 5, event_param3 = 5000, event_param4 = 9000, comment = 'Uruok Enforcer - Cast Strike' WHERE creature_id = 10601;
UPDATE creature_ai_scripts SET event_param3 = 25400, event_param4 = 42900 WHERE creature_id = 11458;
UPDATE creature_ai_scripts SET event_param1 = 0, event_param2 = 5, event_param3 = 14200, event_param4 = 21700, action1_param3 = 0 WHERE creature_id = 13160;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 20, event_param3 = 6100, event_param4 = 15700 WHERE creature_id = 13276;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 32900, action1_param3 = 1, comment = 'Broken Skeleton - Cast Bone Shards Proc on Spawn' WHERE creature_id = 16805;
UPDATE creature_ai_scripts SET action2_param1 = -106 WHERE creature_id = 17188;
UPDATE creature_ai_scripts SET action1_param2 = -988, action1_param3 = -989 WHERE creature_id = 17681;
UPDATE creature_ai_scripts SET event_param2 = 12100, event_param4 = 20100 WHERE creature_id = 17816;
UPDATE creature_ai_scripts SET event_param1 = 2400, event_param2 = 18100, event_param3 = 10800, event_param4 = 16900 WHERE creature_id = 17817;
UPDATE creature_ai_scripts SET event_flags = 6, event_param1 = 50, event_param3 = 0, event_param4 = 0, comment = 'Durnholde Tracking Hound - Cast Frenzy at 50% HP' WHERE creature_id = 17840;
UPDATE creature_ai_scripts SET action1_param2 = 1 WHERE creature_id = 18120;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 5 WHERE creature_id = 18451;
UPDATE creature_ai_scripts SET event_type = 9, event_param1 = 0, event_param2 = 5 WHERE creature_id = 18540;
UPDATE creature_ai_scripts SET event_chance = 50, event_flags = 39, action1_param3 = -1292, action2_type = 1, action2_param1 = -589, action2_param2 = -1292, action2_param3 = -1293, action3_type = 1, action3_param1 = -588, action3_param2 = -1292, action3_param3 = -1293, comment = 'Orc Prisoner - Random Say OOC' WHERE creature_id = 18598;
UPDATE creature_ai_scripts SET event_type = 0, event_param1 = 4800, event_param2 = 14500, event_param3 = 12100, event_param4 = 18100, action1_param3 = 0 WHERE creature_id = 18642;
UPDATE creature_ai_scripts SET action2_type = 11, action2_param1 = 7765, comment = 'D''ore - Text Emote and Cast Projection on Spawn' WHERE creature_id = 19412;
UPDATE creature_ai_scripts SET event_chance = 80, event_flags = 33, action1_param3 = -1130, action2_type = 1, action2_param1 = -1131, action2_param2 = -1132 WHERE creature_id = 19541;
UPDATE creature_ai_scripts SET comment = 'Terror Totem - Cast Fear' WHERE creature_id = 20455;
UPDATE creature_ai_scripts SET action1_param2 = 5 WHERE creature_id = 21128;
UPDATE creature_ai_scripts SET event_param1 = 4300, event_param2 = 12100, event_param3 = 15600, event_param4 = 19300, action1_param3 = 32 WHERE creature_id = 21891;
UPDATE creature_ai_scripts SET event_type = 8, event_chance = 100, event_flags = 5, event_param1 = 0, event_param2 = 7, event_param3 = 0, event_param4 = 0, action1_param2 = 0, comment = 'Eagle Spirit (Heroic) - Cast Spite of the Eagle on SpellHit' WHERE creature_id = 23136;
UPDATE creature_ai_scripts SET event_type = 0, event_param1 = 5000, event_param2 = 16000, event_param3 = 45000, event_param4 = 60000, action2_type = 0, action2_param1 = 0 WHERE creature_id = 23172;
UPDATE creature_ai_scripts SET event_type = 11, event_flags = 0, event_param1 = 0, event_param2 = 0, event_param3 = 0, event_param4 = 0, action1_param1 = 8876, comment = 'Den Vermin - Cast Thrash on Spawn' WHERE creature_id = 24567;

--
-- Updating data of table creature_ai_texts
--
UPDATE creature_ai_texts SET content_default = 'Core overload detected. System malfunction detected...', TYPE = 2, comment = '24972' WHERE entry = -966;
UPDATE creature_ai_texts SET content_default = 'The rift''s power is ours!', TYPE = 0, LANGUAGE = 33, comment = '24966' WHERE entry = -965;
UPDATE creature_ai_texts SET content_default = 'Excellent. We must focus our efforts on the cleansing of Nagrand!', TYPE = 0, emote = 5, comment = '18537' WHERE entry = -964;
UPDATE creature_ai_texts SET content_default = 'Looks like today was the wrong day to quit mana tapping...', TYPE = 0, emote = 1, comment = '19926' WHERE entry = -963;
UPDATE creature_ai_texts SET content_default = 'What? HAR''KOA?' WHERE entry = -952;
UPDATE creature_ai_texts SET content_default = 'You don''t worry me, boy. Just the opposite - I look forward to killing you and your friends. Nothing will stand between me and Zim''Torga now!' WHERE entry = -949;
UPDATE creature_ai_texts SET content_default = 'What is this? I recognize you from when I killed Akali. You must be this $n that everyone is dreading.' WHERE entry = -948;
UPDATE creature_ai_texts SET content_default = 'Move along, $c.' WHERE entry = -937;
UPDATE creature_ai_texts SET content_default = 'The %s seems overjoyed.' WHERE entry = -910;
UPDATE creature_ai_texts SET content_default = 'The %s doen''t look like it minds the crystal''s effect.' WHERE entry = -909;
UPDATE creature_ai_texts SET content_default = 'The %s nods appreciatively.' WHERE entry = -908;
UPDATE creature_ai_texts SET content_default = 'The %s looks confused.' WHERE entry = -907;
UPDATE creature_ai_texts SET content_default = 'The %s didn''t like what just happened.' WHERE entry = -906;
UPDATE creature_ai_texts SET TYPE = 1 WHERE entry = -891;
UPDATE creature_ai_texts SET TYPE = 1 WHERE entry = -890;
UPDATE creature_ai_texts SET TYPE = 1 WHERE entry = -889;
UPDATE creature_ai_texts SET TYPE = 1 WHERE entry = -888;
UPDATE creature_ai_texts SET content_default = 'Hey, $G man:lady;? Got some money?  I got five kids to feed!' WHERE entry = -837;
UPDATE creature_ai_texts SET content_default = 'You will rest with the honored dead.', sound = 0, TYPE = 0, LANGUAGE = 0, comment = '18493' WHERE entry = -818;
UPDATE creature_ai_texts SET content_default = 'Wait... WAIT! What is it that you want to know? I know you''re the $c named $N.' WHERE entry = -803;
UPDATE creature_ai_texts SET content_default = 'Ow! I''ll tell you NOTHING, filthy $r!' WHERE entry = -802;
UPDATE creature_ai_texts SET content_default = '%s puts his club away and begins swinging wildly!', TYPE = 2, comment = '11441' WHERE entry = -797;
UPDATE creature_ai_texts SET content_default = 'You will not master me, puny $r!' WHERE entry = -766;
UPDATE creature_ai_texts SET content_default = 'My treasure! You no steal from Tartek, dumb big-tongue traitor thing.' WHERE entry = -737;
UPDATE creature_ai_texts SET TYPE = 1 WHERE entry = -673;
UPDATE creature_ai_texts SET content_default = 'Pathetic worm!', TYPE = 0, comment = '18848' WHERE entry = -670;
UPDATE creature_ai_texts SET content_default = 'The Legion reigns!', comment = 'Shadow Labyrinth' WHERE entry = -669;
UPDATE creature_ai_texts SET content_default = 'In Sargeras'' name!', comment = 'Shadow Labyrinth' WHERE entry = -668;
UPDATE creature_ai_texts SET content_default = 'Ruin finds us all!', comment = 'Shadow Labyrinth' WHERE entry = -667;
UPDATE creature_ai_texts SET content_default = 'I do as I must!', comment = 'Shadow Labyrinth' WHERE entry = -666;
UPDATE creature_ai_texts SET content_default = 'I shall be rewarded!', comment = 'Shadow Labyrinth' WHERE entry = -665;
UPDATE creature_ai_texts SET content_default = 'The end comes for you!', comment = 'Shadow Labyrinth' WHERE entry = -664;
UPDATE creature_ai_texts SET content_default = '%s faces southeast and whimpers before looking back at you. ', TYPE = 2, comment = '3695' WHERE entry = -663;
UPDATE creature_ai_texts SET content_default = '%s growls in your direction before taking time to sniff you.', TYPE = 2, comment = '3695' WHERE entry = -662;
UPDATE creature_ai_texts SET content_default = 'You have chosen death.', comment = '18493' WHERE entry = -661;
UPDATE creature_ai_texts SET content_default = 'By the Light, you will leave this tomb!', TYPE = 0, comment = '1854' WHERE entry = -581;
UPDATE creature_ai_texts SET content_default = 'DIE, MORTALS!', sound = 5871, TYPE = 1, comment = '8443' WHERE entry = -580;
UPDATE creature_ai_texts SET content_default = 'No! You must no do this!', TYPE = 1, comment = '8497' WHERE entry = -579;
UPDATE creature_ai_texts SET content_default = 'Me Grimlok, king!' WHERE entry = -577;
UPDATE creature_ai_texts SET content_default = 'By Thaurissan''s beard! Slay them!' WHERE entry = -576;
UPDATE creature_ai_texts SET content_default = '%s begins to cast Magic Pull!', TYPE = 3, comment = '27654' WHERE entry = -544;
UPDATE creature_ai_texts SET content_default = 'Stop!  Foolish $c, we cannot let you summon the creature Myzrael!' WHERE entry = -532;
UPDATE creature_ai_texts SET content_default = 'I wonder what Nesingwary will give me for your hide!' WHERE entry = -522;
UPDATE creature_ai_texts SET content_default = 'My talons will shred your puny body, $r.' WHERE entry = -494;
UPDATE creature_ai_texts SET content_default = 'Filthy $r intruder. DIE!' WHERE entry = -474;
UPDATE creature_ai_texts SET content_default = 'You have my word that I shall find a use for your body after I''ve killed you, $r.' WHERE entry = -470;
UPDATE creature_ai_texts SET content_default = 'You will never stop the Forsaken, $r. The Dark Lady shall make you suffer.' WHERE entry = -469;
UPDATE creature_ai_texts SET content_default = 'Who is this mere $r that meddles with that which is past?  May the legend of Stalvan die along with you!' WHERE entry = -449;
UPDATE creature_ai_texts SET content_default = 'Your corpse will nourish the soil!', sound = 15486, TYPE = 1, comment = '32915' WHERE entry = -428;
UPDATE creature_ai_texts SET content_default = 'Fertilizer.', sound = 15485, TYPE = 1, comment = '32915' WHERE entry = -427;
UPDATE creature_ai_texts SET content_default = 'Matron, the Conservatory has been breached!', sound = 15483, TYPE = 1, comment = '32915' WHERE entry = -426;
UPDATE creature_ai_texts SET content_default = 'Freya! They come for you.', sound = 15496, TYPE = 1, comment = '32913' WHERE entry = -425;
UPDATE creature_ai_texts SET content_default = 'BEGONE!', sound = 15495, TYPE = 1, comment = '32913' WHERE entry = -424;
UPDATE creature_ai_texts SET content_default = 'I return you whence you came!', sound = 15494, TYPE = 1, comment = '32913' WHERE entry = -423;
UPDATE creature_ai_texts SET content_default = 'Sir, I think we were close with the Lethargy Root in that last poison recipe.' WHERE entry = -419;
UPDATE creature_ai_texts SET content_default = 'Spare some change for a poor blind man?...What do you mean I''m not blind?...I''M NOT BLIND! I CAN SEE! ITS A MIRACLE!' WHERE entry = -412;
UPDATE creature_ai_texts SET content_default = 'I shall spill your blood, $c!' WHERE entry = -407;
UPDATE creature_ai_texts SET content_default = 'Never cross a Dark Iron, $c.' WHERE entry = -406;
UPDATE creature_ai_texts SET LANGUAGE = 33 WHERE entry = -380;
UPDATE creature_ai_texts SET LANGUAGE = 33 WHERE entry = -379;
UPDATE creature_ai_texts SET content_default = 'Die $r! These lands belong to the Stonesplinter Tribe!' WHERE entry = -371;
UPDATE creature_ai_texts SET content_default = 'A $c called $N? You''ll make a fine breakfast!' WHERE entry = -370;
UPDATE creature_ai_texts SET content_default = 'Raaar!!! Me smash $r!', comment = 'Ogre Common Text' WHERE entry = -361;
UPDATE creature_ai_texts SET comment = 'Ogre Common Text' WHERE entry = -360;
UPDATE creature_ai_texts SET comment = 'Ogre Common Text' WHERE entry = -359;
UPDATE creature_ai_texts SET content_default = 'Time to die, $c!' WHERE entry = -358;
UPDATE creature_ai_texts SET content_default = 'Weak $c! You are no match for the Stonesplinter Tribe!' WHERE entry = -356;
UPDATE creature_ai_texts SET content_default = 'The only good $r is a dead $r!' WHERE entry = -355;
UPDATE creature_ai_texts SET content_default = 'Me no run from $c like you!' WHERE entry = -353;
UPDATE creature_ai_texts SET content_default = 'The only justice is death!', comment = '18796' WHERE entry = -345;
UPDATE creature_ai_texts SET content_default = 'We must not fail our leader!  Kael''thas will redeem us!', TYPE = 1, comment = '17976' WHERE entry = -344;
UPDATE creature_ai_texts SET content_default = '%s calls for reinforcements!', TYPE = 2, comment = '17976' WHERE entry = -343;
UPDATE creature_ai_texts SET content_default = '%s whispers softley in reverent tones under her breath.' WHERE entry = -339;
UPDATE creature_ai_texts SET content_default = 'Mortals have no place here!', sound = 15493, comment = '32913' WHERE entry = -320;
UPDATE creature_ai_texts SET content_default = 'Matron, flee! They are ruthless....', sound = 15503, TYPE = 1, comment = '32914' WHERE entry = -317;
UPDATE creature_ai_texts SET content_default = 'He''s so well disciplined!' WHERE entry = -309;
UPDATE creature_ai_texts SET content_default = 'By the Naaru, may it be so.' WHERE entry = -285;
UPDATE creature_ai_texts SET content_default = 'All who venture here belong to me, including you!', TYPE = 1, comment = '23864' WHERE entry = -276;
UPDATE creature_ai_texts SET content_default = 'This land was mine long before your wretched kind set foot here.', TYPE = 1, comment = '23864' WHERE entry = -275;
UPDATE creature_ai_texts SET content_default = '%s looks weak enough to capture.', TYPE = 2, comment = '4351' WHERE entry = -274;
UPDATE creature_ai_texts SET content_default = 'Amazing Amulets! Incredible curios! The newfangled jewelcrafters be havin'' nothin'' on the tried and true mystical methods of ol'' Griftah! Improve yerself through these magical talismans for a bargain price!' WHERE entry = -254;
UPDATE creature_ai_texts SET content_default = 'The raven will return to us as foretold in the prophecy and you are powerless to stop it!' WHERE entry = -243;
UPDATE creature_ai_texts SET content_default = 'The falcon is vaniquished, doomed never to rise again! Your quest is futile!' WHERE entry = -242;
UPDATE creature_ai_texts SET content_default = '%s fortifies nearby allies with runic might!', sound = 0, TYPE = 3, comment = 'Ancient Rune Giant' WHERE entry = -212;
UPDATE creature_ai_texts SET content_default = 'NO!!!  It puts the tannin in the basket, or it gets the mallet again!', TYPE = 1, comment = '14351' WHERE entry = -203;
UPDATE creature_ai_texts SET content_default = 'A fine trophy your head will make, $r.' WHERE entry = -202;
UPDATE creature_ai_texts SET content_default = 'My talons will shred your puny body, $r.' WHERE entry = -201;
UPDATE creature_ai_texts SET content_default = 'You will be easy prey, $c.' WHERE entry = -200;
UPDATE creature_ai_texts SET sound = 6596 WHERE entry = -186;
UPDATE creature_ai_texts SET content_default = 'Looking for these???? You''ll never have em!' WHERE entry = -178;
UPDATE creature_ai_texts SET content_default = 'I''m going to wear your skin as a smoking jacket! The stogies? You''ll have to pry them from my cold dead... er... RAWR!!!!', TYPE = 0 WHERE entry = -177;
UPDATE creature_ai_texts SET content_default = 'Little creature made of flesh, your wish is granted! Death comes for you!', sound = 0, TYPE = 1, comment = '21181' WHERE entry = -176;
UPDATE creature_ai_texts SET content_default = 'You will suffer eternally!', sound = 0, TYPE = 1, comment = '21181' WHERE entry = -175;
UPDATE creature_ai_texts SET content_default = 'The %s moans but seems otherwise unaffected by the blight.', sound = 0, TYPE = 2, comment = '27349' WHERE entry = -174;
UPDATE creature_ai_texts SET content_default = 'Fools! Our cause is righteous!' WHERE entry = -172;
UPDATE creature_ai_texts SET content_default = 'Lapdogs, all of you!' WHERE entry = -171;
UPDATE creature_ai_texts SET content_default = 'The end is come!', comment = '18796' WHERE entry = -147;
UPDATE creature_ai_texts SET content_default = 'You hear a faint unlocking sound...', TYPE = 2, comment = 'Mechanar' WHERE entry = -146;
UPDATE creature_ai_texts SET content_default = 'Protect the Mechanar at all costs!', TYPE = 1, comment = '19166' WHERE entry = -145;
UPDATE creature_ai_texts SET content_default = 'Protect the Botanica at all costs!', TYPE = 1, comment = '17976' WHERE entry = -144;
UPDATE creature_ai_texts SET content_default = 'Any intruders must be eliminated!', TYPE = 1, comment = 'Tempest Keep' WHERE entry = -143;
UPDATE creature_ai_texts SET content_default = 'Get it while it''s hot!', comment = '3518' WHERE entry = -142;
UPDATE creature_ai_texts SET sound = 5787 WHERE entry = -140;
UPDATE creature_ai_texts SET sound = 5788 WHERE entry = -123;
UPDATE creature_ai_texts SET content_default = 'Your bones will break under my boot, $r!' WHERE entry = -105;
UPDATE creature_ai_texts SET content_default = 'This land belongs to the Dark Iron Dwarves. Prepare to see the afterlife, $c!' WHERE entry = -104;
UPDATE creature_ai_texts SET content_default = 'Long live the Dragonmaw! Die you worthless $r!' WHERE entry = -102;
UPDATE creature_ai_texts SET sound = 5785 WHERE entry = -99;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -72;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -71;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -69;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -67;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -65;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -61;
UPDATE creature_ai_texts SET TYPE = 0, emote = 1, comment = 'Mistress Text' WHERE entry = -60;
UPDATE creature_ai_texts SET TYPE = 0, comment = 'Mistress Text' WHERE entry = -59;
UPDATE creature_ai_texts SET emote = 1, comment = 'Mistress Text' WHERE entry = -58;
UPDATE creature_ai_texts SET emote = 1, comment = 'Mistress Text' WHERE entry = -57;
UPDATE creature_ai_texts SET TYPE = 0, emote = 1, comment = 'Mistress Text' WHERE entry = -56;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -54;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -52;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -51;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -50;
UPDATE creature_ai_texts SET TYPE = 0 WHERE entry = -49;

UPDATE creature_ai_texts SET content_default = 'Anaya...? Do my eyes deceive me? Is it really you?', COMMENT = 'Cerellean Whiteclaw' WHERE entry = -661;
UPDATE creature_ai_texts SET content_default = 'That fates should be so cruel as to permit us only this after a thousand years apart...', TYPE = 2, COMMENT = 'Cerellean Whiteclaw' WHERE entry = -662;
UPDATE creature_ai_texts SET content_default = 'Do you hate me, my love? That I was forced to destroy your living form, that your spirit be released from unhappy bondage.', TYPE = 2, COMMENT = 'Cerellean Whiteclaw' WHERE entry = -663;
UPDATE creature_ai_texts SET content_default = 'No! Anaya... Anaya! Don''t leave me! Please...', COMMENT = 'Cerellean Whiteclaw' WHERE entry = -664;
UPDATE creature_ai_texts SET content_default = 'How, my love? How will I find the strength to face the ages of the world without you by my side...', COMMENT = 'Cerellean Whiteclaw' WHERE entry = -665;
UPDATE creature_ai_texts SET content_default = 'The ages have been cruel to you and I, my love, but be assured, it is, and at long last we are reunited.', COMMENT = 'Anaya' WHERE entry = -666;
UPDATE creature_ai_texts SET content_default = 'Let it not trouble your heart, beloved. You have freed me from slavery, and for that I love you all the more.', COMMENT = 'Anaya' WHERE entry = -667;
UPDATE creature_ai_texts SET content_default = 'Sadly, even this must be cut short... The ties that bind me to this world weaken, and pull me away...', COMMENT = 'Anaya' WHERE entry = -668;
UPDATE creature_ai_texts SET content_default = 'Farewell, Cerellean, until we are joined once again...', COMMENT = 'Anaya' WHERE entry = -669;
UPDATE creature_ai_texts SET content_default = '%s''s soft voice trails away into the mists, "Know that I love you always..."', TYPE = 2, COMMENT = 'Anaya' WHERE entry = -670;
UPDATE creature_ai_texts SET content_default = 'In the throes of the Sundering, Ameth''Aran was at the whim of the terror that gripped the land. There was little hope for survival.', COMMENT = 'Sargath' WHERE entry = -142;
UPDATE creature_ai_texts SET content_default = 'Athrikus came to us. He told us that he could save us from harm. He cast a spell upon us to protect us from harm.', TYPE = 0, COMMENT = 'Sargath' WHERE entry = -143;
UPDATE creature_ai_texts SET content_default = 'When the shaking stopped, his true motives were revealed. We were trapped, and he was slowly draining our powers.', TYPE = 0, COMMENT = 'Sargath' WHERE entry = -144;
UPDATE creature_ai_texts SET content_default = 'There were hundreds imprisoned by his spell. Now only a few remain in their prisons. He would speak to us sometimes.', TYPE = 0, COMMENT = 'Sargath' WHERE entry = -145;
UPDATE creature_ai_texts SET content_default = 'He worried that his power weakened, that soon even we last of his precious soulgems would fade and die.', TYPE = 0, COMMENT = 'Sargath' WHERE entry = -146;
UPDATE creature_ai_texts SET content_default = 'His lieutenant, Ilkurd Magthrull possesses a tome which might indicate the location of the remaining soulgems.', COMMENT = 'Sargath' WHERE entry = -147;
UPDATE creature_ai_texts SET content_default = '%s will be armed in 10 seconds!', TYPE = 2, COMMENT = 'Dark Iron Land Mine' WHERE entry = -579;
UPDATE creature_ai_texts SET content_default = '%s will be armed in 5 seconds!', sound = 5871, TYPE = 2, COMMENT = 'Dark Iron Land Mine' WHERE entry = -580;
UPDATE creature_ai_texts SET content_default = '%s is now armed!', TYPE = 2, COMMENT = 'Dark Iron Land Mine' WHERE entry = -581;
UPDATE creature_ai_texts SET content_default = 'Anchorite Nindumen, I have a request to make of you.', TYPE = 0, COMMENT = 'Harbinger Erothem' WHERE entry = -274;
UPDATE creature_ai_texts SET content_default = 'We''ve just sent another inexperienced squad into Nagrand. Might you offer a prayer for them?', TYPE = 0, COMMENT = 'Harbinger Erothem' WHERE entry = -275;
UPDATE creature_ai_texts SET content_default = 'May it be so.', TYPE = 0, COMMENT = 'Harbinger Erothem' WHERE entry = -276;
UPDATE creature_ai_texts SET content_default = 'Wit a reward like dis, how could they refuse, mon?', sound = 0, TYPE = 0, COMMENT = 'Warcaller Sardon Truslice' WHERE entry = -317;
UPDATE creature_ai_texts SET content_default = 'So the oracle has decided to aid you, outlander. Very well, I will offer you what I know.' WHERE entry = -10014;

-- Malykriss Altar of Sacrifice Bunny SAI
SET @ENTRY := 31065;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,4000,4000,4000,4000,11,58196,0,0,0,0,0,1,0,0,0,0,0,0,0,"Malykriss Altar of Sacrifice Bunny - OOC - Cast Malykriss Altar of Sacrifice Pulse");

-- Malykriss Blood Forge Bunny SAI
SET @ENTRY := 31068;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,4000,4000,4000,4000,11,58198,0,0,0,0,0,1,0,0,0,0,0,0,0,"Malykriss Blood Forge Bunny - OOC - Cast Malykriss Blood Forge Pulse");

-- Malykriss Icy Lookout Bunny SAI
SET @ENTRY := 31064;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,4000,4000,4000,4000,11,58195,0,0,0,0,0,1,0,0,0,0,0,0,0,"Malykriss Icy Lookout Bunny - OOC - Cast Malykriss Icy Lookout Pulse");

-- Malykriss Runeworks Bunny SAI
SET @ENTRY := 31066;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,4000,4000,4000,4000,11,58197,0,0,0,0,0,1,0,0,0,0,0,0,0,"Malykriss Runeworks Bunny - OOC - Cast Malykriss Runeworks Pulse");

-- Update creatures
UPDATE `creature` SET `modelid`=0,`spawndist`=0,`MovementType`=0 WHERE `id` IN (31064,31065,31066,31068,31075);
UPDATE `creature_template` SET `flags_extra`=`flags_extra`|128 WHERE `entry` IN (31064,31065,31066,31068);

-- Fixing quest 13010 Krolmir, Hammer of Storms
UPDATE `conditions` SET `ConditionTypeOrReference`=9, `ConditionValue1`=13010, `NegativeCondition`=0, `Comment`= 'Show gossip option if player has quest taken' WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=9900 AND `SourceEntry`=0;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=15 AND `SourceGroup`=9900 AND `SourceEntry`=1;
UPDATE `gossip_menu_option` SET `action_menu_id`=9899 WHERE `menu_id`=9900 AND `id`=0;
DELETE FROM `gossip_menu_option` WHERE `menu_id`=9900 AND `id`=1;

-- King Jokkum SAI
SET @ENTRY := 30105;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,1,62,0,100,0,9899,0,0,0,72,0,0,0,0,0,0,7,0,0,0,0,0,0,0,"King Jokkum - Gossip Option Select - Cloase gossip window"),
(@ENTRY,0,1,0,61,0,100,0,0,0,0,0,11,61319,0,0,0,0,0,7,0,0,0,0,0,0,0,"King Jokkum - Gossip Option Select - Cast Jokkum Scriptcast on player");

-- NPC talk text for King Jokkum Quest 13010 Krolmir, Hammer of Storms
DELETE FROM `creature_text` WHERE `entry`=30331;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(30331,0,0, 'Hold on, little $r.',12,0,100,0,0,0, 'King Jokkum - Mount up'),
(30331,1,0, 'Thorim! Come, show yourself!',14,0,100,0,0,0, 'King Jokkum - Conversation start'),
(30331,2,0, 'The deeds of your $r servant defy $g his:her; stature, Stormlord. $g His:Her; efforts have succeeded in softening the hearts of my people.',12,0,100,396,0,0, 'King Jokkum - Conversation 20 sec in'),
(30331,3,0, 'Never have such humble words come from mighty Thorim. I shall deliver your words to Dun Niffelem.',12,0,100,396,0,0, 'King Jokkum - Conversation 40 sec in'),
(30331,4,0, 'The events of that dark day are hereby forgiven by my people.  They shall never again be spoken of.',12,0,100,396,0,0, 'King Jokkum - Conversation 48 sec in'),
(30331,5,0, 'To signify our reforged friendship, I have something which belongs to you...',12,0,100,0,0,0, 'King Jokkum - Conversation 57 sec in'),
(30331,6,0, 'As the great explosion filled the region, my father cast a rune at the great hammer that it might not be had by our enemies. It was his final act...',12,0,100,396,0,0, 'King Jokkum - Conversation 63 sec in'),
(30331,7,0, 'We welcome the opportunity to fight by your side, mighty Thorim.',12,0,100,0,0,0, 'King Jokkum - Conversation 75 sec in'),
(30331,8,0, 'I must return to Dun Niffilem. We shall speak again soon, Stormlord.',12,0,100,396,0,0, 'King Jokkum - Conversation 80 sec in');

-- NPC talk text for Thorim Quest 13010 Krolmir, Hammer of Storms
DELETE FROM `creature_text` WHERE `entry`=30390;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
(30390,0,0, 'King Jokkum, you have summoned me?',12,0,100,0,0,0, 'Thorim  - Conversation 18 sec in'),
(30390,1,0, 'Jokkum, son of Arngrim, I have always regretted my actions here. In my grief, I brought great harm to those closest to me.',12,0,100,0,0,0, 'Thorim  - Conversation 26 sec in'),
(30390,2,0, 'I would ask your forgiveness for the suffering I have caused you and your people.',12,0,100,0,0,0, 'Thorim - Conversation 32 sec in'),
(30390,3,0, 'Krolmir... I thank you Jokkum. I hadn''t dared hope it still existed. It shall soon see glorious battle once again!',12,0,100,396,0,0, 'Thorim - Conversation 68 sec in');

SET @GURGTHOCK      := 30007;
SET @STINKBEARD     := 30017;
SET @YGGDRAS        := 30014;
SET @GARGORAL       := 30024;
SET @AZBARIN        := 30026;
SET @DUKESINGEM     := 30019;
SET @ERATHIUS       := 30025;
SET @ORINOKO        := 30020;
SET @KORRAK         := 30023;
SET @VLADOF         := 30022;
SET @FIENDOFFIRE    := 30042;
SET @FIENDOFEARTH   := 30043;
SET @FIENDOFWATER   := 30044;
SET @FIENDOFAIR     := 30045;
SET @WHISKER        := 30113;
SET @HUNGRYPENGUIN  := 30110;
SET @YGGWORM        := 30093;
SET @ENORMOS        := 30021;

-- Template Updates
UPDATE `creature_template` SET `AIName`="SmartAI",`ScriptName`="",`faction_A`=16, `faction_H`=16 WHERE `entry`IN(@VLADOF,@GARGORAL,@AZBARIN,@DUKESINGEM,@ERATHIUS);
UPDATE `creature_template` SET `AIName`="SmartAI",`ScriptName`="" WHERE `entry`IN(@GURGTHOCK,@YGGDRAS,@STINKBEARD,@ORINOKO,@KORRAK,@VLADOF,@WHISKER,@HUNGRYPENGUIN,@YGGWORM);
UPDATE `creature_template` SET `AIName`="SmartAI",`ScriptName`="",`faction_A`=634, `faction_H`=634,`unit_flags`=768 WHERE `entry` BETWEEN @FIENDOFFIRE AND @FIENDOFAIR;
-- Smart Scripts
DELETE FROM `smart_scripts` WHERE `entryorguid` BETWEEN @FIENDOFFIRE AND @FIENDOFAIR AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid` =@FIENDOFFIRE*100 AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`IN(@VLADOF*100,(@VLADOF*100)+1) AND `source_type`=9;
DELETE FROM `smart_scripts` WHERE `entryorguid`IN(@GURGTHOCK,@YGGDRAS,@STINKBEARD,@GARGORAL,@AZBARIN,@DUKESINGEM,@ERATHIUS,@ORINOKO,@KORRAK,@VLADOF,@WHISKER,@HUNGRYPENGUIN,@YGGWORM) AND `source_type`=0;
DELETE FROM `smart_scripts` WHERE `entryorguid` BETWEEN (@GURGTHOCK*100)+1 AND (@GURGTHOCK*100)+9 AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
-- Gurgthock SAI
(@GURGTHOCK,0,0,7,19,0,100,0,12932,0,0,0,80,(@GURGTHOCK*100)+1,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: Yggdras!) - Run Script"),
(@GURGTHOCK,0,1,7,19,0,100,0,12954,0,0,0,80,(@GURGTHOCK*100)+1,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: Yggdras!) - Run Script"),
(@GURGTHOCK,0,2,7,19,0,100,0,12933,0,0,0,80,(@GURGTHOCK*100)+2,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: Magnataur!) - Run Script"),
(@GURGTHOCK,0,3,7,19,0,100,0,12934,0,0,0,87,(@GURGTHOCK*100)+3,(@GURGTHOCK*100)+7,(@GURGTHOCK*100)+8,(@GURGTHOCK*100)+9,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: From Beyond!) - Run Random Script"),
(@GURGTHOCK,0,4,7,19,0,100,0,12935,0,0,0,80,(@GURGTHOCK*100)+4,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: Tuskarrmageddon!) - Run Script"),
(@GURGTHOCK,0,5,7,19,0,100,0,12936,0,0,0,80,(@GURGTHOCK*100)+5,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Amphitheater of Anguish: Korrak the Bloodrager!) - Run Script"),
(@GURGTHOCK,0,6,7,19,0,100,0,12948,0,0,0,80,(@GURGTHOCK*100)+6,2,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - On Quest Accept (The Champion of Anguish) - Run Script"),
(@GURGTHOCK,0,7,8,61,0,100,0,0,0,0,0,81,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Linked with Previous Event - Set NPC Flags"),
(@GURGTHOCK,0,8,0,61,0,100,0,0,0,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Linked with Previous Event - Set Event Phase 2"),
(@GURGTHOCK,0,9,16,38,0,100,0,10,10,60000,60000,1,15,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,10,16,38,0,100,0,11,11,0,0,1,10,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,11,16,38,0,100,0,12,12,0,0,1,12,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,12,16,38,0,100,0,13,13,0,0,1,11,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,13,16,38,0,100,0,14,14,0,0,1,11,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,14,16,38,0,100,0,15,15,0,0,1,11,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,15,16,38,0,100,0,16,16,0,0,1,14,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - On Data Set - Say"),
(@GURGTHOCK,0,16,17,61,0,100,0,0,0,0,0,81,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Linked with Previous Event - Set NPC Flags"),
(@GURGTHOCK,0,17,0,61,0,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Linked with Previous Event - Set Event Phase 1"),
(@GURGTHOCK,0,18,19,1,2,100,0,300000,300000,300000,300000,81,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Out of Combat - Set Npc Flags Gossip & Questgiver (Phase 2)"),
(@GURGTHOCK,0,19,0,61,2,100,0,0,0,0,0,22,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gurgthock - Out of Combat - Set Event Phase 0 (Phase 2)"),
((@GURGTHOCK*100)+1,9,0,0,0,0,100,0,3000,3000,0,0,1,7,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+1,9,1,0,0,0,100,0,4000,4000,0,0,1,8,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+1,9,2,0,0,0,100,0,5000,5000,0,0,12,@YGGDRAS,2,300000,0,0,0,8,0,0,0,5762.054199,-2954.385010,273.826955,5.108289,"Gurgthock - Script - Summon Yggdras"),
((@GURGTHOCK*100)+2,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+2,9,1,0,0,0,100,0,4000,4000,0,0,1,3,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+2,9,2,0,0,0,100,0,5000,5000,0,0,12,@STINKBEARD,2,300000,0,0,0,8,0,0,0,5754.692,-2939.46,286.276123,5.156380,"Gurgthock - Script - Summon Stinkbeard"),
((@GURGTHOCK*100)+3,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+3,9,1,0,0,0,100,0,4000,4000,0,0,1,6,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+3,9,2,0,0,0,100,0,5000,5000,0,0,12,@GARGORAL,2,300000,0,0,0,8,0,0,0,5776.855,-2989.77979,272.96814,5.194,"Gurgthock - Script - Summon Gargoral"),
((@GURGTHOCK*100)+3,9,3,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5739.813, -2981.524, 290.7671, 5.986479,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,4,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5828.899, -2960.155, 312.7516, 3.525565,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,5,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5743.305, -3011.297, 290.7671, 0.6108652,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,6,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5763.189, -3029.675, 290.7671, 1.37881,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,7,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5793.061, -2934.593, 286.3596, 4.08407,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,8,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5742.101, -2950.756, 286.2643, 5.113815,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,9,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5828.502, -2981.737, 286.3596, 3.141593,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,10,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5769.401, -2935.121, 286.3358, 4.852015,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,11,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5820.309, -3002.837, 290.7671, 2.583087,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,12,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5733.76, -3000.346, 286.3596, 0.4712389,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,13,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5724.983, -2969.896, 286.3596, 6.056293,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,14,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5722.487, -3010.75, 312.7516, 0.5061455,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,15,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5744.417, -3025.528, 286.3596, 0.9424778,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,16,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5813.945, -2956.747, 286.3596, 3.717551,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,17,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5797.321, -2955.269, 290.7671, 4.118977,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+3,9,18,0,0,0,100,0,0,0,0,0,12,@FIENDOFWATER,2,300000,0,0,0,8,0,0,0,5816.855, -2974.476, 290.7671, 3.612832,"Gurgthock - Script - Fiend of Water"),
((@GURGTHOCK*100)+4,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+4,9,1,0,0,0,100,0,3000,3000,0,0,1,0,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+4,9,2,0,0,0,100,0,5000,5000,0,0,12,@ORINOKO,2,300000,0,0,0,8,0,0,0,5754.692,-2939.46,286.276123,5.156380,"Gurgthock - Script - Summon Orinko"),
((@GURGTHOCK*100)+5,9,0,0,0,0,100,0,3000,3000,0,0,1,1,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+5,9,1,0,0,0,100,0,4000,4000,0,0,1,2,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+5,9,2,0,0,0,100,0,5000,5000,0,0,12,@KORRAK,2,300000,0,0,0,8,0,0,0,5754.692,-2939.46,286.276123,5.156380,"Gurgthock - Script - Summon Korrak the Bloodrager"),
((@GURGTHOCK*100)+6,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+6,9,1,0,0,0,100,0,4000,4000,0,0,1,13,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+6,9,2,0,0,0,100,0,5000,5000,0,0,12,@VLADOF,2,300000,0,0,0,8,0,0,0,5754.692,-2939.46,286.276123,5.156380,"Gurgthock - Script - Summon Vladof the Butcher"),
((@GURGTHOCK*100)+7,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+7,9,1,0,0,0,100,0,4000,4000,0,0,1,6,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+7,9,2,0,0,0,100,0,5000,5000,0,0,12,@AZBARIN,2,300000,0,0,0,8,0,0,0,5776.855,-2989.77979,272.96814,5.194,"Gurgthock - Script - Summon Az'Barin"),
((@GURGTHOCK*100)+7,9,3,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5739.813, -2981.524, 290.7671, 5.986479,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,4,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5828.899, -2960.155, 312.7516, 3.525565,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,5,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5743.305, -3011.297, 290.7671, 0.6108652,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,6,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5763.189, -3029.675, 290.7671, 1.37881,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,7,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5793.061, -2934.593, 286.3596, 4.08407,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,8,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5742.101, -2950.756, 286.2643, 5.113815,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,9,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5828.502, -2981.737, 286.3596, 3.141593,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,10,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5769.401, -2935.121, 286.3358, 4.852015,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,11,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5820.309, -3002.837, 290.7671, 2.583087,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,12,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5733.76, -3000.346, 286.3596, 0.4712389,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,13,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5724.983, -2969.896, 286.3596, 6.056293,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,14,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5722.487, -3010.75, 312.7516, 0.5061455,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,15,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5744.417, -3025.528, 286.3596, 0.9424778,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,16,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5813.945, -2956.747, 286.3596, 3.717551,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,17,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5797.321, -2955.269, 290.7671, 4.118977,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+7,9,18,0,0,0,100,0,0,0,0,0,12,@FIENDOFAIR,2,300000,0,0,0,8,0,0,0,5816.855, -2974.476, 290.7671, 3.612832,"Gurgthock - Script - Fiend of Air"),
((@GURGTHOCK*100)+8,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+8,9,1,0,0,0,100,0,4000,4000,0,0,1,6,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+8,9,2,0,0,0,100,0,5000,5000,0,0,12,@DUKESINGEM,2,300000,0,0,0,8,0,0,0,5776.855,-2989.77979,272.96814,5.194,"Gurgthock - Script - Summon Duke Singen"),
((@GURGTHOCK*100)+8,9,3,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5739.813, -2981.524, 290.7671, 5.986479,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,4,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5828.899, -2960.155, 312.7516, 3.525565,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,5,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5743.305, -3011.297, 290.7671, 0.6108652,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,6,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5763.189, -3029.675, 290.7671, 1.37881,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,7,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5793.061, -2934.593, 286.3596, 4.08407,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,8,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5742.101, -2950.756, 286.2643, 5.113815,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,9,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5828.502, -2981.737, 286.3596, 3.141593,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,10,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5769.401, -2935.121, 286.3358, 4.852015,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,11,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5820.309, -3002.837, 290.7671, 2.583087,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,12,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5733.76, -3000.346, 286.3596, 0.4712389,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,13,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5724.983, -2969.896, 286.3596, 6.056293,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,14,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5722.487, -3010.75, 312.7516, 0.5061455,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,15,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5744.417, -3025.528, 286.3596, 0.9424778,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,16,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5813.945, -2956.747, 286.3596, 3.717551,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,17,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5797.321, -2955.269, 290.7671, 4.118977,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+8,9,18,0,0,0,100,0,0,0,0,0,12,@FIENDOFFIRE,2,300000,0,0,0,8,0,0,0,5816.855, -2974.476, 290.7671, 3.612832,"Gurgthock - Script - Fiend of Fire"),
((@GURGTHOCK*100)+9,9,0,0,0,0,100,0,3000,3000,0,0,1,9,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+9,9,1,0,0,0,100,0,4000,4000,0,0,1,6,0,0,0,0,0,7,0,0,0,0,0,0,0,"Gurgthock - Script - Say"),
((@GURGTHOCK*100)+9,9,2,0,0,0,100,0,5000,5000,0,0,12,@ERATHIUS,2,300000,0,0,0,8,0,0,0,5776.855,-2989.77979,272.96814,5.194,"Gurgthock - Script - Summon Erathius"),
((@GURGTHOCK*100)+9,9,3,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5739.813, -2981.524, 290.7671, 5.986479,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,4,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5828.899, -2960.155, 312.7516, 3.525565,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,5,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5743.305, -3011.297, 290.7671, 0.6108652,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,6,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5763.189, -3029.675, 290.7671, 1.37881,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,7,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5793.061, -2934.593, 286.3596, 4.08407,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,8,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5742.101, -2950.756, 286.2643, 5.113815,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,9,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5828.502, -2981.737, 286.3596, 3.141593,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,10,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5769.401, -2935.121, 286.3358, 4.852015,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,11,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5820.309, -3002.837, 290.7671, 2.583087,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,12,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5733.76, -3000.346, 286.3596, 0.4712389,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,13,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5724.983, -2969.896, 286.3596, 6.056293,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,14,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5722.487, -3010.75, 312.7516, 0.5061455,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,15,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5744.417, -3025.528, 286.3596, 0.9424778,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,16,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5813.945, -2956.747, 286.3596, 3.717551,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,17,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5797.321, -2955.269, 290.7671, 4.118977,"Gurgthock - Script - Fiend of Earth"),
((@GURGTHOCK*100)+9,9,18,0,0,0,100,0,0,0,0,0,12,@FIENDOFEARTH,2,300000,0,0,0,8,0,0,0,5816.855, -2974.476, 290.7671, 3.612832,"Gurgthock - Script - Fiend of Earth"),
-- Yggdras SAI
(@YGGDRAS,0,0,0,9,0,100,0,0,5,9000,15000,11,40504,1,0,0,0,0,7,0,0,0,0,0,0,0,"Yggdras - In Combat - Cast Cleave"),
(@YGGDRAS,0,1,0,9,0,100,0,0,5,6000,11000,11,57076,0,0,0,0,0,7,0,0,0,0,0,0,0,"Yggdras - In Combat - Cast Corrode Flesh"),
(@YGGDRAS,0,2,3,6,0,100,0,0,0,0,0,11,55859,2,0,0,0,0,1,0,0,0,0,0,0,0,"Yggdras - On Death - Cast Jormungar Spawn"),
(@YGGDRAS,0,3,4,61,0,100,0,0,0,0,0,45,11,11,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Yggdras - Linked with Previous Event - Set Data 11 11 "),
(@YGGDRAS,0,4,5,61,0,100,0,0,0,0,0,15,12932,0,0,0,0,0,16,0,0,0,0,0,0,0,"Yggdras - Linked with Previous Event - Call Area Explored or Event Happens"),
(@YGGDRAS,0,5,0,61,0,100,0,0,0,0,0,15,12954,0,0,0,0,0,16,0,0,0,0,0,0,0,"Yggdras - Linked with Previous Event - Call Area Explored or Event Happens"),
(@YGGDRAS,0,6,7,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Yggdras - On Evade - Set Data 10 10 "),
(@YGGDRAS,0,7,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Yggdras - Linked with Previous Event - Despawn"),
(@YGGDRAS,0,8,0,11,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Yggdras - On Spawn - Say"),
-- Stinkbeard SAI
(@STINKBEARD,0,0,1,11,0,100,0,0,0,0,0,18,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - On Spawn - Set Unit Flags"),
(@STINKBEARD,0,1,2,61,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Say"),
(@STINKBEARD,0,2,0,61,0,100,0,0,0,0,0,53,1,@STINKBEARD,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Start WP"),
(@STINKBEARD,0,3,4,40,0,100,0,7,@STINKBEARD,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - On Reached WP7 - Set Unit Flags"),
(@STINKBEARD,0,4,5,61,0,100,0,0,0,0,0,8,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Set Agressive"),
(@STINKBEARD,0,5,0,61,0,100,0,0,0,0,0,101,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Set Home Position"),
(@STINKBEARD,0,6,7,6,0,100,1,0,0,0,0,45,12,12,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Stinkbeard - On Death - Set Data 12 12 "),
(@STINKBEARD,0,7,0,61,0,100,0,0,0,0,0,15,12933,0,0,0,0,0,16,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Call Area Explored or Event Happens"),
(@STINKBEARD,0,8,9,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Stinkbeard - On Evade - Set Data 10 10 "),
(@STINKBEARD,0,9,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Despawn"),
(@STINKBEARD,0,10,0,9,0,100,0,0,10,10000,16000,11,31389,2,0,0,0,0,7,0,0,0,0,0,0,0,"Stinkbeard - On Range - Cast Knock Away"),
(@STINKBEARD,0,11,0,0,0,100,0,11000,15000,12000,18000,11,55867,2,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - In Combat - Cast Stinky Beard"),
(@STINKBEARD,0,12,0,4,0,100,0,0,0,0,0,11,55866,2,0,0,0,0,2,0,0,0,0,0,0,0,"Stinkbeard - On Agro - Cast Thunderblade"),
(@STINKBEARD,0,13,14,2,0,100,1,0,20,0,0,11,50420,2,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - On Less than 20% HP - Cast Enrage"),
(@STINKBEARD,0,14,0,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - Linked with Previous Event - Say"),
(@STINKBEARD,0,15,0,2,0,100,1,0,10,0,0,11,15588,2,0,0,0,0,1,0,0,0,0,0,0,0,"Stinkbeard - On Less than 20% HP - Cast Thunderclap"),
-- Gargoral the Water Lord SAI
(@GARGORAL,0,0,0,0,0,100,0,5000,8000,5000,8000,11,55909,0,0,0,0,0,2,0,0,0,0,0,0,0,"Gargoral the Water Lord - In Combat - Cast Crashing Wave"),
(@GARGORAL,0,1,2,6,0,100,1,0,0,0,0,45,13,13,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Gargoral the Water Lord - On Death - Set Data 13 13 "),
(@GARGORAL,0,2,5,61,0,100,0,0,0,0,0,15,12934,0,0,0,0,0,16,0,0,0,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Call Area Explored or Event Happens"),
(@GARGORAL,0,3,4,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Gargoral the Water Lord - On Evade - Set Data 10 10 "),
(@GARGORAL,0,4,5,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Despawn"),
(@GARGORAL,0,5,0,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,@FIENDOFWATER,0,500,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Set Data on Fiend of Water"),
(@GARGORAL,0,6,7,11,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gargoral the Water Lord - On Spawn - Say"),
(@GARGORAL,0,7,0,61,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Remove unattackable flags"),
(@GARGORAL,0,8,0,2,0,100,1,0,50,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Gargoral the Water Lord - On Less than 50% HP - Say"),
(@GARGORAL,0,9,10,2,0,100,0,0,50,5000,5000,64,1,0,0,0,0,0,2,0,0,0,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Store Target List"),
(@GARGORAL,0,10,11,61,0,100,0,0,0,0,0,100,1,0,0,0,0,0,9,@FIENDOFWATER,0,500,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Send Target List to Fiend of Water"),
(@GARGORAL,0,11,0,61,0,100,0,0,0,0,0,45,1,2,0,0,0,0,9,@FIENDOFWATER,0,500,0,0,0,0,"Gargoral the Water Lord - Linked with Previous Event - Set Data on Fiend of Water"),
-- Az'Barin, Prince of the Gust SAI
(@AZBARIN,0,0,0,0,0,100,0,5000,8000,11000,16000,11,55912,0,0,0,0,0,1,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - In Combat - Cast Blast of Air"),
(@AZBARIN,0,1,2,6,0,100,1,0,0,0,0,45,13,13,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - On Death - Set Data 13 13 "),
(@AZBARIN,0,2,5,61,0,100,0,0,0,0,0,15,12934,0,0,0,0,0,16,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Call Area Explored or Event Happens"),
(@AZBARIN,0,3,4,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - On Evade - Set Data 10 10 "),
(@AZBARIN,0,4,5,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Despawn"),
(@AZBARIN,0,5,0,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,@FIENDOFAIR,0,500,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Set Data on Fiend of Air"),
(@AZBARIN,0,6,7,11,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - On Spawn - Say"),
(@AZBARIN,0,7,0,61,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Remove unattackable flags"),
(@AZBARIN,0,8,0,2,0,100,1,0,50,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - On Less than 50% HP - Say"),
(@AZBARIN,0,9,10,2,0,100,0,0,50,5000,5000,64,1,0,0,0,0,0,2,0,0,0,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Store Target List"),
(@AZBARIN,0,10,11,61,0,100,0,0,0,0,0,100,1,0,0,0,0,0,9,@FIENDOFAIR,0,500,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Send Target List to Fiend of Air"),
(@AZBARIN,0,11,0,61,0,100,0,0,0,0,0,45,1,2,0,0,0,0,9,@FIENDOFAIR,0,500,0,0,0,0,"Az'Barin, Prince of the Gust - Linked with Previous Event - Set Data on Fiend of Air"),
-- Duke Singen <The New Hotness> SAI
(@DUKESINGEM,0,0,0,0,0,100,0,5000,8000,5000,8000,11,55916,0,0,0,0,0,2,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - In Combat - Cast Magma Wave"),
(@DUKESINGEM,0,1,2,6,0,100,1,0,0,0,0,45,13,13,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Duke Singen <The New Hotness> - On Death - Set Data 13 13 "),
(@DUKESINGEM,0,2,5,61,0,100,0,0,0,0,0,15,12934,0,0,0,0,0,16,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Call Area Explored or Event Happens"),
(@DUKESINGEM,0,3,4,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Duke Singen <The New Hotness> - On Evade - Set Data 10 10 "),
(@DUKESINGEM,0,4,5,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Despawn"),
(@DUKESINGEM,0,5,0,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,@FIENDOFFIRE,0,500,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Set Data on Fiend of Fire"),
(@DUKESINGEM,0,6,7,11,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - On Spawn - Say"),
(@DUKESINGEM,0,7,0,61,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Remove unattackable flags"),
(@DUKESINGEM,0,8,0,2,0,100,1,0,50,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - On Less than 50% HP - Say"),
(@DUKESINGEM,0,9,10,2,0,100,0,0,50,5000,5000,64,1,0,0,0,0,0,2,0,0,0,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Store Target List"),
(@DUKESINGEM,0,10,11,61,0,100,0,0,0,0,0,100,1,0,0,0,0,0,9,@FIENDOFFIRE,0,500,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Send Target List to Fiend of Fire"),
(@DUKESINGEM,0,11,0,61,0,100,0,0,0,0,0,45,1,2,0,0,0,0,9,@FIENDOFFIRE,0,500,0,0,0,0,"Duke Singen <The New Hotness> - Linked with Previous Event - Set Data on Fiend of Fire"),
-- Erathius, King of Dirt SAI
(@ERATHIUS,0,0,0,9,0,100,0,0,5,5000,8000,11,55918,2,0,0,0,0,1,0,0,0,0,0,0,0,"Erathius, King of Dirt - On Range - Cast Shockwave"),
(@ERATHIUS,0,1,2,6,0,100,1,0,0,0,0,45,13,13,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Erathius, King of Dirt - On Death - Set Data 13 13 "),
(@ERATHIUS,0,2,5,61,0,100,0,0,0,0,0,15,12934,0,0,0,0,0,16,0,0,0,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Call Area Explored or Event Happens"),
(@ERATHIUS,0,3,4,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Erathius, King of Dirt - On Evade - Set Data 10 10 "),
(@ERATHIUS,0,4,5,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Despawn"),
(@ERATHIUS,0,5,0,61,0,100,0,0,0,0,0,45,1,1,0,0,0,0,9,@FIENDOFEARTH,0,500,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Set Data on Fiend of Earth"),
(@ERATHIUS,0,6,7,11,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Erathius, King of Dirt - On Spawn - Say"),
(@ERATHIUS,0,7,0,61,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Remove unattackable flags"),
(@ERATHIUS,0,8,0,2,0,100,1,0,50,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Erathius, King of Dirt - On Less than 50% HP - Say"),
(@ERATHIUS,0,9,10,2,0,100,0,0,50,5000,5000,64,1,0,0,0,0,0,2,0,0,0,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Store Target List"),
(@ERATHIUS,0,10,11,61,0,100,0,0,0,0,0,100,1,0,0,0,0,0,9,@FIENDOFEARTH,0,500,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Send Target List to Fiend of Earth"),
(@ERATHIUS,0,11,0,61,0,100,0,0,0,0,0,45,1,2,0,0,0,0,9,@FIENDOFEARTH,0,500,0,0,0,0,"Erathius, King of Dirt - Linked with Previous Event - Set Data on Fiend of Earth"),
-- Orinoko Tuskbreaker SAI
(@ORINOKO,0,0,1,11,0,100,0,0,0,0,0,18,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Spawn - Set Unit Flags"),
(@ORINOKO,0,1,2,61,0,100,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Say"),
(@ORINOKO,0,2,0,61,0,100,0,0,0,0,0,97,20,10,0,0,0,0,1,0,0,0,5776.319824,-2981.005371,273.100037,0,"Orinoko Tuskbreaker - Linked with Previous Event - Jump to Position"),
(@ORINOKO,0,3,4,1,0,100,1,5000,5000,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Reached WP7 - Set Unit Flags"),
(@ORINOKO,0,4,5,61,0,100,0,0,0,0,0,8,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Set Agressive"),
(@ORINOKO,0,5,0,61,0,100,0,0,0,0,0,101,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Set Home Position"),
(@ORINOKO,0,6,7,6,0,100,1,0,0,0,0,45,14,14,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Death- Set Data 14 14 "),
(@ORINOKO,0,7,0,61,0,100,0,0,0,0,0,15,12935,0,0,0,0,0,16,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Call Area Explored or Event Happens"),
(@ORINOKO,0,8,9,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Evade - Set Data 10 10 "),
(@ORINOKO,0,9,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Despawn"),
(@ORINOKO,0,10,0,0,0,100,0,20000,25000,35000,45000,11,55937,0,0,0,0,0,2,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - In Combat - Cast Fishy Scent"),
(@ORINOKO,0,11,0,0,0,100,0,0,0,60000,60000,11,32064,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - In Combat - Cast Battle Shout"),
(@ORINOKO,0,12,0,4,0,100,0,0,0,0,0,11,55929,2,0,0,0,0,7,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Agro - Cast Impale"),
(@ORINOKO,0,13,14,2,0,100,1,0,50,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - On Less than 50% HP - Say"),
(@ORINOKO,0,14,0,61,0,100,1,0,0,0,0,11,55946,0,0,0,0,0,1,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Linked with Previous Event - Summon Whisker"),
(@ORINOKO,0,15,0,9,0,100,0,10,60,15000,25000,11,55929,2,0,0,0,0,7,0,0,0,0,0,0,0,"Orinoko Tuskbreaker - Range - Cast Impale"),
-- Korrak the Bloodrager SAI
(@KORRAK,0,0,1,11,0,100,0,0,0,0,0,18,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Spawn - Set Unit Flags"),
(@KORRAK,0,1,2,61,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Say"),
(@KORRAK,0,2,0,61,0,100,0,0,0,0,0,53,1,@KORRAK,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Start WP"),
(@KORRAK,0,3,4,40,0,100,0,6,@KORRAK,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Reached WP6 - Set Unit Flags"),
(@KORRAK,0,4,5,61,0,100,0,0,0,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Say"),
(@KORRAK,0,5,6,61,0,100,0,0,0,0,0,8,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Set Agressive"),
(@KORRAK,0,6,0,61,0,100,0,0,0,0,0,101,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Set Home Position"),
(@KORRAK,0,7,8,6,0,100,1,0,0,0,0,45,15,15,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Korrak the Bloodrager - On Death - Set Data 15 15 "),
(@KORRAK,0,8,0,61,0,100,0,0,0,0,0,15,12936,0,0,0,0,0,16,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Call Area Explored or Event Happens"),
(@KORRAK,0,9,10,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Korrak the Bloodrager - On Evade - Set Data 10 10 "),
(@KORRAK,0,10,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - Linked with Previous Event - Despawn"),
(@KORRAK,0,11,0,9,0,100,0,8,25,15000,21000,11,24193,2,0,0,0,0,7,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Range - Cast Charge"),
(@KORRAK,0,12,0,9,0,100,0,0,5,12000,17000,11,30471,2,0,0,0,0,7,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Range - Cast Uppercut"),
(@KORRAK,0,13,0,4,0,100,0,0,0,0,0,11,55948,2,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Agro - Cast Grow"),
(@KORRAK,0,14,0,2,0,100,1,0,20,0,0,11,42745,2,0,0,0,0,1,0,0,0,0,0,0,0,"Korrak the Bloodrager - On Less than 20% HP - Cast Enrage"),
-- Vladof the Butcher SAI
(@VLADOF,0,0,1,11,0,100,0,0,0,0,0,18,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - On Spawn - Set Unit Flags"),
(@VLADOF,0,1,2,61,0,100,0,0,0,0,0,43,@ENORMOS,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Linked with Previous Event - Mount to Enormos"),
(@VLADOF,0,2,3,61,0,100,0,0,0,0,0,97,20,10,0,0,0,0,1,0,0,0,5776.319824,-2981.005371,273.100037,0,"Vladof the Butcher - Linked with Previous Event - Jump to Position"),
(@VLADOF,0,3,0,61,0,100,0,0,0,0,0,80,@VLADOF*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Linked with Previous Event - Run Script"),
(@VLADOF,0,4,0,4,0,100,1,0,0,0,0,80,(@VLADOF*100)+1,2,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - On Agro - Run Script"),
(@VLADOF,0,5,6,6,0,100,1,0,0,0,0,45,16,16,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Vladof the Butcher - On Death - Set Data 16 16 "),
(@VLADOF,0,6,0,61,0,100,0,0,0,0,0,15,12948,0,0,0,0,0,16,0,0,0,0,0,0,0,"Vladof the Butcher - Linked with Previous Event - Call Area Explored or Event Happens"),
(@VLADOF,0,7,8,7,0,100,1,0,0,0,0,45,10,10,0,0,0,0,19,@GURGTHOCK,0,0,0,0,0,0,"Vladof the Butcher - On Evade - Set Data 10 10 "),
(@VLADOF,0,8,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Linked with Previous Event - Despawn"),
(@VLADOF,0,9,0,9,0,100,0,0,5,7000,12000,11,55973,2,0,0,0,0,7,0,0,0,0,0,0,0,"Vladof the Butcher - On Range - Cast Blood Plague"),
(@VLADOF,0,10,0,9,0,100,0,0,5,15000,21000,11,55974,2,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - On Range - Cast Blood Boil"),
(@VLADOF,0,11,0,0,0,100,0,21000,26000,21000,26000,11,55975,2,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - IC - Cast Hysteria"),
(@VLADOF,0,12,13,0,0,100,0,15000,21000,21000,29000,1,4,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - IC - Say (Phase 2)"),
(@VLADOF,0,13,0,61,0,100,0,0,0,0,0,11,55976,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - IC - Cast Spell Deflection "),
(@VLADOF*100,9,0,0,0,0,100,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Say"),
(@VLADOF*100,9,1,0,0,0,100,0,3000,3000,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Remove Unit Flags"),
(@VLADOF*100,9,2,0,0,0,100,0,0,8,0,0,8,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Set Agressive"),
(@VLADOF*100,9,3,0,0,0,100,0,0,0,0,0,101,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Set Home Position"),
((@VLADOF*100)+1,9,0,0,0,0,100,0,0,0,0,0,1,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Say"),
((@VLADOF*100)+1,9,1,0,0,0,100,0,0,0,0,0,1,3,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Say"),
((@VLADOF*100)+1,9,2,0,0,0,100,0,0,0,0,0,43,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Vladof the Butcher - Script - Dismount"),
-- Elemental Fiends SAI
(@FIENDOFFIRE,0,0,1,38,0,100,0,1,1,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Fire - On Data Set - Evade"),
(@FIENDOFEARTH,0,0,1,38,0,100,0,1,1,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Earth - On Data Set - Evade"),
(@FIENDOFWATER,0,0,1,38,0,100,0,1,1,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Water - On Data Set - Evade"),
(@FIENDOFAIR,0,0,1,38,0,100,0,1,1,0,0,24,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Air - On Data Set - Evade"),
(@FIENDOFFIRE,0,1,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Fire - Linked with Previous Event - Despawn"),
(@FIENDOFEARTH,0,1,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Earth - Linked with Previous Event - Despawn"),
(@FIENDOFWATER,0,1,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Water - Linked with Previous Event - Despawn"),
(@FIENDOFAIR,0,1,0,61,0,100,0,0,0,0,0,41,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Air - Linked with Previous Event - Despawn"),
(@FIENDOFFIRE,0,2,0,38,0,100,0,1,2,0,0,80,@FIENDOFFIRE*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Fire - On Data Set - Run Script"),
(@FIENDOFEARTH,0,2,0,38,0,100,0,1,2,0,0,80,@FIENDOFFIRE*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Earth - On Data Set - Run Script"),
(@FIENDOFWATER,0,2,0,38,0,100,0,1,2,0,0,80,@FIENDOFFIRE*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Water - On Data Set - Run Script"),
(@FIENDOFAIR,0,2,0,38,0,100,0,1,2,0,0,80,@FIENDOFFIRE*100,2,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Air - On Data Set - Run Script"),
(@FIENDOFFIRE,0,3,0,9,0,30,0,10,70,2000,7000,11,55872,0,0,0,0,0,7,0,0,0,0,0,0,0,"Fiend of Fire - IC - Cast Orb of Flame"),
(@FIENDOFEARTH,0,3,0,9,0,30,0,10,70,2000,7000,11,55886,0,0,0,0,0,7,0,0,0,0,0,0,0,"Fiend of Earth - IC - Cast Boulder"),
(@FIENDOFWATER,0,3,0,9,0,30,0,10,70,2000,7000,11,55888,0,0,0,0,0,7,0,0,0,0,0,0,0,"Fiend of Water - IC - Cast Orb of Water"),
(@FIENDOFAIR,0,3,0,9,0,30,0,10,70,2000,7000,11,55882,0,0,0,0,0,7,0,0,0,0,0,0,0,"Fiend of Air - IC - Cast Orb of Storms"),
(@FIENDOFFIRE,0,4,0,7,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Fire - On Evade - Set Unit Flags"),
(@FIENDOFEARTH,0,4,0,7,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Earth - On Evade - Set Unit Flags"),
(@FIENDOFWATER,0,4,0,7,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Water - On Evade - Set Unit Flags"),
(@FIENDOFAIR,0,4,0,7,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Fiend of Air - On Evade - Set Unit Flags"),
(@FIENDOFFIRE*100,9,0,0,0,0,100,0,0,0,0,0,19,768,0,0,0,0,0,1,0,0,0,0,0,0,0,"Elemental Fiend - Script - Remove Unit Flags"),
(@FIENDOFFIRE*100,9,1,0,0,0,100,0,0,0,0,0,49,0,0,0,0,0,0,12,1,0,0,0,0,0,0,"Elemental Fiend - Script - Attack Stored Target"),
-- Whisker SAI
(@WHISKER,0,0,0,9,0,100,0,0,5,10000,15000,11,50169,0,0,0,0,0,7,0,0,0,0,0,0,0,"Whisker - On Range - Cast Flipper Attack"),
-- Hungry Penguin SAI
(@HUNGRYPENGUIN,0,0,1,11,0,100,0,0,0,0,0,2,7,0,0,0,0,0,1,0,0,0,0,0,0,0,"Hungry Penguin - On Spawn - Set Faction"),
(@HUNGRYPENGUIN,0,1,2,61,0,100,0,0,0,0,0,8,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Hungry Penguin - Linked with Previous Event - Set Defensive"),
(@HUNGRYPENGUIN,0,2,0,61,0,100,0,0,0,0,0,49,0,0,0,0,0,0,23,0,0,0,0,0,0,0,"Hungry Penguin - Linked with Previous Event - Attack Sumonner"),
-- Yggdras Worm SAI
(@YGGWORM,0,0,0,11,0,100,0,0,0,0,0,2,14,0,0,0,0,0,1,0,0,0,0,0,0,0,"Yggdras Worm - On Spawn - Set Faction");
-- Texts
DELETE FROM `creature_text` WHERE `entry`IN(@GURGTHOCK,@YGGDRAS,@STINKBEARD,@GARGORAL,@AZBARIN,@DUKESINGEM,@ERATHIUS,@ORINOKO,@KORRAK,@VLADOF);
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES
(@GURGTHOCK, 0, 0, 'This battle must be seen to be believed! Once a mild-mannered tuskarr fisherman, our next fighter turned to the life of a soulless mercenary when his entire family was wiped out by a vicious pack of lion seals and ill-tempered penguins! Now he''s just in it for the gold! Ladies and gentlemen, ORINOKO TUSKBREAKER!!', 14, 0, 100, 15, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 1, 0, 'The champion of the Winterax trolls has challenged you, $N! I hope you''re ready!', 12, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 2, 0, 'Hailling from the distant mountains of Alterac, one of the fiercest competitors this arena has ever seen: KORRAK THE BLOODRAGER!!!', 14, 0, 100, 15, 0, 13363, 'Gurgthock'),
(@GURGTHOCK, 3, 0, 'The battle is about to begin! Am I reading this card right? It... It''s the nefarious magnataur lord, STINKBEARD! Yes, folks, STINKBEARD! $N doesn''t stand a chance!', 14, 0, 100, 15, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 6, 0, 'Do you feel that folks? The air is crackling with energy! That can only mean one thing...', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 7, 0, 'The grand Amphitheater of Anguish awaits, $N. Remember, once a battle starts you have to stay in the arena. WIN OR DIE!', 12, 0, 100, 1, 0, 13910, 'Gurgthock'),
(@GURGTHOCK, 7, 1, 'The grand Amphitheater of Anguish awaits, $N. Remember, once a battle starts you have to stay in the arena. WIN OR DIE!', 12, 0, 100, 1, 0, 13911, 'Gurgthock'),
(@GURGTHOCK, 8, 0, 'Here we are once again, ladies and gentlemen. The epic struggle between life and death in the Amphitheater of Anguish! For this round we have $N versus the hulking jormungar, Yg... Yggd? Yggdoze? Who comes up with these names?! $N versus big worm!', 14, 0, 100, 15, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 9, 0, 'Prepare to make your stand, $N! Get in the Amphitheater and stand ready! Remember, you and your opponent must stay in the arena at all times or you will be disqualified!', 12, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 10, 0, '$N has defeated Yg... Yggg-really big worm!', 14, 0, 100, 15, 0, 13905, 'Gurgthock'),
(@GURGTHOCK, 10, 1, '$N has defeated Yg... Yggg-really big worm!', 14, 0, 100, 15, 0, 13908, 'Gurgthock'),
(@GURGTHOCK, 10, 2, '$N has defeated Yg... Yggg-really big worm!', 14, 0, 100, 15, 0, 13909, 'Gurgthock'),
(@GURGTHOCK, 10, 3, '$N has defeated Yg... Yggg-really big worm!', 14, 0, 100, 15, 0, 13907, 'Gurgthock'),
(@GURGTHOCK, 11, 0, '$N is victorious once more!', 14, 0, 100, 15, 0, 13908, 'Gurgthock'),
(@GURGTHOCK, 11, 1, '$N is victorious once more!', 14, 0, 100, 15, 0, 13909, 'Gurgthock'),
(@GURGTHOCK, 11, 2, '$N is victorious once more!', 14, 0, 100, 15, 0, 13905, 'Gurgthock'),
(@GURGTHOCK, 12, 0, 'And with AUTHORITY, $N dominates the magnataur lord! Stinkbeard''s clan is gonna miss him back home in the Dragonblight!!', 14, 0, 100, 15, 0, 13908, 'Gurgthock'),
(@GURGTHOCK, 13, 0, 'From the Savage Ledge of Icecrown, Vladof the Butcher and his mammoth, Enormos! There ain''t gonna be a thing left of our challengers, folks! Prepare for a downpour of blood, guts and tears!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 14, 0, 'I DON''T BELIEVE IT! WE HAVE A NEW CHAMPION OF ANGUISH!!! Vladof the Butcher has been defeated by a ragtag crew of nobodies! Incredible finish!', 14, 0, 100, 15, 0, 13908, 'Gurgthock'),
(@GURGTHOCK, 15, 0, 'All that''s left of the challenger is a red stain on the floor!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 15, 1, 'Avert your eyes, ladies and gentlemen! It''s a bloodbath!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 15, 2, 'Call in the clowns! It''s turned into a circus in there!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 15, 3, 'OOOF! That one''s gonna require the \"scraper.\"', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 15, 4, 'OUTTA NOWHERE -- WHAMO! DEAD!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@GURGTHOCK, 15, 5, 'They went down like a sack of orc skulls!', 14, 0, 100, 1, 0, 0, 'Gurgthock'),
(@YGGDRAS, 1, 0, '%s emerges!', 41, 0, 100, 0, 0, 13878, 'Yggdras'),
(@STINKBEARD, 0, 0, '%s becomes enraged!', 16, 0, 100, 1, 0, 0, 'Stinkbeard'),
(@STINKBEARD, 1, 0, 'Stinkbeard comin'' for you, little ones!', 14, 0, 100, 1, 0, 13907, 'Stinkbeard'),
(@GARGORAL, 1, 0, 'For Neptulon! Strength of storm and sea, crush my enemies!', 14, 0, 100, 1, 0, 13878, 'Gargoral the Water Lord'),
(@GARGORAL, 2, 0, 'I am the baddest of the bad, the coolest of the cool! To my side, my elements, let us freeze and rule!', 14, 0, 100, 1, 0, 0, 'Gargoral the Water Lord'),
(@AZBARIN, 1, 0, 'Al''Akir grant my enemies a cold, swift death!', 14, 0, 100, 0, 0, 13878, 'Az''Barin, Prince of the Gust'),
(@AZBARIN, 2, 0, 'I bring a gust so strong, it can knock down trees! Minions, assist me, they shall not do as they please!', 14, 0, 100, 0, 0, 0, 'Az''Barin, Prince of the Gust'),
(@DUKESINGEM, 1, 0, 'I''ll melt the flesh off your bones!', 14, 0, 100, 0, 0, 13878, 'Duke Singen <The New Hotness>'),
(@DUKESINGEM, 2, 0, 'To burn my kingdom, you can''t use fire! Come my servants, I am not ready to retire!', 14, 0, 100, 0, 0, 0, 'Duke Singen <The New Hotness>'),
(@ERATHIUS, 1, 0, 'Arise, brothers of the earth! Watch as Erathius destroys the mortals!', 14, 0, 100, 0, 0, 13878, 'Erathius, King of Dirt'),
(@ERATHIUS, 2, 0, 'I''m the king of dirt, there is none higher! To my aid, minions - assist your sire!', 14, 0, 100, 0, 0, 0, 'Erathius, King of Dirt'),
(@ORINOKO, 0, 0, 'Come, land-dwellers, face the fury of tusk and whisker!', 14, 0, 100, 1, 0, 0, 'Orinoko Tuskbreaker'),
(@ORINOKO, 1, 0, 'Whisker! Where are you?! Assist me!', 14, 0, 100, 0, 0, 0, 'Orinoko Tuskbreaker'),
(@KORRAK, 1, 0, 'Korrak has come, weaklings! Snowfall belongs to Winterax! Erm, Korrak mean to say amphitheater belong to Winterax!', 14, 0, 100, 15, 0, 0, 'Korrak the Bloodrager'),
(@KORRAK, 2, 0, 'No graveyard here for you to play wicked games on Korrak! DIE!', 14, 0, 100, 15, 0, 0, 'Korrak the Bloodrager'),
(@VLADOF, 1, 0, 'Yes, my precious, we will tear them apart! For the one true king!', 14, 0, 100, 15, 0, 13907, 'Vladof the Butcher'),
(@VLADOF, 2, 0, 'Useless mongrel! I''ll kill ''em myself!', 14, 0, 100, 0, 0, 0, 'Vladof the Butcher'),
(@VLADOF, 3, 0, 'Vladof dismounts!', 41, 0, 100, 1, 0, 0, 'Vladof the Butcher'),
(@VLADOF, 4, 0, '%s begins to spin, encased in a magical shield!', 41, 0, 100, 1, 0, 0, 'Vladof the Butcher');
-- Waypoints
DELETE FROM `script_waypoint` WHERE `entry`IN(@STINKBEARD,@KORRAK);
DELETE FROM `waypoints` WHERE `entry`IN(@STINKBEARD,@KORRAK);
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
(@STINKBEARD, 1, 5790.98, -2927.3, 286.277, ''),
(@STINKBEARD, 2, 5803, -2953, 286.277, ''),
(@STINKBEARD, 3, 5805.18, -2958.51, 282.899, ''),
(@STINKBEARD, 4, 5807.04, -2961.45, 280.25, ''),
(@STINKBEARD, 5, 5808.86, -2965.14, 277.089, ''),
(@STINKBEARD, 6, 5811.7, -2970.82, 273.569, ''),
(@STINKBEARD, 7, 5789.98, -2980.25, 273.584, ''),
(@KORRAK, 1, 5722.56, -2960.69, 286.276, ''),
(@KORRAK, 2, 5734.7, -2984.99, 286.276, ''),
(@KORRAK, 3, 5737.4, -2991.31, 282.575, ''),
(@KORRAK, 4, 5740.42, -2997.54, 277.263, ''),
(@KORRAK, 5, 5743.79, -3004.05, 273.57, ''),
(@KORRAK, 6, 5764.24, -2993.79, 272.944, '');
-- Conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry` IN(55872,55886,55888,55882);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 1, 55872, 0, 0, 31, 0, 4, 0, 0, 0, 0, 0, '', 'Orb of Fire Hits player'),
(13, 1, 55886, 0, 0, 31, 0, 4, 0, 0, 0, 0, 0, '', 'Boulder Hits player'),
(13, 1, 55888, 0, 0, 31, 0, 4, 0, 0, 0, 0, 0, '', 'Orb of Water Hits player'),
(13, 1, 55882, 0, 0, 31, 0, 4, 0, 0, 0, 0, 0, '', 'Orb of Storms Hits player');
-- Template addon entries
DELETE FROM `creature_template_addon` WHERE `entry` IN (@YGGDRAS,@VLADOF,@FIENDOFFIRE,@FIENDOFEARTH,@FIENDOFWATER,@FIENDOFAIR,@ORINOKO,@STINKBEARD,@DUKESINGEM,@GARGORAL,@ERATHIUS,@AZBARIN);
INSERT INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(@YGGDRAS, 0, 0x0, 0x1, ''),
(@VLADOF, 0, 0x0, 0x1, '50689'),
(@FIENDOFFIRE, 0, 0x0, 0x1, ''),
(@FIENDOFEARTH, 0, 0x0, 0x1, ''),
(@FIENDOFWATER, 0, 0x0, 0x1, ''),
(@FIENDOFAIR, 0, 0x0, 0x1, ''),
(@ORINOKO, 0, 0x0, 0x1, ''),
(@DUKESINGEM, 0, 0x2000000, 0x1, ''),
(@GARGORAL, 0, 0x2000000, 0x1, ''),
(@ERATHIUS, 0, 0x2000000, 0x1, ''),
(@AZBARIN, 0, 0x2000000, 0x1, ''),
(@STINKBEARD, 0, 0x0, 0x1, '');
-- Equipment Template entries
DELETE FROM `creature_equip_template` WHERE `entry` IN(@VLADOF,@STINKBEARD);
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`) VALUES
(@VLADOF, 1, 41764, 41764, 0),
(@STINKBEARD, 1, 41691, 41691, 0);

DELETE FROM `areatrigger_tavern` WHERE `id`=5360;
INSERT INTO `areatrigger_tavern` (`id`, `name`) VALUES (5360, 'Grom\'arsh Crash-Site');

SET @CGUID := 62739;
DELETE FROM `creature` WHERE `id` IN (31804,31805,31807);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `MovementType`) VALUES
-- Transport_Moa'ki_Unu'pe
(@CGUID+0, 31807, 620, 1, 1, -19.87627, -0.049722, 17.61058, 2.86234, 120, 0, 0),
(@CGUID+1, 31805, 620, 1, 1, 8.109375, -1.96228, 15.83483, 3.224337, 120, 0, 0),

-- Transport_Moa'ki_Kamagua
(@CGUID+2, 31804, 621, 1, 1, 14.08325, 1.959717, 18.8097, 3.735005, 120, 0, 0);


DELETE FROM `creature_template_addon` WHERE `entry` IN (31804,31805,31807);
INSERT INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(31807, 0, 0x0, 0x1, ''),
(31805, 0, 0x0, 0x101, ''),
(31804, 0, 0x0, 0x101, '');

